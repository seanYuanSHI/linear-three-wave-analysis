% This function generate a list of seed
% wave lengths near the expected resonance.
%
% Input:
%    fpath : file path that contains
%            fpath/Resonance.mat
%    branch: integer selecting resonance branch
%            branch=1 is the first b3 in scatter data
%    na    : number of additional points to scan
%    scale : when scale>0, radius of the scan is scale*(max-min)
%            when scale<0, radius of the scan is |scale| in Trad/s 
% Output:
%    lambda2 : array of lambda2 values, also write to
%      fpath/lambda2s_b3-<b3>.txt
%      which list 3 expected lambda 
%      and additional points nearby.
%      Here, b3 is the index of the resonance branch.
%      The first three values are from Resonances.mat
%      The remaining values are additional points, in reverse order
%
% Written by Yuan SHI, May 27, 2022

function lambda2 = SeedScan(fpath, branch, na, scale)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % number of additional points
    if nargin<3
        na = 5;
    end
    % additional point are at
    % w2 = linspace(w0-dw, w0+dw,na)
    % dw = scale*(max-min) of expected
    if nargin<4
        scale = 10;
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % load expected resonances
    load([fpath,'/Resonances.mat'])
    % analytic pump frequency
    w1 = scatters(1).w1;
    % place holder for b3 and w2
    w2r = zeros(1,3);
    b3r = zeros(1,3);
    % load data
    for i=1:3
        w2r(i) = scatters(i).map.w2(branch);
        b3r(i) = scatters(i).map.b3(branch);
    end
    % check b3 is consistent
    assert(b3r(1)==b3r(2) && b3r(2)==b3r(3), 'b3 is inconsistent!')
    b3 = b3r(1);

    % mean and spread of w2
    w0 = mean(w2r);
    dw = max(w2r)-min(w2r);
    % scaled dw
    if scale>0
        dw = scale*dw;
    else
        dw = abs(scale);
    end
    % range of w2
    w2min = max(0,w0-dw);
    w2max = min(w1,w0+dw);
    % prepare array of w2, in Trad/s
    w2 = [w2r,linspace(w2min,w2max,na)];
    % convert to vacuum wavelength in um
    constants % load c8 from Three-Wave/src/auxiliary/constants.m
    lambda2 = 200*pi*c8./w2;

    % print data to file
    fname = [fpath,'/lambda2s_b3-',num2str(b3),'.txt'];
    % open file for writing
    fid = fopen(fname,'w+');
    % write to file
    for i=1:3+na
        fprintf(fid,'%.15f\n',lambda2(i));
    end
    % close file
    fclose(fid);
    
