% This function collects resonant seed frequencies
% from analytic and numerical results, and print to file
%
% Inputs:
%    fpath  : string specify path the file name
%    vname  : string specified variable name, e.g.'B'
%    values : value for file name  
%    branch : string specify tail of file name, e.g. b3-3
%
%           analytical wavelengths are the first lines of
%               <fpath><vname><values(i)>/lambda2s_<branch>.txt
%           numerical wavelengths are collected in
%               <fpath>rSeedLambda2s_<branch>.txt
%
%           output file name is
%               <fpath>w2s_<branch>.txt
%           each line of the output file is of the form
%               values(i), ana, num 
%
% Written by Yuan SHI on June 19, 2022

function PrintAnaNumW2s(fpath, vname, values, branch)
    % read numerical lambdas
    fname = [fpath,'rSeedLambda2s_',branch,'.txt'];
    fid = fopen(fname,'r');
    num = fscanf(fid,'%f');
    fclose(fid);

    % number of values
    nv = length(values);
    % check match number of lines in num
    assert(nv==length(num));

    % open output file for writing
    fname = [fpath,'w2s_',branch,'.txt'];
    fid = fopen(fname,'w+');
    % write file header
    fprintf(fid,'# %s, analytic (Prad/s), numeric (Prad/s)\n',vname);

    % load sppedof light c8 from Three-Wave/src
    constants
    % prefactor for converting wavelength (um) to frequency (Prad/s)
    l2w = c8*pi/5; 

    % write each line 
    for iv=1:nv
        % read analytic
        vs = num2str(values(iv));
        fname = [fpath,vname,vs,'/lambda2s_',branch,'.txt'];
        aid = fopen(fname,'r');
        ana = fscanf(aid,'%f');
        fclose(aid);

        % print to output file, analytic is 1st line of ana
        fprintf(fid,'%s, %f, %f\n',vs,l2w/ana(1),l2w/num(iv));
    end

    % close file
    fclose(fid);
