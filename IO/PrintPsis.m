% This function prints saved psi data in multiple folders
% and save them to .txt file for shell script to load. 
% Also print to screen. Assume laser wave branch is 1.
%
% If the fitted psi differs from the analytical psi by
% more than threshold (e.g. threshold=0.1), then print
% analytic psi. Otherwise, print fitted psi. 
% 
% Inputs :
%    fhead    : string specify the head of the file name
%    values   : value for file name of the Psi.mat file 
%    ftail    : string specify the tail of the input file name
%               <fhead><values(i)><ftail>/Psi.mat
%    branch   : 1 or 2, specify small psi or large psi
%    oname    : output .txt file name, 
%               defaul oname='~/epoch/epoch1d/Data/Psis.txt'
%   threshold : difference beyond which will use analytic
%
% Output:
%    write a .txt file with <oname>
%    each row is the psi corresponding to values(i)
%
% Written by Yuan SHI on Nov 4, 2021
% Last modified by Yuan SHI on June 12, 2022

function PrintPsis(fhead, values, ftail, branch, oname, threshold)
    if nargin<6
        threshold=0.1;
    end
    if nargin<5
        oname='~/epoch/epoch1d/Data/Psis.txt';
    end
    if strcmp(oname(end-11),'P')
        disp('Comparing psi with analytical pumps')
    else
        disp('Comparing psi with analytical seeds')
    end
 

    % number of values
    nv = length(values);
    % open file for writing
    fid = fopen(oname,'a');
 
    for iv=1:nv
        % file name
        vs = num2str(values(iv));
        fname = [fhead,vs,ftail,'/Psi.mat'];
        pname = [fhead,vs,ftail,'/../PumpPara.mat'];
        sname = [fhead,vs,ftail,'/../SeedPara.mat'];
        %disp(fname)

        % read data and load analytic
        load(fname)

        % use default oname to decide read pump psi
        if strcmp(oname(end-11),'P')
            load(pname)
        else
            load(sname)
        end
        psi1a = wave1.psi_rad;
        psi2a = wave2.psi_rad;

        % select branch
        if branch==1 % smaller. Choose this for seed
            psi = min(psi1,psi2);
        else
            psi = max(psi1,psi2);
        end
        % print to screen
        msg = [vs,': selected psi=',num2str(psi,'%.4f')];
        msg = [msg, ', Analytic psi1 = ',num2str(psi1a,'%.4f')];
        msg = [msg, ', psi2 = ',num2str(psi2a,'%.4f')];
        disp(msg)

        % write to file for wave1 psi
        if abs(psi-psi1a)>threshold
            % use analytic
            fprintf(fid,'%.4f\n',psi1a);
            disp('    change selected to psi1')
        else
            fprintf(fid,'%.15f\n',psi);
        end
    end

    % close file
    fclose(fid);
