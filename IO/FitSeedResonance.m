% This function fit resonant seed wavelength 
% from the a2 value at given time slices.
%
% The fitting assumes that Lorentzian line shape
% near resonance:
%    a2(w) = A/[(w-w0)^2+nv^2]
% where w0 is the resonant frequency, 
% and v is related to damping.
% The equation can be rewritten as
%    y = a*w^2+b*w+c
% where y=1/a2, a=1/A, b=-2*w0/A, b=(w0^2+nv^2)/A
% After linear regression, w0=-b/(2*a)
%
% Inputs:
%    its     : time slice to extract envelop and fit
%    rpath   : path to the root folder
%    branch   : header of resonance, e.g. b3-4
%              Envelope data for i=th seed wavelength is
%              <rpath><branch>L<i>/Stimulated/Envelope/<it>.mat
%
%              Wavelengths (um) correspond to L<i> are listed in
%              <rpath>lambda2s_<branch>.txt
%    flag    : flag=1 also plot figure
%              flag=0 only return data
%    exclude : list of L<i> to exclude from fitting
%              e.g. exclude=[1,2,5] means L1,L2,L5 will be excluded
%
% Outputs:
%   Save inputs and the following data in <rpath>FitResonance_<fhead>.mat  
%      lambda0  : averaged resonant wavelength in um
%      nu0      : averaged damping date, in Trad/s
%      w0s      : resonant frequencies in Trad/s, for all it
%      nus      : phenoneological damping date, in Trad/s , for all it
%   aux : additional data for IO prupose
%  
% Written by Yuan SHI on May 27, 2022

function aux = FitSeedResonance(its, rpath, branch, flag, exclude)
    if nargin<4
        flag=1; % default plot
    end
    if nargin<5
        exclude = []; % default no exclusion
    else
        disp('Excluded from fitting are: ')
        exclude
    end

    % number of time slices
    nt = length(its);
    % initialize As, w0s, nus
    As = zeros(1,nt);
    w0s = zeros(1,nt);
    nus = zeros(1,nt);

    % load lambdas
    fname = [rpath,'lambda2s_',branch,'.txt'];
    disp(['Loading wavelengths from ',fname])
    fid = fopen(fname,'r');
    lambdas = fscanf(fid,'%f'); % column vector
    fclose(fid);
 
    % number of lambdas
    nl = length(lambdas);
    % speed of light consistent with epoch
    constants % load c8 from Three-Wave/src/auxiliary/constants.m
    % convert vacuum wavelength lambda to frequency w
    ws = 200*pi*c8./lambdas; % in Trad/s, nl-by-1 
    % initialize arrays
    % included daya load from front to back
    % excluded data load from back to front
    a2max = zeros(nl,nt); % to store a2
    wlist = zeros(nl,1); % to store ws

    % mark exclusion as negative lambda
    ne = length(exclude);
    for i=1:ne
        ie = exclude(i);
        lambdas(ie)=-lambdas(ie);
    end

    if flag>0
       % initialize figure
       figure
       % x axis    
       x = linspace(min(ws),max(ws),100);
       % handle for legend
       hl = zeros(1,nt);
    end

    % fit for each it 
    jj = 0;
    for j = 1:nt
        it = its(j);
        % initialize counter
        ii = 0;
        ie = nl;
        % load values
        for i = 1:nl
           % load data
           spath = [rpath,branch,'L',num2str(i),'/Stimulated/Envelope/'];
           fname = [spath,num2str(it,'%04.f'),'.mat'];
           load(fname)

           % store data
           if lambdas(i)>0 % include the data
               % update counter
               ii = ii+1;
               % seed amplitude
               a2max(ii,j) = max(EA.ar);
               wlist(ii) = ws(i);
           else % exclude the data from fit
               % seed amplitude
               a2max(ie,j) = max(EA.ar);
               wlist(ie) = ws(i);
               % update counter
               ie = ie-1;
           end
       end
     
       % linear regressions y=P*q+e, where P is predictor
       % q is coefficients, and e is error
       % minimizes e'*e=(y-P*q)'*(y-P*q).
       % Take variation, q is solved from (P'*P)*q=P'*y
       % construct y vector
       y = 1./a2max(1:ii,j);
       % construct predictor matrix
       P = [wlist(1:ii).^2,wlist(1:ii),ones(ii,1)];
       PP = P'*P;
       Py = P'*y;
       % solve linear system for q
       q = PP\Py;
     
       % extract fitting parameters 
       w0 = -q(2)/2/q(1);
       A = 1/q(1);
       v2 = A*q(3)-w0^2;
       nu = sqrt(v2);

       % load output
       if v2>0 % valie
           jj = jj+1;
           As(jj) = A;
           w0s(jj) = w0;
           nus(jj) = nu;
       end

       % plot figure
       if flag>0
           % plot all data
           hl(j)=scatter(wlist,a2max(:,j),'DisplayName',['it=',num2str(it)]);
           hold all
           % mark excluded data
           if ie<nl
               scatter(wlist(ie+1:nl),a2max(ie+1:nl,j),'kx')
           end
           % plot fitting
           if v2>0
               y = A./((x-w0).^2+v2);
               plot(x,y,':')
               % plot best fit
               line([w0,w0],ylim,'LineStyle','--','Color',[0.5,0.5,0.5]);
           end
        end
    end

    if jj>0 % exist valid fit
        % averaged results
        w0 = mean(w0s(1:jj));
        nu0 = mean(nus(1:jj));
        % convert to lambda0
        lambda0 = 200*pi*c8/w0; % in um
    else % no valid fit, use analytic
        disp('No valid fit! Use analytic.')
        % first three elements of lambdas
        lambda0 = mean(lambdas(1:3));
        nu0 = 0;
    end
    % save data
    fname = [rpath,'FitResonance_',branch,'.mat'];
    save(fname,'lambda0','As','nu0','w0s','nus','its','lambdas')

    % load aux data
    aux.wlist = wlist;
    aux.a2max = a2max;
    aux.ie = ie;
    aux.w0 = w0;

    % mark up figure
    if flag>0
        if jj>1
            % plot averaged fit
            line([w0,w0],ylim,'Color','k');
        end
        % mark axis
        xlabel('frequency (Trad/s)')
        ylabel('max (a2)')
        % title str
        ts = ['lambda0=',num2str(lambda0),' um, ...'];
        %ts = [ts, ' um, damping=',num2str(nu0),' Trad/s'];
        ts = [ts,rpath(end-15:end)]; % last 15 character
        title(ts)
        legend(hl)
        set(gcf,'Color','w')
    end
