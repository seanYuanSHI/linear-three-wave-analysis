% This function gathers and prints saved 1D scan 
% statistics to .txt file for plotting. 
% 
% Inputs :
%    fhead  : string specify the head of the file name
%    values : value for file name 
%    ftail  : string specify the tail of the file name
%             <fhead><values(i)>/<ftail>/Stimulated/Fits/1Dstat_<axis>.mat
%    axis   : axis='r0' for 1D scan of r0 at fixed mu3
%             axis='mu3' for 1D scan of mu3 at fixed r0
%    oname  : output .txt file name
%             default '~/epoch/epoch1d/Data/1Dstat_<ftail>_<axis>.txt'
%
% Output:
%    write a .txt file in oname
%    Each row is of the form:  value, va, v0, vl, vr
%
% Written by Yuan SHI on June 21, 2022

function Print1Dstat(fhead, values, ftail, axis, oname)
    % default axis
    if nargin<4
        axis = 'r0';
    end
    % default file name
    if nargin<5
        oname = ['~/epoch/epoch1d/Data/1Dstat_',ftail,'_',axis,'.txt'];
    end

    % check and report axis
    if strcmp(axis,'r0')~=1
        axis = 'mu3'; % enforece
    end
    disp(['Collecting 1D scan along ',axis])

    % number of values
    nv = length(values);
    % open file for writing
    fid = fopen(oname,'w+');
    % write file header
    fprintf(fid,'#######################'); 
    fprintf(fid,'\n# fhead is %s',fhead); 
    fprintf(fid,'\n# ftail is %s',ftail); 
    fprintf(fid,'\n# axis  is %s',axis); 
    fprintf(fid,'\n#######################\n');
    fprintf(fid,'## value,      %sa,        %s',axis,axis);
    fprintf(fid,',        %sl,        %sr\n',axis,axis);
 
    for iv=1:nv
        % file name
        f = values(iv);
        fs = num2str(f);
        fname = [fhead,fs,'/',ftail,'/Stimulated/Fits/1Dstat_',axis,'.mat'];
        disp(fname)

        % read data
        load(fname)
        % write to file
        fprintf(fid,'%8.4e, %8.4e, %8.4e, %8.4e, %8.4e\n',f,va,v0,vl,vr);
    end

    % write file footer
    fprintf(fid,'## v0 above is %s\n',vname);
    % close file
    fclose(fid);
