% This function generates a list of resonant seed wavelengths
% by reading parameters for fitting a2 agaist lambda2 
% which should have been generaed using FitSeedResonance.m
%
% Inputs:
%    fhead  : string specify the head of the file name
%    values : value for file name  
%    ftail  : string specify the tail of the file name
%             The fit parameters are stored in
%             <fhead><values(i)>/FitResonance_<ftail>.mat
%    oname  : output .txt file name
%             default '~/epoch/epoch1d/Data/rSeedLambda2s_<ftail>.txt'
% Outputs:
%    list all resonant lambda2 in file <oname>
%    each row is the resonant lambda2 corresponds to values(i)
%
% Written by Yuan SHI, June 1, 2022

function PrintLambda2s(fhead, values, ftail, oname)
    if nargin<4
        oname = ['~/epoch/epoch1d/Data/rSeedLambda2s_',ftail,'.txt'];
        disp(['Default file name is ',oname])
    end

    % number of values
    nv = length(values);
    % open file for writing
    fid = fopen(oname,'w+');
 
    for iv=1:nv
        % file name
        vs = num2str(values(iv));
        fname = [fhead,vs,'/FitResonance_',ftail,'.mat'];

        % read data
        disp(fname)
        load(fname)

        % write to file
        fprintf(fid,'%.15f\n',lambda0);
    end

    % close file
    fclose(fid);
