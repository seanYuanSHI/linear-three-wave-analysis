% This function plot pump and seed envelopes saved in file
% The data are processed by PostPtocess.m and are saved in
% with default file name like <spath>Stimulated/Envelope/0002.mat
%
% Inputs:
%    spath : path to the seed folder 
%    its   : time slices to plot
%  combine : combine>0 plot all pumps in one figure
%            otherwise plot separate figures
%            combine is also used to label figures

function PlotAs(spath, its, combine)
    % default plot separate figures
    if nargin<3
        combine=0;
    end
    if combine>0
        % large number to avoid collisions with other plots
        figure(99+combine) % for pump
    end

    % number of time slices
    nt = length(its);
    for i = 1:nt
       it = its(i);
       % load data
       fname = [spath,'Stimulated/Envelope/',num2str(it,'%04.f'),'.mat'];
       load(fname)

       % plot figure
       if combine>0
           % check pump depletion
           figure(99+combine)
           plot(EA.xl,EA.al,'DisplayName',['it=',num2str(it)])
           hold all
       else
           % plot pump and seed
           figure('Name',num2str(it))
           plot(EA.xr,EA.ar,'r.-')
           hold on
           plot(EA.xl,EA.al,'b.-')
           ts = ['it=',num2str(it),': t=',num2str(EA.time),' s, '];
           ts = [ts,spath(end-15:end)];
           title(ts)
           legend('right','left')
       end
       xlabel('x')
       ylabel('a')
       set(gcf,'Color','w')
   end

   % mark combined figure
   if combine>0
       figure(99+combine)
       legend show
       legend('Location','southeast')
       % show x axis to give a scale
       %line(xlim,[0,0],'Color',[1,1,1]/2)
       title(spath(end-25:end)) % last 25 characters
   end
