% This function read grid-based variables in sdf files
% The default file contents are:
%    Ex, Ey, Ez,
%    Bx, By, Bz,
%    ne, ni,
%    x
%
% Inputs:
%    it     : read file corresponds to the it-th dump
%             e.g. it=3, read files '0003.sdf'
%    vname  : variable name, choose from "Ex","Ey",...  
%    path   : folder where sdf files are located
%    flag   : flag=1 plot figure
% Outputs:
%    xm : cell-centered grid, 1-by-nx
%    f  : data, 1-by-nx
%    t  : current time in SI unit, sclar
%
% Written by Yuan SHI, Sep 14, 2021

function [xm, f, t] = ReadGrid(it, vname, path, flag)

    % default no plot
    if nargin<4
        flag = 0;
    end

    % file name
    fname = [path,num2str(it(1),'%04.f'),'.sdf'];
    %disp(['read data from: ',fname])
    % read data
    data = GetDataSDF(fname);
    t = data.time;

    % x grid
    x = data.Grid.Grid.x;
    xm = (x(1:end-1)+x(2:end))/2;

    % field value
    if strcmp(vname,'Ex')
        f = data.Electric_Field.Ex.data;
    elseif strcmp(vname,'Ey')
        f = data.Electric_Field.Ey.data;
    elseif strcmp(vname,'Ez')
        f = data.Electric_Field.Ez.data;
    elseif strcmp(vname,'Bx')
        f = data.Magnetic_Field.Bx.data;
    elseif strcmp(vname,'By')
        f = data.Magnetic_Field.By.data;
    elseif strcmp(vname,'Bz')
        f = data.Magnetic_Field.Bz.data;
    elseif strcmp(vname,'ne')
        f = data.Derived.Number_Density.electron.data;
    else
        f = data.Derived.Number_Density.proton.data;
    end
    
    if flag==1
        clf
        plot(xm,f,'.-')
        xlabel('x (m)')
        ylabel(vname)
        title(['t=',num2str(t),' s'])
        set(gcf,'color','w')
    end
        
