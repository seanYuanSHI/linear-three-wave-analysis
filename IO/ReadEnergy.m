% This function read total particle or field energy 
% assuming thet are contained in sdf files.
%
% Inputs:
%    its    : time slices to read file
%             e.g. its=[3,7], read '0003.sdf' and '0007.sdf'
%    vname  : variable name, choose from "particle","field"
%             otherwise read both  
%    path   : folder where sdf files are located
%    flag   : flag=1 plot figure
% Outputs:
%    t  : time corresponds to its in SI unit, nt-by-1
%    U  : energy at t in J, nt-by-1 if read one vname
%                           nt-by-1 if read both
%
% Written by Yuan SHI on June 27, 2022

function [t, U] = ReadEnergy(its, vname, path, flag)
    % default no plot
    if nargin<4
        flag = 0;
    end
    % check vname
    nv = 1;
    if strcmp(vname,'particle')
        disp('Reading particle energy')
    elseif strcmp(vname,'field')
        disp('Reading field energy')
    else
        disp('Unrecognized name, read both particle and field')
        nv = 2;
    end

    % initialize output
    nt = length(its);
    t = zeros(nt,1);
    U = zeros(nt,nv);

    % read each file
    for i=1:nt
        it = its(i);

        % file name
        fname = [path,num2str(it,'%04.f'),'.sdf'];
        %disp(['read data from: ',fname])
        % read data
        data = GetDataSDF(fname);
        t(i) = data.time;
     
        % value
        p = data.Total_Particle_Energy_in_Simulation_J;
        f = data.Total_Field_Energy_in_Simulation_J;
        if strcmp(vname,'particle')
            U(i) = p;
        elseif strcmp(vname,'field')
            U(i) = f;
        else
            U(i,:) = [p,f];
        end
    end    

    % plot figure
    if flag>0
        clf
        semilogy(t,U,'.-')
        xlabel('t (s)')
        ylabel('U (J/m^2)')
        %title('Total energy')
        title(path(end-25:end)) % last 25 characters
        set(gcf,'color','w')
        if nv==2
            legend('particle','field');
        else
            legend(vname);
        end
    end
        
