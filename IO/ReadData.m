% This function read and check epoch data for PostProcess.m
% for pump (iname=1) and seed (iname=2). Common parameters
% including the defaul file path are loaded from ParaSetup.m
%
% Only need to read data for steps 2,3,5
% step=2: linear-polarization run
%         laser front should be close to exist plasma, y,z components
% step=3: eigen-polarization run
%         laser front should be close to exist plasma, x,y,z components
% step=5: stimulated run
%         all data are relevant
%
% Inputs:
%    step  : select from 2,3,5
%    apath : struct containing all default paths
%            also contain time slice index apath.it
%    iname : select from 1 or 2
%    flag  : flag>0 plot data selection
% Output:
%    data : a struct that contains field components 
%           and wave frequency in Trad/s
% 
% Written by Yuan SHI on June 6, 2022

function data = ReadData(step, apath, iname, flag)
    %%%%%%%%%%%%%%%%%
    % threshold beyond which the laser 
    % is considered fully ramped up
    threshold = 0.95; % 0<threshold<1
    %%%%%%%%%%%%%%%%%
    if nargin<4
        flag=0; % default no plot
    end
    if iname==1
        if step==2 || step==3
            disp('Read pump data.')
        end
    else
        iname=2;
        if step==2 || step==3
            disp('Read seed data.')
        end
    end 
    % load common setup parameters
    ParaSetup
    % initialize output by loading wave frequency
    load(char(apath.paras(iname))) % para, wave1, wave2, theta
    data.w = wave1.w;
    % load index of the x origin
    data.i0 = left_lambda*cell_lambda + 1;

    % path to read data
    ifread = 1; % if read data
    ifB = 0; % if read B components
    % special case
    if step==4
        disp('Reading eigen data for RLmixer')
        ifB = 1;
        % read data in same way as step=3
        step=3;
    end
    % general cases
    if step==2 % linearly polarized run
        fpath = char(apath.PLSL(iname));
    elseif step==3 % eigen polarized run 
        fpath = char(apath.PESE(iname));
    elseif step==5 % stimulated run
        fpath = char(apath.SS);
        ifB = 1;
        disp('Read stimulated data.')
    else
        disp('Unnecessary to read epoch data at this step.')
        ifread = 0;
    end
 
    if ifread==1
        % time slice to read
        it = apath.it;

        % always read Ey and Ez 
        fprintf(['Reading it=',num2str(it),' data from \n',fpath,'\n'])
        % read simulation data
        [xm, fy, t] = ReadGrid(it,'Ey',fpath);
        [xm, fz, t] = ReadGrid(it,'Ez',fpath);
        % uniform x grid spacing
        dx = xm(2)-xm(1);
        % |E| for determining data region and plot
        Eabs = sqrt(fy.^2+fz.^2);
     
        % determine data region
        il = ileft;  % ileft from ParaSetup
        ir = iright; % iright from ParaSetup
        % For step=5, all data in plasma region are relevant
        % for step=2,3, only plasma-region data behind wave front 
        if step==2 || step==3
            disp('Searching for laser front')

            % running average on about twice wavelength 
            Eavg = RunAverage(Eabs,2*cell_lambda);
            % threshold scaled to data
            Eth = threshold*mean(Eavg);
            % twice safty grids
            ig = 2*safety_lambda*cell_lambda;
        
            if iname==1 % pump left-propagating
                % increase il to find pump front
                % first round crude search to see if front exists
                while Eavg(il)<Eth && il<ir
                    il = il+1;
                end

                if il==ileft 
                   disp('No pump wave front found, use data at earlier time!')
                else
                   % second round refined search with updated threshold
                   Eth = threshold*mean(Eavg(il:ir));
                   % further increase il
                   while Eavg(il)<Eth && il<ir
                       il = il+1;
                   end
                   % add safty grids
                   if il+ig<ir
                       il = il + ig;
                   else
                       disp('Valid data range too small, use data at later time!')
                   end
                end
            else % iname=2, seed right-propagating
                % decrease ir to find seed front
                % first round crude search to see if front exists
                while Eavg(ir)<Eth && ir>il
                    ir = ir-1;
                end

                if ir==iright 
                   disp('No seed wave front found, use data at earlier time!')
                else
                   % second round refined search with updated threshold
                   Eth = threshold*mean(Eavg(il:ir));
                   % further increase il
                   while Eavg(ir)<Eth && ir>il
                       ir = ir-1;
                   end
                   % add safty grids
                   if ir-ig>il
                       ir = ir - ig;
                   else
                       disp('Valid data range too small, use data at later time.')
                   end
                end
            end
        end
     
        % data in desired range
        Ey = fy(il:ir);
        Ez = fz(il:ir);
        % load data
        data.Ey = Ey;
        data.Ez = Ez;
        data.dx = dx;
        data.t  = t;
        data.it = it;
        data.il = il;
        data.ir = ir;
     
        % read Ex for step 3
        if step==3
            % also read Ex simulation data
            [xm, fx] = ReadGrid(it,'Ex',fpath);
            % data in desired range
            data.Ex = fx(il:ir);
        end

        % read By and Bz for step 4 and 5
        if ifB==1
            % also read By and Bz simulation data
            [xm, fy] = ReadGrid(it,'By',fpath);
            [xm, fz] = ReadGrid(it,'Bz',fpath);
            % data in desired range
            By = fy(il:ir);
            Bz = fz(il:ir);
        
            % load data
            data.By = By;
            % subtract DC field, B0 and theta from ParaSetup
            data.Bz = Bz - 1e2*para.B0*sin(theta/180*pi); 
        end
     
        % plot diagnostic figure
        if flag>0
            figure
            % plot all data
            plot(xm, Eabs,'b.-')
            hold on
            % plot selected data
            plot(xm(il:ir),sqrt(Ey.^2+Ez.^2),'c')
            % plot wave front identification
            if step==2 || step==3
                hold on
                plot(xm, Eavg,'k-')      
                legend('all data','chosen','run avg')           
                line(xlim,[Eth,Eth],'LineStyle','-.','Color','m','LineWidth',2)
            end
            % mark axis
            xlabel('x (m)')
            ylabel('|E| (V/m)')
            title([fpath,' it=',num2str(it)])
            set(gcf,'Color','w')

            % mark plasma boundaries
            ii = left_lambda*cell_lambda+1;
            line([xm(ii),xm(ii)],ylim,'LineStyle','--','Color','g','LineWidth',3)
            ii = (box_lambda-right_lambda)*cell_lambda;
            line([xm(ii),xm(ii)],ylim,'LineStyle','--','Color','g','LineWidth',3)
            % mark data boundaries
            line([xm(il),xm(il)],ylim,'LineStyle','-','Color','r','LineWidth',2)
            line([xm(ir),xm(ir)],ylim,'LineStyle','-','Color','r','LineWidth',2)
        end
    end
