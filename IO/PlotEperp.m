% This function diagnozes elliptical polarization is 
% as expected by plotting Ey and Ez (1) at a fixed x 
% for different t, or (2) at a fixed t for different x.
%
% Inputs:
%    its    : a list of file number to process, integers
%             (1) if length(its)>1, e.g. its=1:10, then 
%                 plot at fixed x for different t.
%             (2) if length(its)=1, e.g. its=1, then
%                 plot at fixed t for different x.
%    ixs    : spatial slices to plot, e.g. ixs=[1,3,7]
%             (1) each spatial slice will be plotted 
%                 in a separate figure
%             (2) plot in one figure 
%    path   : folder where sdf files are located
%    
% Written by Yuan SHI on Sep 14, 2021
% Last Modified by Yuan SHI on June 28, 2022

function PlotEperp(its, ixs, path)

    % number of files
    nt = length(its);
    % number of spatial slices
    nx = length(ixs);

    % initialize data
    Eyall = zeros(nx*nt,1);
    Ezall = zeros(nx*nt,1);
    tall = zeros(nt,1);

    % read and load data
    for i=1:nt
        it = its(i);
        % read data
        [xm,Ey,t] = ReadGrid(it, 'Ey', path, 0);
        [~,Ez,~] = ReadGrid(it, 'Ez', path, 0);
        % load data
        i0 = (i-1)*nx+1;
        i1 = i*nx;
        Eyall(i0:i1) = Ey(ixs);
        Ezall(i0:i1) = Ez(ixs);
        tall(i) = t;
    end
    % store max abs values
    Emax = max(sqrt(Eyall.^2+Ezall.^2));

    % decide plot (1) or (2)
    if nt==1
        % plot at fixed t in single figure
        nf = 1;
        stride = 1:nx;
        color = xm(ixs);
        % title of figure
        vname = 't = ';
        values = tall;
        units = ' s';
    else
        % plot at fixed x each in one figure
        nf = nx;
        stride = 1:nx:nx*nt
        color = its;
        % title of figure
        vname = 'x = ';
        values = xm(ixs);
        units = ' m';
    end

    % plot figure, color code by t or x
    cs = 36; % circle size
    for j=1:nf
        figure
        % select and plot data
        inds = stride+j-1;
        scatter(Eyall(inds), Ezall(inds), cs, color, 'filled')
        hold on
        % mark axis
        t=linspace(-Emax,Emax,100);
        plot(t,zeros(1,100),'-','Color',[0.5 0.5 0.5]) 
        plot(zeros(1,100),t,'-','Color',[0.5 0.5 0.5]) 
        % mark circle
        t=linspace(0,2*pi,100);
        x=Emax*cos(t);
        y=Emax*sin(t);
        plot(x,y,':','Color',[0.5 0.5 0.5])
        % mark axis
        xlabel('Ey (V/m)')
        ylabel('Ez (V/m)')
        title([vname,num2str(values(j)),units])
        set(gcf,'color','w')
    end

