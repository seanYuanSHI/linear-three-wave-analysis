% This function read 1D distribution function in sdf files
% The default file contents are:
%    px, f(px)
%
% Inputs:
%    its    : read files correspond to the it-th dump, length nt
%             e.g. it=3, read files '0003.sdf'
%    path   : folder where sdf files are located
%    flag   : flag=1 plot figure
% Outputs:
%    data of the last time slice
%    p  : momentum grid, np-by-nt
%    t  : time grid, 1-by-nt
%    f  : data, np-by-1
%
% Written by Yuan SHI, Sep 14, 2021

function [p, t, f] = Read1Ddist_fn(its, path, flag)

    % default no plot
    if nargin<3
        flag = 0;
    end
    if flag>0
        %clf
        figure % initialize new figure
    end
    % number of time slices
    nt = length(its);
    t = zeros(1,nt);

    for i=1:nt
        it = its(i);
     
        % file name
        fname = [path,num2str(it,'%04.f'),'.sdf'];
        % read data
        data = GetDataSDF(fname);
        % report time
        t(i) = data.time;

        % momentum grid
        p = data.dist_fn.px.electron.grid.x;
        % distribution function value, size will change
        f(:,i) = data.dist_fn.px.electron.data;   
     
        if flag>0
            % plot in linear and log scales
            for j=1:2
                subplot(1,2,j)
                plot(p,f(:,i),'.-','DisplayName',['it=',num2str(it)])
                hold all
                xlabel('px (kg*m/s)')
                ylabel('fe')
                title(['t=',num2str(data.time),' s'])
            end
            % set the second plot in log y
            set(gca,'YScale','log')
            % change title of 2nd plot to path
            title(path(end-25:end)) % last 25 characters
            set(gcf,'color','w')
        end
    end
    % figure legend
    hold off
    subplot(1,2,1)
    legend show
