% This function plot the max and mean of seed amplitude at give time
% to search for maximum coupling as a function of seed frequency
%
% Inputs:
%    it    : time slices to plot
%    rpath : path to the root folder
%    ws    : a list of seed wavelength in um

function PlotSeeds(it, rpath, ws)
    % number of w
    nw = length(ws);
    % initialize max
    a2max = zeros(size(ws));
    % initialize mean
    %a2mean = zeros(size(ws));
    % sort w
    sws = sort(ws);

    for i = 1:nw
       w = sws(i);
       % load data
       spath = [rpath,num2str(i)];
       fname = [spath,'/Stimulated/Envelope/',num2str(it,'%04.f'),'.mat'];
       load(fname)
       % seed amplitude
       a2max(i) = max(EA.ar);
       %a2mean(i) = mean(EA.ar);
   end

   % plot figure
   figure('Name',num2str(it))
   plot(sws,a2max,'r.-')
   %hold on
   %plot(sws,a2mean,'k.-')
   xlabel('wave length (um)')
   ylabel('max (a2)')
   title(['t=',num2str(EA.time),' s'])
   set(gcf,'Color','w')

