% This script is used to loop PostProcess.m
% over scan parameters, such as B and theta
% as well as seed wavelength lambda. 
clear % clear all variables in workspace

% select pump, scan seed, resonant seed 
%     list=[]        : loop over scan parameters for pump runs
%     list=1         : loop over scan parameters for resonant seed runs  
%     list=[1,2,...] : also loop over seed wavelengths L1, L2,...
list = [];
%list = 1:27;
%list = 1;

% steps for PostProcess
%   Positive integers 1,2,... are for PostProcess and further process.
%   Half integer 1.5, 2.5,... steps are for further process only.
%   Special case step=-6 for printing resonant seed wavelengths after fitting
steps = [1,2];
%steps = [3,4];
%steps = [5,6];
%steps = 7;
%steps= 8; % need to check 'its' selection
%steps = 9; % 'its' does not matter

% time slices for PostProcess
its = 1;
%its = 12:22;
%its = 13:22;
%its = 13:16;

% flag for PostProcess
flag=0;
%flag=1;
%flag=2;

% scan variable: 'B','theta','T','N'
%vname = 'T'        % in eV
vname = 'B';      % in MG
%vname = 'theta';  % in deg
%vname = 'I'

% scan values
values = [0,30];
%values = 0:2:40;
%values = 0:5:85;
%values = [5,10,20,40,80];
%values = [0.625,1.25,2.5,5,10];

% file head fhead = <fpath><vname><value>/
fpath = '~/epoch/epoch1d/Data/examples/Bscan/';
%fpath = '~/epoch/epoch1d/Data/LinearThreeWave/Production/Tscan/fine/';
%fpath = '~/epoch/epoch1d/Data/LinearThreeWave/Production/Bscan/fine/';
%fpath = '~/epoch/epoch1d/Data/LinearThreeWave/Production/thetaScan/fine/';
%fpath = '~/epoch/epoch1d/Data/LinearThreeWave/Production/Iscan/I2scan/';

% shead = <branch> when nl=1
% shead = <branch>L<k> when nl>1
% branch is of the form 'b3-'<b3>
branch = 'b3-3';
%branch = 'b3-4';
%branch = '';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% pump wavelength in um
pump_lambda = 1;
% seed wavelength scan additional points
nseeds = 6; % total points is 3+na2
% seed wavelength scan radius in Trad/s
radius = 15;
% 1D scan axis: 'r0' or 'mu3'
axis = 'r0'; % default in PostProcess.m
%axis = 'mu3'; % will not run, only collect existing results

% number of lambdas
nl = length(list);
% number of steps
ns = length(steps);
% number of scans
nv = length(values);

% decide how to load lambda
% the value of lambda is significant only for step=1
lambdas = ones(1,max(nv,nl)); % defaul lambda>0 for seed
if nl==0
    disp('Loop over scan parameters for pump runs') 
    lambdas = -pump_lambda*ones(1,nv); % pump lambda<0
elseif nl==1
    disp('Loop over scan parameters for resonant seed') 
    if steps(1)==1 % value of lambda is sigificant
        % read resonant seed wavelengths
        fname = [fpath,'rSeedLambda2s_',branch,'.txt'] % print
        fid = fopen(fname,'r');
        lambdas = fscanf(fid,'%f')'; % transpose
        fclose(fid);
        % check length
        if length(lambdas)~=nv
            disp('number of resonant seeds in file differs from # values!')
        end
    end
else
    disp('Loop over scan parameters and seed wavelengths') 
end

for i=1:ns % loop over steps
    disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
    step = steps(i);
    istep = floor(abs(step)); % positive integer step
    for j=1:nv % loop over scan parameters
        disp('%%%%%%%%%%%%%%%%')
        vs = [vname,num2str(values(j))];
        fhead = [fpath,vs,'/'];
        if nl<=1 % pump or resonant seed
            if step==istep % PostProcess needed
                PostProcess(flag,fhead,lambdas(j),its,istep,branch,vs);
            end

            % after running pump, print seed lambdas to scan
            if nl==0 && istep==4
                % generate for first three resonant branches    
                for ib=1:3
                    % scale=-radius
                    SeedScan(fhead, ib, nseeds, -radius);
                end
            end 

            % after stimulated, check pump depletion
            if nl==1 && istep==6
                PlotAs([fhead,branch,'/'],its(end)) % plot last
                PlotAs([fhead,branch,'/'],its,j) % j>1: combine all pumps
            end

        else % non-resonant seeds
            if step==istep % PostProcess needed
                if step==1 % value of lambda is significant
                    % read seed wavelengths to scan
                    fname = [fhead,'lambda2s_',branch,'.txt'] % print
                    fid = fopen(fname,'r');
                    lambdas = fscanf(fid,'%f')'; % transpose
                    fclose(fid);
                end

                % loop over seed wavelengths
                for k=1:nl 
                    lk = list(k);
                    shead = [branch,'L',num2str(lk)]; 
                    PostProcess(flag+1-k,fhead,lambdas(lk),its,istep,shead,vs);
                end
            end

            % after running seed linear, print seed psi
            if istep==2
                 disp(['Collecting psi for scanned seed wavelength at ',vs])
                 % default file name
                 oname = [fhead,'SeedPsis_',branch,'.txt']; 
                 % print psi, 1: smaller psi, 2: larger psi
                 PrintPsis([fhead,branch,'L'],list,'/SeedLinear',1,oname)
            end

            % after extracting envelopes, fit resonant seed lambda
            if istep==6 && step>0
                % plot first envelope at last time
                PlotAs([fhead,branch,'L1/'],its(end)) 

                disp('Fitting resonant seed wavelength...')
                % 1: always plot figure for check up         
                % use last 4 slice for fit  
                FitSeedResonance(its(end-3:end),fhead,branch,1);
                
                % mark readiness for print resonant seed lambdas
                if j==nv
                    step = -istep;
                end
            end
        end % if pump, seed, or resonant
    end % loop scan parameter
    
    % after linear runs, print pump and resonant psis
    if istep==2
        if nl==0 % print pump psi
           disp('Collecting pump psi pump...')
           % default file name
           oname = [fpath,'PumpPsis.txt']; 
           % print psi, 1: smaller psi, 2: larger psi
           PrintPsis([fpath,vname],values,'/PumpLinear',2,oname)
        elseif nl==1 % print resonant seed psi
           % default file name
           oname = [fpath,'rSeedPsis_',branch,'.txt']; 
           disp(['Collecting psi resonant seeds in ',oname])
           % print psi, 1: smaller psi, 2: larger psi
           PrintPsis([fpath,vname],values,['/',branch,'/SeedLinear'],1,oname)
        end
    end

    % after fit resonances, print resonant seed lambdas
    if step==-6 && nl>1
        oname = [fpath,'rSeedLambda2s_',branch,'.txt'];
        disp(['Collecting resonant seed wavelengths in ',oname])
        PrintLambda2s([fpath,vname], values, branch, oname)

        disp('Collectinng analytic/numeric wavelengths')
        PrintAnaNumW2s(fpath, vname, values, branch)
    end

    % after fit scan, print stats
    if istep==9
        oname = [fpath,'1Dstat_',branch,'_',axis,'.txt'];
        disp(['Collecting fit uncertainty statistics in', oname])
        Print1Dstat([fpath,vname],values,branch,axis,oname)
    end

end % loop steps
