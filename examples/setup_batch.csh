#!/bin/csh
### This script set up default batch epoch runs 
### by entering working folders and changing input.deck
### for a list of modifier values
#
### Allow loop over scan parameters and seed wavelengths
#
### The folder name contains fvalue 
### The input.deck contains keys with kvalue 
### kvalue is equals to fvalue or read from file 
### When read from file, each row matches an fvalue
#
### step = 
###  a0: setup pump folder and linear run
###  b0: setup pump eigen run            
###  a1: setup seed folder and linear run
###  b1: setup seed eigen run            
###  c:  setup stimulated run     
###  d:  delete unused stimulated outputs       
### other: dry run for testing fvalues
set step = a0 
### resonance branch label
set BRANCH = b3-3

### directory where epoch /bin executable is located
set EPOCHDIR = $HOME/epoch/epoch1d

### working folder name is <BASEDIR>/<BHEAD><bvalue>/<FHEAD><fvalue><FTAIL>
set BASEDIR = $EPOCHDIR/Data/Analysis/examples/Bscan

### should leave BHEAD and bvalue empty for pump and resonant runs 
set BHEAD = 
### FHRAD and FTAIL can leave empty
set FHEAD = B
set FTAIL = 

### modify variable named KEY in input.deck for step a0
set key = B0_strength
### old value for key in template file
set ovalue = 30

### specify bvalue array
set barray = ()
### if empty, values from simple loop
### modifier values
set b0 = 2
set db = 2
### nb used only when varray is empty
### if empty and nb=0, bvalue will be omitted
set nb = 0

### specify fvalue array
### length should match .txt file when used
set farray = (0 30)
### if empty, values from simple loop
### modifier values
set f0 = 1
set df = 1
### nf>0 used only when varray is empty
set nf = 9

### unused stimulated outputs are 
### 0000.sdf,...,00<unused>.sdf
### The file name is 4-digit long
### and padded with zeros. The default 
### setup in template is unused=11
set unused = 11

########################################
### The following parameters are usually unchanged
### pump laser intensity in units 1e14 W/cm^2
set Ipump = 10
### seed laser intensity in units 1e14 W/cm^2
set Iseed = 0.05
### for stimulated run, number of full dump
set nfull = 20
### for stimulated run, number of partial dump per full dump
set ndump_full = 1
### Original values for nfull is n0, for ndump_full is nf0
set n0 = 2
set nf0 = 1

### in input.deck, intensity name is trailed with        
set IN = _intensity_w_cm2
### linear polarization along z or diagonal in y-z
set PA = polarisation_angle


########################################
### load barray if empty 
set bi = 0
set blen = ${#barray}

if ($blen > 0) then
    echo "base values from array"
    set nb = $blen
    echo "nb set to $nb"
else
    if ($nb>0) then
        echo "base values from loop"
        ### set up barray
        while ($bi < $nb)
            ### float arithamtics using bc
            set b = `echo "$b0 + $bi * $db"|bc`
            ### append to array
            set barray = (${barray} $b)
            ### integer arithmatics in shell
            ### the space after @ is significant
            @ bi++
        end
        ### reset bi to 0
        set bi = 0
    else
        echo "base value will be omitted"
        set bi = -1
    endif
endif
echo "base values are: ${barray}"

### check farray if empty 
set fi = 0
set flen = ${#farray}

if ($flen == 0) then
    echo "file name values from loop"
    ### set up farray
    while ($fi < $nf)
        ### float arithamtics using bc
        set f = `echo "$f0 + $fi * $df"|bc`
        ### append to array
        set farray = (${farray} $f)
        ### integer arithmatics in shell
        ### the space after @ is significant
        @ fi++
    end
else
    echo "file name values from array"
    set nf = $flen
    echo "nf set to $nf"
endif
echo "file name values are: ${farray}"


########################################
### loop over base values
while ($bi < $nb)
    ### reset fi to 0
    set fi = 0
    ### integer arithmatics in shell
    ### the space after @ is significant
    @ bi++
    if ($bi <= 0) then
        ### b is left empty
        set b = 
    else
        set b = $barray[$bi]
    endif
    echo "base value is: $b"

    ### load kvalue
    echo "Loading key value..."
    if ($step == b0) then
       set fname = $BASEDIR/PumpPsis.txt
       echo "Read pump psi from $fname"
       set kvalues = `cat $fname`
    
    else if ($step == a1) then
        if ($nb == 0) then
            set fname = $BASEDIR/rSeedLambda2s_$BRANCH.txt
            echo "Read resonant seed lambda from $fname"
            set kvalues = `cat $fname`
        else
            set fname = $BASEDIR/$BHEAD$b/lambda2s_$BRANCH.txt
            echo "Read seed scan lambda from $fname"
            set kvalues = `cat $fname`
        endif

    else if ($step == b1) then
        if ($nb == 0) then
            set fname = $BASEDIR/rSeedPsis_$BRANCH.txt
            echo "Read resonant seed psi from $fname"
            set kvalues = `cat $fname`
        else
            set fname = $BASEDIR/$BHEAD$b/SeedPsis_$BRANCH.txt
            echo "Read seed scan lambda from $fname"
            set kvalues = `cat $fname`
        endif
    else
        ### default kvalue same as fvalue
        echo "Use file value as key value."
        set kvalues = ($farray)
    endif
    set nk = ${#kvalues}

    ### loop over file values
    while ($fi < $nf)
        ### integer arithmatics in shell
        ### the space after @ is significant
        @ fi++
        set f = $farray[$fi]
        if ($nk == $nf) then
            set kv = $kvalues[$fi]
        else
            ### f is used as index
            set kv = $kvalues[$f]
        endif
        echo "file value = $f"
        echo "key value  = $kv"

        ### working directory
        set WDIR = $BASEDIR/$BHEAD$b/$FHEAD$f$FTAIL

        ### create pump folders 
        if ($step == a0) then
            echo "set up pump folders"
            mkdir $WDIR
            cp -rf $EPOCHDIR/Data/template/PumpLinear $WDIR/. 
            cp -rf $EPOCHDIR/Data/template/PumpEigen $WDIR/. 
        endif
     
        ### create seed folders 
        if ($step == a1) then
            echo "set up seed folders"
            mkdir $WDIR
            cp -rf $EPOCHDIR/Data/template/seed_template/* $WDIR/. 
        endif
     
        ### set up pump linear run
        if ($step == a0) then
            echo "Set up pump linear run"
            ### enter working directory
            cd $WDIR
            set iname = ./PumpLinear/input.deck
     
            ### modify input.deck carried from template. "-i": in place 
            sed -i "s/$key = $ovalue/$key = $kv/" $iname
            ### change linear polarisation from pi/2 for z to pi/4 for y-z
            ### "|" as separator to avoid clash with "/"
            sed -i "s|$PA = pi/2|$PA = pi/4|" $iname
        endif
     
        ### set up pump eigen run
        if ($step == b0) then
            echo "Set up pump eigen run"
            ### enter working directory
            cd $WDIR
            set iname = ./PumpEigen/input.deck
     
            ### inherit from pump linear run, \cp to bypass alias cp -i
            \cp -f ./PumpLinear/input.deck ./PumpEigen/.
            ### change psi by values in file
            echo "psi = $kv"
            sed -i "s/laser_psi = 0/laser_psi = $kv/" $iname
            ### change second field component back to z
            sed -i "s|$PA = pi/4|$PA = pi/2|" $iname
        endif
     
        ### set up seed linear run
        if ($step == a1) then
            echo "Set up seed linear run"
            ### enter working directory
            cd $WDIR
            set iname = ./SeedLinear/input.deck
     
            ### inherit from pump eigen run
            \cp -f ../PumpEigen/input.deck ./SeedLinear/.
     
            ### turn off pump and turn on seed 
            sed -i "s/laser$IN = $Ipump/laser$IN = 0.0/" $iname
            sed -i "s/seed$IN = 0.0/seed$IN = $Iseed/" $iname
     
            ### modify seed wavelength through key:value
            sed -i "s/seed_lambda = 1.1/seed_lambda = $kv/" $iname
            ### change linear polarisation z to pi/4 in y-z
            sed -i "s|$PA = pi/2|$PA = pi/4|" $iname
        endif
        
        ### set up seed eigen run
        if ($step == b1) then
            echo "Set up seed eigen run"
            ### enter working directory
            cd $WDIR
            set iname = ./SeedEigen/input.deck
     
            ### inherit from seed linear run
            \cp -f ./SeedLinear/input.deck ./SeedEigen/.
            ### change psi by values in file
            echo "psi = $kv"
            sed -i "s/seed_psi = 0/seed_psi = $kv/" $iname
            ### change second field component back to z
            sed -i "s|$PA = pi/4|$PA = pi/2|" $iname
        endif
     
        ### set up stimulated run
        if ($step == c) then
            echo "Set up stimulated run"
            ### enter working directory
            cd $WDIR
            set iname = ./Stimulated/input.deck
     
            ### inherit from seed eigen run
            \cp -f ./SeedEigen/input.deck ./Stimulated/.
     
            ### turn pump back on 
            sed -i "s/laser$IN = 0.0/laser$IN = $Ipump/" $iname
     
            ### set simulation to full time
            sed -i "s/t_frac = 0.45/t_frac = 1.0/" $iname
            sed -i "s/s_frac = 0.0/s_frac = 1.0/" $iname
            
            ### request more frequent output
            sed -i "s/nfull = $n0/nfull = $nfull/" $iname
            sed -i "s/ndump_full = $nf0/ndump_full = $ndump_full/" $iname
        endif

        ### clean up unused data
        if ($step == d) then
            echo "Clean up stimulated data"
            ### enter working directory
            cd $WDIR/Stimulated
            ### remove files
            foreach ir (`seq -f "%04g" 0 $unused`)
                echo "removing " $ir.sdf
                rm -rf $ir.sdf
            end
        endif
    end # while over file value
end # while over base value


##### "foreach loop" usage
### foreach variable (worldlist)
###    commands
### end

##### "while loop" usage
### while (condition)
###    commands
### end

##### "if statement" usage
### if ($step == a1) then
###     commands
### else if (expr) then
###     commands
### else
###     commands
### endif
