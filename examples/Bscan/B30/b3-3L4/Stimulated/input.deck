# This input deck is for 1D simulation
# of stimulated laser backscattering. 
# A pump laser enters from right, and
# a seed laser enters from left.
# The simulation ends after twice laser
# transit time in vacuum.
# Users only need to change the constant block

begin:constant
  ############################
  ## resolution parameters
  # particles per cell
  part_cell = 100
  # cell per wavelength
  cell_lambda = 40

  # number of momentum grids for output
  presolution = 50


  ############################
  ## domain parameters 
  ## in units of pump wavelength
  # box size 
  box_lambda = 100
  # left vacuum gap 
  left_lambda = 10
  # right vacuum gap
  right_lambda = 10  

  # the end time of the simulation 
  # is t_frac*2*t_transit, where
  # t_transit is the laser transit time
  t_frac = 1.0
  # pump laser starts at time=0
  # seed laser starts at time=t_transit*s_frac
  s_frac = 1.0
  # ramp up using tanh profile
  # exponential width in unit of wavelength
  ramp_frac = 0.5


  ############################
  ## output
  # expected total number of full dumps
  nfull = 20
  # number of regular dumps per full dump
  ndump_full = 1

  # momentum range for nonrelativistic electrons
  # behavior abnormal if smaller than 5e-22
  ep_range = 1.0e-23 # SI unit, me*c=2.7e-22


  ############################
  ## plasma parameters 
  plasma_density = 0.1e26 # per cubic meter
  plasma_temperature_ev = 10 # in eV

  ## Background B field in x-z plan 
  B0_strength = 30e2 # SI unit
  # B is at angle w.r.t the x=k axis
  B0_angle = 30/180*pi # in radians

  ############################
  ## pump laser parameters in vacuum 
  laser_lambda = 1.0*micron
  laser_intensity_w_cm2 = 10e14 # in W/cm^2 
  # elliptic polarization angle
  # psi =  0,    linear along z
  # psi =  pi/2, linear along y
  # psi =  pi/4, right-handed circular about k
  # psi = -pi/4, left-handed circular about k
  laser_psi = 2.328535944469238 # in radians 

  ## seed laser parameters in vacuum
  seed_lambda = 1.411968967397175*micron
  seed_intensity_w_cm2 = 0.05e14
  seed_psi = 0.815040597180087 


  ############################
  ## derived parameters
  laser_omega = 2.0*pi*c/laser_lambda
  laser_period = laser_lambda/c

  seed_omega = 2.0*pi*c/seed_lambda
  seed_period = seed_lambda/c

  # laser transit time across the simulation box
  t_transit = box_lambda*laser_lambda/c
  # seed laser start time
  s_start = s_frac*t_transit
  # plasma length
  plasma_lambda = box_lambda - left_lambda - right_lambda
  # right of plasma slab is at
  plasma_right = plasma_lambda*laser_lambda 

  # total number of cells
  ncells = box_lambda*cell_lambda
  # estimated total number of time steps
  nt_total = 2*ncells
  # number of time steps per full dump
  nt_full = nt_total/nfull
  # number of time steps per regular dump
  nt_dump = nt_full/ndump_full

end:constant

###############################################
# The following do not need to be changed 
# under normal circumstances
###############################################
begin:control
  nx = ncells

  # Size of domain
  x_min = -left_lambda*laser_lambda
  x_max = x_min + box_lambda*laser_lambda

  # Final time of simulation
  # twice laser transit time
  t_end = 2*t_transit*t_frac

  # print std every # time steps
  stdout_frequency = nt_dump
end:control


begin:boundaries
  bc_x_min = simple_laser
  bc_x_max = simple_laser
end:boundaries

# background fields
# default value is zero
begin:fields
    bx = B0_strength*cos(B0_angle)
    bz = B0_strength*sin(B0_angle)
end:fields

###############################################
# Laser blocks
###############################################
# pump laser y component
# left propagating wave enter from right
# rounded square pulse
begin:laser
  boundary = x_max
  intensity_w_cm2 = laser_intensity_w_cm2
  lambda = laser_lambda

  # laser electric field driven by 
  # profile*sin(w*t+phase)
  phase = 0 
  # center at 10*ramp_width
  t_profile = sin(laser_psi)*(1+tanh(time/2/ramp_frac/laser_period-5))/2

  t_start = start # at begining of simulation
  t_end = end # at end of simulation
end:laser


# pump laser z component
# left propagating wave enter from right
# rounded square pulse
begin:laser
  boundary = x_max
  intensity_w_cm2 = laser_intensity_w_cm2
  lambda = laser_lambda

  # laser electric field driven by profile*sin(w*t+phase)
  polarisation_angle = pi/2 # in radians, 0 along y
  phase = pi/2 
  # center at 10*ramp_width
  t_profile = cos(laser_psi)*(1+tanh(time/2/ramp_frac/laser_period-5))/2

  t_start = start # at begining of simulation
  t_end = end # at end of simulation
end:laser


# seed laser y component
# right propagating wave enter from left
# rounded square pulse
begin:laser
  boundary = x_min
  intensity_w_cm2 = seed_intensity_w_cm2
  lambda = seed_lambda

  # laser electric field driven by profile*sin(w*t+phase)
  phase = 0 
  # center at 10*ramp_width after s_start
  t_profile = sin(seed_psi)*(1+tanh((time-s_start)/2/ramp_frac/seed_period-5))/2

  t_start = s_start # after seed start time
  t_end = end # at end of simulation
end:laser


# seed laser z component
# left propagating wave enter from right
# rounded square pulse
begin:laser
  boundary = x_min
  intensity_w_cm2 = seed_intensity_w_cm2
  lambda = seed_lambda

  # laser electric field driven by profile*sin(w*t+phase)
  polarisation_angle = pi/2 # in radians, 0 along y
  phase = pi/2 
  # center at 10*ramp_width after s_start
  t_profile = -cos(seed_psi)*(1+tanh((time-s_start)/2/ramp_frac/seed_period-5))/2

  t_start = s_start # after seed start time
  t_end = end # at end of simulation
end:laser


###############################################
# Plasma blocks
###############################################
begin:species
 # Electron
 name = electron
 charge = -1.0
 mass = 1.0
 nparticles = nx*part_cell

 number_density = if((x gt 0) and (x lt plasma_right), plasma_density, 0.0)
 number_density_min = 0.1*plasma_density # no pseudo particle below this 
 
 temperature_ev = plasma_temperature_ev
end:species


begin:species
  # Protons
  name = proton
  charge = 1.0
  mass = 1837
  nparticles = nx*part_cell
  
  number_density = number_density(electron)
  number_density_min = 0.1*plasma_density
  
  temperature_ev = plasma_temperature_ev
end:species


###############################################
# Output blocks
###############################################
begin:output
  nstep_snapshot = nt_dump 
  full_dump_every = ndump_full
 
  # Properties on grid
  grid = always
  ex = always
  ey = always
  ez = always 
  bx = always
  by = always
  bz = always 
 
  number_density = always + species
 
  # Other variables
  total_energy_sum = always
  distribution_functions = always

end:output

# electron distribution
begin:dist_fn
  name = px
  ndims = 1
  dumpmask = full

  direction1 = dir_px
  range1 = (-ep_range, ep_range)
  #range1 = (0,0) # auto range
  resolution1 = presolution

  include_species:electron
end:dist_fn

