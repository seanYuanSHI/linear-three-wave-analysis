[[_TOC_]]

# Overview
This suite of program is used to process backscattering data from the [epoch1d](https://github.com/Warwick-Plasma/epoch) code and compare them with analytical growth rates calculated by program [Three-Wave](https://gitlab.com/seanYuanSHI/three-wave-matlab). The problem under investigation is the linearized three-wave equations 
```math
(\partial_t+v_2\partial_x+\mu_2)a_2 = \gamma_0 a_3,\\
(\partial_t-v_3\partial_x+\mu_3)a_3 = \gamma_0 a_2,\\
```
where the group velocities $`v_2>0, v_3\ge 0`$
, the damping rates $`\mu_2,\mu_3\ge 0`$
, and the growth rate $`\gamma_0>0`$ 
is proportional to the pump laser amplitude $`a_1`$,
which remains approximately a constant during the linear regime of three-wave interactions. 
To extract parameters in the above equations, we solve the initia-boundary value problem $`(x\ge 0, t\ge 0)`$
specified by conditions
```math
a_2(x\ge0,t=0)=0,\\
a_3(x\ge0,t=0)=0, \\
a_2(x=0,t>0)=h_0,
```
where $`h_0`$ is the constant incident seed amplitude. The analytical solutions to the initia-boundary value problem can be written as
```math
a_2 = h_0 e^{-\mu_2x/v_2}(1+\Delta_1),\\
a_3 = h_0 e^{-\mu_2x/v_2} \Delta_0.
```
The analytical solution for $`a_2`$
is used to fit [epoch1d](https://github.com/Warwick-Plasma/epoch) simulation data. 

The main driver of this program suite is [PostProcess.m](PostProcess.m), which processes one [epoch1d](https://github.com/Warwick-Plasma/epoch) run at one analysis step. To process multiple runs at multiple steps, use [batch_PostProcess.m](batch_PostProcess.m). Parameters for the simulation setup, which should match [input.deck](examples/template/PumpLinear/input.deck) of [epoch1d](https://github.com/Warwick-Plasma/epoch), are stored in [ParaSetup.m](ParaSetup.m). Default file paths are stored in [PathSetup.m](PathSetup.m).

Running this program requires that data from [epoch1d](https://github.com/Warwick-Plasma/epoch) are generated and stored in 
default file paths specified in [PathSetup.m](PathSetup.m). Also required is adding the folders [Three-Wave/src/](https://gitlab.com/seanYuanSHI/three-wave-matlab) and [epoch/SDF/Matlab](https://github.com/Warwick-Plasma/epoch) to the _MATLAB_ search path, from which functions and constants will be called.

This program is reviewed and released under LLNL-MI-839313.

# Program Descriptions
Functions contained in subfolders of this program are summarized below in alphabetic order. Only major features are described here. See documentations in each function for additional details. 

## IO
This folder contains functions that read or write data and perform simple analysis during the input/output process.

- [FitSeedResonance.m](IO/FitSeedResonance.m): Reads envelopes and seed wavelengths, fits using Lorentzian, and writes results to file. 

- [PlotAs.m](IO/PlotAs.m): Reads and plots pump and seed envelopes.

- [PlotEperp.m](IO/PlotEperp.m): Reads and plots transverse electric fields.

- [PlotSeeds.m](IO/PlotSeeds.m): Similar to [FitSeedResonance.m](IO/FitSeedResonance.m), but without fitting and writting.

- [Print1Dstat.m](IO/Print1Dstat.m): Gathers saved outputs of [FitScan1D.m](steps/FitScan1D.m) and write results of multiple runs into a single file. 

- [PrintAnaNumW2s.m](IO/PrintAnaNumW2s.m): Gathers resonant seed frequencies from analytical and numerical results, and print to file.

- [PrintLambda2s.m](IO/PrintLambda2s.m): Gathers outputs of [FitSeedResonance.m](IO/FitSeedResonance.m) and prints resonant seed wavelengths to a single file.

- [PrintPsis.m](IO/PrintPsis.m): Gathers saved outputs of [EigenPsi.m](steps/EigenPsi.m), corrects outliers, and prints them to a single file. 

- [Read1Ddist_fn.m](IO/Read1Ddist_fn.m): Reads and plots 1D distribution function from [epoch1d](https://github.com/Warwick-Plasma/epoch) sdf files.

- [ReadData.m](IO/ReadData.m): Reads and checks [epoch1d](https://github.com/Warwick-Plasma/epoch) data for [PostProcess.m](PostProcess.m) at given analysis step. 

- [ReadEnergy.m](IO/ReadEnergy.m): Read total particle or field energy from [epoch1d](https://github.com/Warwick-Plasma/epoch) sdf files.

- [ReadGrid.m](IO/ReadGrid.m): Reads and plots grid-based variables in [epoch1d](https://github.com/Warwick-Plasma/epoch) sdf files.

- [SeedScan.m](IO/SeedScan.m): Reads output of [Resonances.m](steps/Resonances.m) and prints a list of seed wavelengths near expected resonances.


## analytic
This folder contains functions that calculate analytical solutions to the linearized three-wave equations. 

- [Delta.m](analytic/Delta.m): Calculates $`\Delta_0, \Delta_1`$ functions at multiple time slices. This is the main wrapper function for [analytic](##analytic).

- [DeltaLimit.m](analytic/DeltaLimit.m): Calculates $`\Delta_0, \Delta_1`$ 
in the limit $`v_3=0, \mu_3=0`$.

- [Dkernal.m](analytic/Dkernal.m): Calculates the integrand of $`\Delta_0, \Delta_1`$
, which involves $`M_0, M_1`$ functions.

- [Mkernal.m](analytic/Mkernal.m): Calculates the $`M_0, M_1`$ functions.

- [dM.m](analytic/dM.m): Calculates the integrand of $`M_0, M_1`$.

- [tDelta.m](analytic/tDelta.m): Calculates $`\Delta_0, \Delta_1`$ functions at a single time slice. 

- [tDeltaLimit.m](analytic/tDeltaLimit.m): Calculates $`\Delta_0, \Delta_1`$
 functions at a single time slice in the limit $`v_3=0`$. $`\mu_3`$ can be nonzero.


## prep
This folder contains trivial functions that facilitates major analysis steps.

- [LoadAK.m](prep/LoadAK): Loads amplitudes and wavevectors extracted from pump and seed eigen runs.

- [LoadWFv2.m](prep/LoadWFv2.m): Loads $`v_2`$ from processed data to avoid loading variable into static work space.

- [ProcessInputs.m](prep/ProcessInputs.m): Parses inputs for [PostProcess.m](PostProcess.m).


## steps
This folder contains functions for major steps of the post processing.

- [AnaPara.m](steps/AnaPara.m): Computes expected parameters in three-wave equations for [FitLinear3Wave.m](steps/FitLinear3Wave.m) using [Three-Wave/src](https://gitlab.com/seanYuanSHI/three-wave-matlab).

- [EigenExtract.m](steps/EigenExtract.m): Extracts eigen mode parameters. Uses [NDsearch.m](steps/NDsearch.m) to separate two eigen modes. Uses [Envelope.m](steps/Envelope.m) to extract mode amplitudes.

- [EigenPsi.m](steps/EigenPsi.m): Estimates polarization angles for elliptically polarized eigen modes. Uses [NDsearch.m](steps/NDsearch.m) to separate two eigen modes and uses [PhaseShift.m](utilities/PhaseShift.m) to correct signs.

- [Envelope.m](steps/Envelope.m): Extracts envelope using numerical lock-in amplifier.

- [FitLinear3Wave.m](steps/FitLinear3Wave.m): Fits parameters of linearized three-wave equations using [FitResidual.m](steps/FitResidual.m) as objective function.

- [FitResidual.m](steps/FitResidual.m): Calculates fit residual between extracted seed envelopes and analytical solution [Delta.m](analytic/Delta.m).

- [FitScan1D.m](steps/FitScan1D.m): Quantifies fit uncertainty by scanning the fit residual as a function of $`\gamma_0`$
 or $`\mu_3`$.

- [FitScan2D.m](steps/FitScan2D.m): Quantifies fit uncertainty by scanning the fit residual on a rectangular grid in the $`(\gamma_0, \mu_3)`$ plane.

- [FitScan2D_ellipse.m](steps/FitScan2D_ellipse.m): Similar to [FitScan2D.m](steps/FitScan2D.m), but scans using an elliptical grid.

- [NDsearch.m](steps/NDsearch.m): Optimizes nearly degenerate wavevectors for co-propagating eigen modes. Uses [NearDegenerate.m](steps/NearDegenerate.m) as the objective function.

- [NearDegenerate.m](steps/NearDegenerate.m): Extracts constant amplitudes of two modes with known wavevectors using linear regression, and computes reconstruction residual.

- [RLenvelope.m](steps/RLenvelope.m): Extracts right/left moving wave envelopes using [Envelope.m](steps/Envelope.m).

- [RLmixer.m](steps/RLmixer.m): Sums pump-eigen-only and seed-eigen-only data, and optimizes for phase velocities that separates them.

- [RLsearch.m](steps/RLsearch.m): Similar to [RLmixer.m](steps/RLmixer.m), but uses either pump-eigen-only or seed-eigen-only data. 

- [Resonances.m](steps/Resonances.m): Solves for three-wave resonances using [Three-Wave/src](https://gitlab.com/seanYuanSHI/three-wave-matlab).

- [RightLeft.m](steps/RightLeft.m): Extracts left and right moving waves from electric and magnetic fields data.

- [WaveFront.m](steps/WaveFront.m): Gathers extracted envelopes, identifies wave fronts to fit for $`v_2`$, and normalizes simulation data to analytical frame so that fit residuals can be computed.


## utilities
This folder contains non-trivial functions for major steps of the post processing, as well as utility functions that are not frequently used but maybe of diagnostic interest to users.

- [Fourier.m](utilities/Fourier.m): Computes and plots FFT with correct wavevector units.
 
- [PhaseShift.m](utilities/PhaseShift.m): Corrects for phase jumps for [EigenPsi.m](steps/EigenPsi.m), such that the $`z`$ signal leads the $`y`$ signal by $`\pi/2`$ phase. 

- [RLvacuum.m](utilities/RLvacuum.m): Calculates right/left waves in vacuum gaps of [epoch1d](https://github.com/Warwick-Plasma/epoch) simulations to check reflection/transmission.

- [RunAverage.m](utilities/RunAverage.m): Calculates running average of an array for a given window size.

- [SelectSlices.m](utilities/SelectSlices.m): Converts time index in [PostProcess.m](PostProcess.m) to index in WF array, which is generated by [WaveFront.m](steps/WaveFront.m) and used by [FitResidual.m](steps/FitResidual.m).

- [StepFinder.m](utilities/StepFinder.m): Locates sudden drops of $`a_2`$ for [WaveFront.m](steps/WaveFront.m).


# Workflow 
Use pump-only runs to launch a pump wave in its numerical eigen mode. Use seed-only runs to launch a much less intense seed laser in its numerical eigen mode. The pump and seed interacts in stimulated runs, and the interaction is strongest when their beat wave resonantly excites a plasma eigen mode. Locate the numerical plasma eigen mode by scanning the seed wavelength. Finally, use stimulated data at resonant wavelength to extract parameters of the linearized three-wave equations. 

Follow steps below to generate data and postprocess results. 
Alphabetical steps a,b,... are steps performed by [epoch1d](https://github.com/Warwick-Plasma/epoch). Numerical steps 1,2,... are steps carried out by [PostProcess.m](PostProcess.m). Unlabeled steps can be manually performed or automated using [batch_PostProcess.m](batch_PostProcess.m). 

In the [Protocol](##protpcol) section below, the focus is to explain the methodology and functions that are involved; whereas in the [Demonstration](##demonstration) section, the focus is to explain the input/output during each step.


## Protocol
Explain steps when they first appear. Abbreviate its description when a step is used repeatedly.

- Pump only

    **a. Simulate with linearly polarized pump**\
        The pump laser enters from the right domain boundary and propagates towards left. The simulation data, which contains two eigen modes and their reflections from plasma/vacuum boundaries, will be used to extract polarization angles of eigen modes.

    1. **Calculate analytical wave parameters**\
        Use warm-fluid theory to calculate wave dispersion relation, polarization angles, and energy coefficients. Performed by [linear_single_w.m](https://gitlab.com/seanYuanSHI/three-wave-matlab/-/blob/master/src/utilities/linear_single_w.m) in [Three-Wave](https://gitlab.com/seanYuanSHI/three-wave-matlab). Results will be used to guide subsequent analysis.

    2. **Extract eigen polarization angles**\
        Use transverse electric fields in the plasma region immediately before the pump exits the plasma to fit two eigen modes. Performed by [EigenPsi.m](steps/EigenPsi.m).

    **b. Simulate with elliptical eigen pump**\
        The pump laser enters from the right domain boundary and propagates towards left. The simulation data, which contains a dominant eigen mode and its reflections from plasma/vacuum boundaries, will be used to extract transmitted amplitude.

    3. **Extract amplitude and wavevector of eigen pump**\
        Use electric-field data in the plasma region immediately before the pump exits the plasma to fit a single eigen mode. Performed by [EigenExtract.m](steps/EigenExtract.m). The constant pump amplitude will be used to calculate analytical growth rate.

    4. **Solve for analytical resonances**\
        Estimate warm-fluid plasma resonances using analytical wavevector and numerical wavevectors for Ey and Ez. Performed by [Resonances.m](steps/Resonances.m). Results will be used to guide the setup of seed wavelengths.

- Scan seeds

    - **Generate a list of seed wavelengths**\
        Setup seed-only runs with multiple wavelengths, can be generated by [SeedScan.m](IO/SeedScan.m), near expected resonance. For each seed wavelength, perform the following steps. 

        **a. Simulate with linearly polarized seed**\
            The seed laser enters from the left domain boundary and propagates towards right. 

        1. **Calculate analytical wave parameters**

        2. **Extract eigen polarization angles**

        **b. Simulate with elliptical eigen seed**

        3. **Extract amplitude and wavevector of eigen seed**\
            The constant seed amplitude will be used to normalize data to analytical frame.

        4. **Fit for phase velocities**\
            Sum pump-eigen-only and seed-eigen-only data, and optimize for phase velocities that best separates right/left propagating waves. Performed by [RLmixer.m](steps/RLmixer.m).

        **c. Simulate with both pump and seed in eigen modes**\
            The pump laser enters from right at the begining of the simulation. The seed laser enters from left after pump front emerges from the left plasma boundary. The two lasers interact via plasma waves, and the seed growth is strongest when the seed wavelength is on resonance.

        5. **Extract right/left moving waves**\
            Use electromagnetic fields data in the plasma region and the optimized phase velocities to separate right-propagating seed from left-propagating pump. Performed by [RightLeft.m](steps/RightLeft.m).

        6. **Extract envelopes of the right/left waves**\
            Use optimized wavevectors to extract slowly-varying pump envelope $`a_1`$ and seed envelopes $`a_2`$ from fast oscillating right/left electric fields. Performed by [RLenvelope.m](steps/RLenvelope.m).

    - **Fit resonant seed wavelength**\
        After performing above steps for all seed wavelengths, fit max $`a_2`$ as a Lorentzian function of seed frequency to determine the resonant wavelength. Performed by [FitSeedResonance.m](IO/FitSeedResonance.m).


- Resonant seed

    **a. Simulate with linearly polarized seed**

    1. **Calculate analytical wave parameters**

    2. **Extract eigen polarization angles**

    **b. Simulate with elliptical eigen seed**

    3. **Extract amplitude and wavevector of eigen seed**

    4. **Fit for phase velocities**

    **c. Simulate with both pump and seed in eigen modes**

    5. **Extract right/left moving waves**

    6. **Extract envelopes of the right/left waves**

    7. **Normalize data to analytical frame**\
        Identify seed wave front in space at multiple time slices. Fit wave fronts to determine $`v_2`$. Normalize $`a_2`$ to compute $`\Delta_1`$ from simulation data. Performed by [WaveFront.m](steps/WaveFront.m).
    
    8. **Fit parameters in three-wave equations**\
        Use residual between numerical and analytical $`\Delta_1`$ 
        to fit for $`\gamma_0, \mu_3, v_3`$, 
        assuming lasers are undampped $`\mu_2=0`$.
        Performed by [FitLinear3Wave.m](steps/FitLinear3Wave.m).

    9. **Quantify fit uncertainty**\
        Compute fit residual while adjusting fit parameters. Determine the fit error bar as the width that doubles the minimum residual. Performed by [FitScan1D.m](steps/FitScan1D.m). Can also use [FitScan2D.m](steps/FitScan2D.m).


## Demonstration
The following example demonstrates step-by-step how to generate data and post process results using [examples/Bscan](examples/Bscan)

### Pump-only runs

**a. Simulate with linearly polarized pump**  

Suppose we want two B0 values 0 and 30 MG, then run [setup_batch.csh](examples/setup_batch.csh) _as is_ in linux command line, assuming C shell
```
./setup_batch.csh
```

This creates folders [PumpLinear/](examples/Bscan/B0/PumpLinear/) and 
[PumpEigen/](examples/Bscan/B0/PumpEigen/) in 
new folders [Bscan/B0/](examples/Bscan/B0/) and [Bscan/B30/](examples/Bscan/B30/). 
Moreover, this changes [input.deck](examples/Bscan/B0/PumpLinear/input.deck), 
so that the line named `B0_strength` is updated with corresponding values.

To run [epoch1d](https://github.com/Warwick-Plasma/epoch) for these two setups in batch on linux clusters using _SLURM_ scheduler, 
use [submit_batch](examples/submit_batch) _as is_ by entering the following in linux command line
```
sbatch submit_batch
```
After [epoch1d](https://github.com/Warwick-Plasma/epoch) runs complete, the [PumpLinear/](examples/Bscan/B0/PumpLinear/) folders 
will be populated with files including [0001.sdf](examples/Bscan/B0/PumpLinear/0001.sdf). 
These sdf files constitute the [epoch1d](https://github.com/Warwick-Plasma/epoch) data that will be post processed. 

1. **Calculate analytical wave parameters**

2. **Extract eigen polarization angles**\
    The above step=1 and step=2 can be run together. First, one needs to add paths such that _MATLAB_ can find relevant functions. In _MATLAB_ command window, run the following
    ```
    >> addpath(genpath('~/epoch/SDF/Matlab/'))
    >> addpath(genpath('~/epoch/epoch1d/Data/Analysis/'))
    >> addpath(genpath('~/Three-Wave/src/'))
    ```
    The first line adds sdf file readers from [epoch1d](https://github.com/Warwick-Plasma/epoch) to search path. The second line adds path to this program suite, assuming it is placed in _~/epoch/epoch1d/Data/Analysis/_. The third line adds path to [Three-Wave/src/](https://gitlab.com/seanYuanSHI/three-wave-matlab) assuming it is placed in the root directory.

    To run [PostProcess.m](PostProcess.m) for both B0=0 and 30 MG, can use [batch_PostProcess.m](batch_PostProcess.m) _as is_. To plot basic diagnostic figures, set `flag=1` in [batch_PostProcess.m](batch_PostProcess.m). To plot all diagnostic figures, set `flag=2`. Not plotting any figure will expediate the runs. Notice that all inputs are embedded in the script.
    ```
    >> batch_PostProcess
    ```  

    After step=1, data files named [PumpPara.mat](examples/Bscan/B0/PumpPara.mat) are saved in folders like [Bscan/B0/](examples/Bscan/B0/), which contains analytical wave parameters for the pump laser.

    After step=2, data files named [Psi.mat](examples/Bscan/B0/PumpLinear/Psi.mat) are saved in [PumpLinear/](examples/Bscan/B0/PumpLinear/), which contains polarization angles of eigen modes. The diagnostic figures are: (1) selected data for the analysis, (2) electric field components and its FFT, and (3) post-fitting reconstruction and residual.
![PumpLinear.png](/examples/figures/PumpLinear.png "Diagnostic figures for pump polarization angle fit")

    After running step=2, [batch_PostProcess.m](batch_PostProcess.m) also runs [PrintPsis.m](IO/PrintPsis.m) to collect polarization angles for all runs. The resultant file [PumpPsis.txt](examples/Bscan/PumpPsis.txt) is saved in [Bscan/](examples/Bscan/). Notice that when B0=0 MG, the eigen modes are degenerate. In outlier cases like this, [PrintPsis.m](IO/PrintPsis.m) automatically replaces the numerically fitted angles by analytical angles in [PumpPara.mat](examples/Bscan/B0/PumpPara.mat). Replaced angles are signified by having fewer digits in [PumpPsis.txt](examples/Bscan/PumpPsis.txt).


**b. Simulate with elliptical eigen pump**   

To setup runs with elliptically polarized eigen pump, use [setup_batch.csh](examples/setup_batch.csh) with `set step = b0`. This setups both B0=0 and 30 MG runs by setting `laser_psi` in [input.deck](examples/Bscan/B0/PumpEigen/input.deck) with values stored in [PumpPsis.txt](examples/Bscan/PumpPsis.txt). 
    
To run [epoch1d](https://github.com/Warwick-Plasma/epoch) simulations in batch, use [submit_batch](examples/submit_batch) with `set FTAIL = /PumpEigen`. After runs complete, folders like [PumpEigen/](examples/Bscan/B0/PumpEigen/) will be populated with files including [0001.sdf](examples/Bscan/B0/PumpEigen/0001.sdf). Notice that the settings in [input.deck](examples/Bscan/B0/PumpEigen/input.deck) is such that [0001.sdf](examples/Bscan/B0/PumpEigen/0001.sdf) is the output at the time just before the pump laser exits the plasma. Output at such an instance is what is needed for the following steps.
    

3. **Extract amplitude and wavevector of eigen pump**

4. **Solve for analytical resonances**\
    The above step=3 and step=4 can be run together by setting set `steps = [3, 4]` in [batch_PostProcess.m](batch_PostProcess.m).

    After step=3, files named [Const.mat](examples/Bscan/B0/PumpEigen/Const.mat) are saved in [PumpEigen/](examples/Bscan/B0/PumpEigen/). The diagnostic figures are similar to those for step=2. Notice that the amplitudes of electric-field components are now constant. 
![PumpEigen.png](/examples/figures/PumpEigen.png "Diagnostic figures for eigen pump amplitude")

    After step=4, files named [Resonances.mat](examples/Bscan/B0/Resonances.mat) are saved in [Bscan/B0/](examples/Bscan/B0). Tables of resonances are also printed to the _MATLAB_ command window.
![Resonances.png](/examples/figures/Resonances.png "Screen shots for tables of resonances")
        
    After running step=4, [batch_PostProcess.m](batch_PostProcess.m) also runs [SeedScan.m](IO/SeedScan.m) to generate a list of seed wavelengths near warm-fluid resonances, which are saved in files like [lambda2s_b3-3.txt](examples/Bscan/B0/lambda2s_b3-3.txt). The first three lines of the file correspond to the three cases in [Resonances.mat](examples/Bscan/B0/Resonances.mat). The remaining lines are nearby wavelengths. The number of additional wavelengths to scan are controlled by `nseeds` and the range of the scan is controled by `radius` in [batch_PostProcess.m](batch_PostProcess.m). 


### Scan-seeds runs

- **Generate a list of seed wavelengths**\
    The list is generatd automatically by [batch_PostProcess.m](batch_PostProcess.m). Set `steps = 4.5` to generate the list again without repeating post processing. Alternatively, the list can be adjusted manually. 

**a. Simulate with linearly polarized seed**

Using files like [lambda2s_b3-3.txt](examples/Bscan/B0/lambda2s_b3-3.txt), a series of seed linear runs can be setup using [setup_batch.csh](examples/setup_batch.csh) with the modifications `set step = a1`, `set BHEAD = B`, and `set FHEAD = ${BRANCH}L`. Notice that the shell variable `BRANCH` should match `b3-3` in the file name [lambda2s_b3-3.txt](examples/Bscan/B0/lambda2s_b3-3.txt), which means that the scan will be near the plasma wave branch b3 that is the 3rd heighest frequency branch in the warm-fluid dispersion relation. 
To scan seed wavelengths for both B0=0 and 30 MG, modify [setup_batch.csh](examples/setup_batch.csh) such that `set barray = (0 30)` and `set farray = ()`. Leaving `farray` empty will allow it to be populated as a simple array of length `set nf = 9`. Notice that this length matches the number of lines in [lambda2s_b3-3.txt](examples/Bscan/B0/lambda2s_b3-3.txt). 
After running the modified [setup_batch.csh](examples/setup_batch.csh) in linux command line, a series of seed folders liks [Bscan/B0/b3-3L7/](examples/Bscan/B0/b3-3L7/) are generated, in which [input.deck](examples/Bscan/B0/b3-3L7/SeedLinear/input.deck) are setup with `seed_lambda` equals to the 7-th lines of [lambda2s_b3-3.txt](examples/Bscan/B0/lambda2s_b3-3.txt).

After [input.deck](examples/Bscan/B0/b3-3L7/SeedLinear/input.deck) are setup, [epoch1d](https://github.com/Warwick-Plasma/epoch) can be run in bach using [submit_batch](examples/submit_batch) with modifications `set BHEAD = B`, `set FHEAD = b3-3L`, `set FTAIL = /SeedLinear`, `set barray = (0 30)` and `set farray = ()`. In addition, the requested run time may need to be adjusted for the _SLURM_ scheduler. For example, using computing clusters with 2 nodes and 32 tasks, one may set `#SBATCH -t 00:10:00`. After [epoch1d](https://github.com/Warwick-Plasma/epoch) runs complete, folders like [Bscan/B0/b3-3L7/SeedLinear/](examples/Bscan/B0/b3-3L7/SeedLinear/) will be populated.

1. **Calculate analytical wave parameters**

2. **Extract eigen polarization angles**\
    Similar to steps 1 and 2 under [Pump-only runs](#pump-only-runs). To post process both B0=0  and 30 MG, each with 9 seed wavelengths, modify [batch_PostProcess.m](batch_PostProcess.m) such that `list = 1:9` and `steps = [1, 2]`.
    
    After step=1, data files named [SeedPara.mat](examples/Bscan/B0/b3-3L1/SeedPara.mat) are saved in folders like [B0/b3-3L1/](examples/Bscan/B0/b3-3L1/).

    After step=2, data files named [Psi.mat](examples/Bscan/B0/b3-3L1/SeedLinear/Psi.mat) are saved in folders like [B0/b3-3L1/SeedLinear/](examples/Bscan/B0/b3-3L1/SeedLinear/). The diagnostic figures are similar to step=2 under [Pump-only runs](#pump-only-runs).

    After running step=2, [batch_PostProcess.m](batch_PostProcess.m) also runs [PrintPsis.m](IO/PrintPsis.m) to produce [SeedPsis_b3-3.txt](examples/Bscan/B30/SeedPsis_b3-3.txt) in folders like [B30/](examples/Bscan/B30/).

**b. Simulate with elliptical eigen seeds**   

Similar to steps **b** under [Pump-only runs](#pump-only-runs). Futher modify [setup_batch.csh](examples/setup_batch.csh) such that `set step = b1` and run the shell script in linux command line. This setups both B0=0 and 30 MG runs each for 9 seed wavelengths by setting `seed_psi` in [input.deck](examples/Bscan/B0/b3-3L1/SeedEigen/input.deck) with values stored in [SeedPsis_b3-3.txt](examples/Bscan/B0/SeedPsis_b3-3.txt)
    
To run [epoch1d](https://github.com/Warwick-Plasma/epoch) simulations in batch, further modifies [submit_batch](examples/submit_batch) such that `set FTAIL = /SeedEigen`. After runs complete, folders like [B0/b3-3L1/SeedEigen/](examples/Bscan/B0/b3-3L1/SeedEigen/) will be populated.

3. **Extract amplitude and wavevector of eigen seed**

4. **Fit for phase velocities**\
    Similar to steps 3 and 4 under [Pump-only runs](#pump-only-runs). Set `steps = [3, 4]` in [batch_PostProcess.m](batch_PostProcess.m).

    After step=3, files named [Const.mat](examples/Bscan/B0/b3-3L1/SeedEigen/Const.mat) are saved in folders like [B0/b3-3L1/SeedEigen/](examples/Bscan/B0/b3-3L1/SeedEigen/). The diagnostic figures are similar to step=3 under [Pump-only runs](#pump-only-runs).

    After step=4, files named [VPhases.mat](examples/Bscan/B0/b3-3L1/Stimulated/VPhases.mat) are saved in folders like [B0/b3-3L1/Stimulated/](examples/Bscan/B0/b3-3L1/Stimulated/). The diagnostic figures include (1) selected pump-only data, (2) selected seed-only data, (3) extracted right/left data, and (4) residuals after extraction. Notice that the residuals are orders of magnitude smaller than the signals.
![ScanVPhase.png](/examples/figures/ScanVPhase.png "Diagnostic figures for fitting pump and seed phase velocities")
   
**c. Simulate with both pump and seed in eigen modes**

Run [setup_batch.csh](examples/setup_batch.csh) with `set step = c` to turn both pump and seed on in [input.deck](examples/Bscan/B0/b3-3L1/Stimulated/input.deck). Notice that stimulated runs take about twice the time of pump-only or seed-only runs. 
    
Run [submit_batch](examples/submit_batch) with `set FTAIL = /Stimulated` and `#SBATCH -t 00:20:00`. After runs complete, folders like [B0/b3-3L1/Stimulated/](examples/Bscan/B0/b3-3L1/Stimulated/) will be populated.


5. **Extract right/left moving waves** 

6. **Extract envelopes of the right/left waves**\
    The above two steps can be run together using [batch_PostProcess.m](batch_PostProcess.m) by setting `steps = [5, 6]` and `its = 13:22`. Here, the setting for `its` means that from [0013.sdf](examples/Bscan/B0/b3-3L1/Stimulated/0013.sdf) to [0022.sdf](examples/Bscan/B0/b3-3L1/Stimulated/0022.sdf) will be processed. These choices correspond to settings in [input.deck](examples/Bscan/B0/b3-3L1/Stimulated/input.deck), where the 13-th output is at the time when the seed laser first enters the plasma, and the 22-nd output is the last output of the simulation. 

    After step=5, folders like [B0/b3-3L1/Stimulated/RightLeft/](examples/Bscan/B0/b3-3L1/Stimulated/RightLeft) will be populated with files like [0013.mat](examples/Bscan/B0/b3-3L1/Stimulated/RightLeft/0013.mat), which contains electric-field components for right- and left-propagating waves. Diagnostic figures include (1) data selection, (2) electric fields, and (3) FFT of selected data.
![RightLeft.png](/examples/figures/RightLeft.png "Diagnostic figures for right/left separation")

    After step=6, folders like [B0/b3-3L1/Stimulated/Envelope/](examples/Bscan/B0/b3-3L1/Stimulated/Envelope) will be populated with files like [0013.mat](examples/Bscan/B0/b3-3L1/Stimulated/Envelope/0013.mat), which contains down-sampled envelopes for right- and left-propagating waves. Diagnostic figures include (1) field of left-moving wave, (2) field of right-moving wave, and (3) envelopes.
![Envelope.png](/examples/figures/Envelope.png "Diagnostic figures for envelope extraction")

    After running step=6, [batch_PostProcess.m](batch_PostProcess.m) also runs [FitSeedResonance.m](IO/FitSeedResonance.m) to fit for the resonant seed wavelength. In many cases, this automatic fitting procedure should be successful. However, it is possible that the scanned wavelength range includes multiple resonant peaks, or the data contains outliers due to wrong settings of the elliptical polarization angles. In these cases, the fitting can be adjusted manually using the `exclude` optional input of [FitSeedResonance.m](IO/FitSeedResonance.m) to exclude outliers from fitting. 
![FitResonance.png](/examples/figures/FitResonance.png "Diagnostic figures for fitting a single resonance")

- **Fit resonant seed wavelength**\
    Assuming fittings for all B0 values are successful, [batch_PostProcess.m](batch_PostProcess.m) continues to run [PrintLambda2s.m](IO/PrintLambda2s.m), which generates [Bscan/rSeedLambda2s_b3-3.txt](examples/Bscan/rSeedLambda2s_b3-3.txt) that will be used to setup resonant seed runs, and run [PrintAnaNumW2s.m](IO/PrintAnaNumW2s.m), which generates [Bscan/w2s_b3-3.txt](examples/Bscan/w2s_b3-3.txt) that can be used for plotting. In case that fittings are adjusted manually, these IO steps can be performed separately by setting `steps = -6` in [batch_PostProcess.m](batch_PostProcess.m).


### Resonant-seed runs

**a. Simulate with linearly polarized seed**

Similar to step **a** under [Scan-seeds runs](#scan-seeds-runs). Using files like [rSeedLambda2s_b3-3.txt](examples/Bscan/rSeedLambda2s_b3-3.txt), a series of seed linear runs can be setup using [setup_batch.csh](examples/setup_batch.csh) with `set step = a1`, `set BHEAD = `, `set FHEAD = B`, `set FTAIL = /$BRANCH`, `set barray = ()`, and `set farray = (0 30)`. After running the modified [setup_batch.csh](examples/setup_batch.csh), a series of seed folders liks [Bscan/B0/b3-3/](examples/Bscan/B0/b3-3/) are generated, in which [input.deck](examples/Bscan/B0/b3-3/SeedLinear/input.deck) are setup with resonant `seed_lambda` in [rSeedLambda2s_b3-3.txt](examples/Bscan/rSeedLambda2s_b3-3.txt).

After [input.deck](examples/Bscan/B0/b3-3/SeedLinear/input.deck) are setup, [epoch1d](https://github.com/Warwick-Plasma/epoch) can be run in bach using [submit_batch](examples/submit_batch) with `set BHEAD = `, `set FHEAD = B`, `set FTAIL = /b3-3/SeedLinear`, `set barray = ()` and `set farray = (0 30)`. Since the number of runs is reduced, the requested run time can be reduced accordingly to `#SBATCH -t 00:02:00`. When the runs complete, folders like [Bscan/B0/b3-3/SeedLinear/](examples/Bscan/B0/b3-3/SeedLinear/) will be populated.

1. **Calculate analytical wave parameters**

2. **Extract eigen polarization angles**\
    Similar to steps 1 and 2 under [Scan-seeds runs](#scan-seeds-runs). Run [batch_PostProcess.m](batch_PostProcess.m) with `list = 1`, `steps = [1, 2]` and `its = 1`.
    
    After step=1, data files named [SeedPara.mat](examples/Bscan/B0/b3-3/SeedPara.mat) are saved in folders like [B0/b3-3/](examples/Bscan/B0/b3-3/).

    After step=2, data files named [Psi.mat](examples/Bscan/B0/b3-3/SeedLinear/Psi.mat) are saved in folders like [B0/b3-3/SeedLinear/](examples/Bscan/B0/b3-3L1/SeedLinear/).

    After running step=2, [batch_PostProcess.m](batch_PostProcess.m) also runs [PrintPsis.m](IO/PrintPsis.m) to produce [rSeedPsis_b3-3.txt](examples/Bscan/rSeedPsis_b3-3.txt) in the root fold [Bscan/](examples/Bscan/).


**b. Simulate with elliptical eigen seed**

Similar to steps **b** under [Scan-seeds runs](#scan-seeds-runs). Run [setup_batch.csh](examples/setup_batch.csh) with `set step = b1`. This setups eigen-seed runs for both B0=0 and 30 MG using polarization angles in [rSeedPsis_b3-3.txt](examples/Bscan/rSeedPsis_b3-3.txt). 
    
To run [epoch1d](https://github.com/Warwick-Plasma/epoch) simulations in batch, modifies [submit_batch](examples/submit_batch) such that `set FTAIL = /b3-3/SeedEigen`. After runs complete, folders like [B0/b3-3/SeedEigen/](examples/Bscan/B0/b3-3/SeedEigen/) will be populated.

3. **Extract amplitude and wavevector of eigen seed**

4. **Fit for phase velocities**\
    Similar to steps 3 and 4 under [Scan-seeds runs](#scan-seeds-runs). Run [batch_PostProcess.m](batch_PostProcess.m) with `steps = [3, 4]`. The output files have the same names as before, but are now saved in folders like [B0/b3-3/SeedEigen/](examples/Bscan/B0/b3-3/SeedEigen/) and [B0/b3-3/Stimulated/](examples/Bscan/B0/b3-3/Stimulated/).

**c. Simulate with both pump and seed in eigen modes**

Similar to steps **c** under [Scan-seeds runs](#scan-seeds-runs). Run [setup_batch.csh](examples/setup_batch.csh) with `set step = c`. Run [submit_batch](examples/submit_batch) with `set FTAIL = /b3-3/Stimulated`. After runs complete, folders like [B0/b3-3/Stimulated/](examples/Bscan/B0/b3-3/Stimulated/) will be populated.

5. **Extract right/left moving waves**

6. **Extract envelopes of the right/left waves**\
    Similar to steps 5 and 6 under [Scan-seeds runs](#scan-seeds-runs). Run [batch_PostProcess.m](batch_PostProcess.m) with `steps = [5, 6]` and `its = 13:22`. The output files have the same names as before, but are now saved in folders like [B0/b3-3/Stimulated/RightLeft/](examples/Bscan/B0/b3-3/Stimulated/RightLeft/) and [B0/b3-3/Stimulated/Envelope/](examples/Bscan/B0/b3-3/Stimulated/Envelope/). Additional diagnostic figures include a check for pump depletion.
![Depletion.png](/examples/figures/Depletion.png "Diagnostic figures for pump laser depletion")

7. **Normalize data to analytical frame**\
    Run [batch_PostProcess.m](batch_PostProcess.m) with `steps = 7`, which produces files like [WFdata.mat](examples/Bscan/B0/b3-3/Stimulated/WFdata.mat) in folders like [B0/b3-3/Stimulated/](examples/Bscan/B0/b3-3/Stimulated/). The file contains seed envelopes normalized to analytical frame, and will be used to fit parameters in the linearized three-wave equations. In addition, the file contains group velocity $`v_2`$, which is fitted using the locations of the seed wave front at specified time slices. Diagnostic figures include (1) identification of wave front, (2) normalized $`a_2`$ in spacetime, and (3) distribution function check. The wiggles of $`a_2`$ are primarily due to leakages during right/left separation and spontaneous excitation of unintended waves.
![WaveFront.png](/examples/figures/WaveFront.png "Diagnostic figures for wave front fit")

    
8. **Fit parameters in three-wave equations**\
    The fitting should only use data in the linear regime, namely, when both pump laser depletion and distiribution function evolution are negligible. Suppose the last output that satisfies these conditions is [00016.sdf](examples/Bscan/B0/b3-3/Stimulated/0016.sdf), then run [batch_PostProcess.m](batch_PostProcess.m) with `its=13:16` and `steps = 8`. The three output files saved in folders like [B0/b3-3/Stimulated/Fits/](examples/Bscan/B0/b3-3/Stimulated/Fits/) are [lpara0.mat](examples/Bscan/B0/b3-3/Stimulated/Fits/lpara0.mat), which contains analytical parameters, [lpara1_limit1.mat](examples/Bscan/B0/b3-3/Stimulated/Fits/lpara1_limit1.mat), which contains constrained best fits for $`\gamma_0`$ assuming $`\mu_3=0`$, and [lpara2_limit1.mat](examples/Bscan/B0/b3-3/Stimulated/Fits/lpara2_limit1.mat), which contains unconstrained best fits for both $`\gamma_0`$ and $`\mu_3`$. Here, `limit = 1` indicates that $`v_3=0`$ is enforced, which expediates the calculation of analytical solutions and is usually a very good approximation because $`v_3\ll v_2`$. To also fit for $`v_3`$, use `limit = 0` in [FitLinear3Wave.m](steps/FitLinear3Wave.m) when called by [PostProcess.m](PostProcess.m). The diagnostic figures are the overlay of numerical and analytical envelopes and their differences at multiple time slices.
![FitEquation.png](/examples/figures/FitEquation.png "Diagnostic figure for fitting parameters in three-wave equations")


9. **Quantify fit uncertainty**\
    To quantify the uncertainty of $`\gamma_0`$ near the unconstrained best fit, run [batch_PostProcess.m](batch_PostProcess.m) with `steps = 9`. This produces files like [lscan1D_r0.txt](examples/Bscan/B0/b3-3/Stimulated/Fits/lscan1D_r0.txt) in folders like [B0/b3-3/Stimulated/Fits/](examples/Bscan/B0/b3-3/Stimulated/Fits/), where each line of the file lists a $`\gamma_0`$ value and its corresponding fit residual when $`\mu_3`$ takes its best-fit value. From this list, the program also calculates properties such as the value of $`\gamma_0`$ when the residual doubles its minimum, and save these properties in files like [1Dstat_r0.mat](examples/Bscan/B0/b3-3/Stimulated/Fits/1Dstat_r0.mat) within the same folder. 
    The diagnostic figures plot the fit residual as a function of the scanned parameter and mark key properties.
![Uncertainty.png](/examples/figures/Uncertainty.png "Diagnostic figure for quantifying fitconcertainty")
    Finally, [batch_PostProcess.m](batch_PostProcess.m) calls [Print1Dstat.m](IO/Print1Dstat.m) to gather results for both B0=0 and 30 MG, and save them in files like [1Dstat_b3-3_r0.txt](examples/Bscan/1Dstat_b3-3_r0.txt) in the root directory [Bscan/](examples/Bscan/). 

    To quantify the uncertainty of damping, set `axis = 'mu3'` in [FitScan1D.m](steps/FitScan1D.m), which produces files with a 'mu3' suffix instead of the 'r0' suffix. 
    To quantify the uncertainty near constrained best fits, set `origin = 1` in [FitScan1D.m](steps/FitScan1D.m). 
    To quantify uncertainties for both parameters, perform 2D scans using [FitScan2D.m](steps/FitScan2D.m). For example, in _MATLAB_ command line
    ```
    >> r0array = linspace(4e12, 1.6e13, 10);         % linear grid for r0
    >> mu3array = 10.^linspace(8, 13, 12);           % log grid for mu3
    >> spath = 'examples/Bscan/B0/b3-3/Stimulated/'; % path contains WFdata.mat
    >> limit = 1;                                    % v3=0
    >> FitScan2D(r0array, mu3array, spath, limit)
    ```
    After the run completes, [FitScan2D.m](steps/FitScan2D.m) generates [lscan2D.txt](examples/Bscan/B0/b3-3/Stimulated/Fits/lscan2D.txt) in folder [B0/b3-3/Stimulated/Fits/](examples/Bscan/B0/b3-3/Stimulated/Fits/). Each line of the file lists the parameter values and the resultant residual of fit.




