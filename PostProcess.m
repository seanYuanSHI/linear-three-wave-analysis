% This function is the main driver that post process data 
% at the following steps. Each step assumes all previous 
% steps have been completed and data is stored in default folders.
%
% Frequently changed parameters are set as input
% Usually unchanged parameters are stored in ParaSetup.m
% Default file paths are returned from in PathSetup.m
%
% At step=1, plasma parameters are loaded from Three-Wave/species.txt
% At later steps, plasma parameters are inherited from step=1 
%
% Inputs:
%    flag  : plot figures
%            flag=0: no plot
%            flag=1: plot main results
%            flag=2: also plot diagnostic figures
%    fhead : file name head
%    lambda: wave length in um
%            The sign is significant for steps 1,2,3,4
%            lambda<0 for pump, lambda>0 for seed
%            The value of lambda is significant only for step 1
%    its   : time slices of epoch data, array of ascending integers
%            for steps 1,4(lambda<0),9 its is in-significant
%    step  : which step to process 
%            1 : calculate analytical wave parameters
%            2 : extract eigen polarization angles from linearly polarized run
%            3 : extract amplitude and wave vector from elliptical eigen run
%          
%            lambda<0 
%            4 : solve for resonances at pump k's from analytic + eigen (Ey, Ez) 
%            lambda>0
%            4 : fit phase velocities for right/left waves from eigen runs
%
%            5 : extract left/right moving waves from stimulated run
%            6 : extract envelopes of the left/right waves
%
%            7 : normalize data to analytical frame
%            8 : fit for parameters in 3-wave equations
%            9 : quantify fit uncertainty  
%    shead  : addtional path for seed
%  overwrite: value to overwrite parameters in ParaSetup.m
%             for looping PostProcess.m over different runs
%             Support the following:
%                To over write B0 to 20 MG, use overwrite='B20'   
%                To over write n0 to 0.2*10^20cc, use overwrite='N0.2'   
%                To over write theta to 15 deg, use overwrite='theta15'   
%                To over write T to 0.1 keV, use overwrite='T100'   
% 
% Output:
%    aux  : struct containing data for IO  
%
% Last modified by Yuan SHI on June 30, 2022

function aux = PostProcess(flag, fhead, lambda, its, step, shead, overwrite)
    % default inputs
    if nargin<6
        shead = '';
    end
    if nargin<7
        overwrite = ''; % default no overwrite
    end
    % default no output
    aux = [];

    % load parameters and constants 
    ParaSetup
    % load default path
    apath = PathSetup(fhead,shead);
    % processing inputs
    ProcessInputs

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if step==1
        disp('Calculating analytical wave parameters')
        % Use functions in Three-Wave/src
        % plasma para, overwrite density in species.txt input file
        disp('Plasma para initialized with Three-Wave/species.txt')
        % initial load to check the number of species
        para = Parameter(B0); 
        ns = length(para.Ns);
        % reload with n0 and T0 overwrite      
        para = Parameter(B0,n0*ones(1,ns),T0*ones(1,ns));

        % linear wave parameters for branch = 1 and 2
        wave1 = linear_single_w(para,alambda,theta_w,1,'length')
        wave2 = linear_single_w(para,alambda,theta_w,2,'length')

        % save data
        fname = char(apath.paras(iname));
        fprintf(['Saving wave data in \n', fname,'\n'])
        save(fname,'para','wave1','wave2','theta')

    elseif step==2 || step==3
        % load k from analytical wave parameters 
        load(char(apath.paras(iname))) % contain wave1 and wave2
        k1 = wave1.k_SI;
        k2 = wave2.k_SI; 
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if step==2
        disp('Extract eigen polarization angles from initial calibration run')
        % read epoch data
        apath.it = max(its); % read last time
        data = ReadData(2, apath, iname, flag-1); % step=2

        % fit polarization angle
        [psi1, psi2] = EigenPsi(data, k1, k2, flag)
     
        % save angles
        fname = char(apath.psis(iname));
        fprintf(['Saving psi data in \n', fname,'\n'])
        save(fname,'psi1','psi2')
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if step==3
        disp('Extract amplitudes and wave vectors for eigen run')
        % read epoch data
        apath.it = max(its); % read last time
        data = ReadData(3, apath, iname, flag-1); % step=3

        % fit amplitude and k
        eigenAK = EigenExtract(data, k1, k2, flag) 

        % save eigen data
        fname = char(apath.consts(iname));
        fprintf(['Saving Const data in \n', fname,'\n'])
        save(fname,'eigenAK')
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if step==4
        if slambda<0 % pump
            disp('Solve for resonances at three pump k')
            % compute and save expected resonances
            Resonances(theta,apath)    

        else % seed
            disp('Fitting phase velocities for right/left')
            % read epoch data
            apath.it = max(its); % read last time
            pdata = ReadData(4, apath, 1, flag-1); % step=4, 1: pump
            sdata = ReadData(4, apath, 2, flag-1); % step=4, 2: seed

            % Expected amplitudes and wave vectors
            Amps = LoadAK(apath,'Amps');
            kSIs = LoadAK(apath,'kSIs');

            % fit phase velocities
            [vopt, aux] = RLmixer(pdata, sdata, Amps, kSIs, flag);
            vopt % print 
            % doc string for phase velocity components
            comps = {'vr','vl'}; 

            % save eigen data
            fname = char(apath.SSV);
            fprintf(['Saving phase velocities in \n', fname,'\n'])
            save(fname,'vopt','comps')
        end
    elseif step==5
        % load optimized phase velocities vopt
        load(char(apath.SSV));
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if step==5
       disp('Extracting right/left moving waves from stimulated data')
       % check stimulated spectrum, initialize a distinct figure 
       if flag>=0
           nfig = sum(double(char([fhead,shead]))); % specific to path
           nfig = nfig + ceil(1e4*(lambda-1)); % specific to wavelength
           figure(nfig) 
       end

       % process each time
       for ii=1:nt
           it = its(ii);
           % store it into
           apath.it = it;
           % read data
           data = ReadData(5, apath, 0, flag-1); % step=5, 0: insignificant
        
           % extract right/left waves, vopt loaded from step=4
           RL = RightLeft(data, vopt, flag);

           % create a new folder
           RLpath = [apath.SS, 'RightLeft/'];
           if ~exist(RLpath,'dir')
               mkdir(RLpath)
           end
           % file named same way as sdf file
           fnameRL = [RLpath,num2str(it,'%04.f'),'.mat'];
           fprintf(['Saving right/left data in \n', fnameRL,'\n'])
           % save data
           save(fnameRL,'RL')

           % check stimulated spectrum
           if flag>=0
               % compute FFT for Ey + Ez, 0: no default plot
               [k, f] = Fourier(data.Ey+data.Ez, data.dx, 0);
               % plot figure, convert k to Trad/s
               % c8 from constants.m in Three-Wave/src, loaded by ParaSetup.m
               figure(nfig) % plot in designated figure
               semilogy(c8*k*1e-4,abs(f).^2,'.-','DisplayName',['it=',num2str(it)])
               hold all
           end
       end
       if flag>=0
           % keep last time for IO
           aux.ck = c8*k*1e-4;;
           aux.f = f;
       end

       % label spectrum
       if flag>=0
           figure(nfig)
           xlim([0,4e3])
           xlabel('k (Trad/s)')
           ylabel('|FFT(Ey+Ez)|^2')
           title(apath.SS(end-25:end)) % last 25 characters
           legend show
           set(gcf,'Color','w')
       end
    end     

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if step==6
       disp('Reading numerically fitted left/right k vectors')
       kSIs = LoadAK(apath,'kSIs');
       % process each time
       for ii=1:nt
           it = its(ii);
           disp(['Extract RL envelope at it=',num2str(it)])
           % read data
           fnameRL = [apath.SS,'RightLeft/',num2str(it,'%04.f'),'.mat'];
           load(fnameRL) % RL

           % process data
           [EA, aux] = RLenvelope(RL,kSIs,flag);

           % create folder to save data
           Epath = [apath.SS,'Envelope/'];
           if ~exist(Epath, 'dir')
               mkdir(Epath)
           end
           % file named same way as sdf file
           fname = [Epath,num2str(it,'%04.f'),'.mat'];
           save(fname,'EA')
       end
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if step==7
       disp('Locate seed wave front and normalize data to analytical frame')
       % locate wave front and save data
       WaveFront(apath,its,flag)
       % plot distribution function for check up
       Read1Ddist_fn(its,apath.SS,1); % 1: plot
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if step==8
       disp('Fit data for parameters in 3-wave equations')
       FitLinear3Wave(apath,its,1,flag) % 1: use limiting form v3=mu3=0 
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if step==9
       disp('Quantify fit uncertainty by scanning residual')
       % origin:
       %   1: scan near constrained best fit
       %   2: scan near unconstrained best fit
       % axis:
       %  'r0' : scan r0 at fixed mu3
       %  'mu3': scan mu3 at fixed r0
       FitScan1D(nscan,apath.SS,2,'r0',srm,flag) 
    end 
