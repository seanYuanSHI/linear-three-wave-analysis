% This script store in-frequently changed parameters, 
% which should be consistent with epoch input.deck, 
% and Three-Wave/species.txt 

% plasma parameters
% B field value in MG
B0 = 30;
% plasma density in 10^20 cc
n0 = 0.1;
% plasma temperature in keV
T0 = 0.01;
% B in xz plan with theta=<x,B0>
theta = 30; % in degree

% parameters used in epoch setup
% cell per wavelength
cell_lambda = 40;
% box size in wavelength
box_lambda = 100;
% left vacuum gap size in wavelength
left_lambda = 10;
% right vacuum gap size in wavelength
right_lambda = 10;
% process data within plasma region
% safety gap between plsma and data region
safety_lambda = 4;

% for scan in step 9
% normalized radius of scan
srm = -1.0; % -: abs radius in Trad/s
% number of 1D points
nscan = 20;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Users are not expected to change the following
% initial index of plasma region
ileft = (left_lambda+safety_lambda)*cell_lambda + 1;
% finial index of plasma region
iright = (box_lambda-right_lambda-safety_lambda)*cell_lambda;

% load constants from Three-Wave/src
constants
