% This function computes analytically expected 
% parameters in linearized three-wave equations
% v2, v3, mu2, mu3, r0
%
% Inputs:
%    apath : struct that contains default path
%    flag  : flag>1 plot diagnostic figure
% Output: 
%   lpara : analytical initial guess
%
% Written by Yuan SHI, Oct 29, 2021

function lpara = AnaPara(apath, flag)
    if nargin<2
        flag=0; % default no plot
    end

    %%%%%%%%%%%%%%%%%%%%
    % load seed para
    load(char(apath.paras(2))) % wave1, wave2, para
    % determin seed branch
    load(char(apath.consts(2))) % eigenAK
    b = eigenAK.branches(1);
    if b==1
        waveS = wave1;
    else
        waveS = wave2;
    end
    % propagation angle
    theta = waveS.theta;
    % seed group velocity
    v2 = waveS.vg_SI;


    %%%%%%%%%%%%%%%%%%%%
    % determine plasma wave para
    disp('loading resonances')
    load(apath.PR) 
    disp('Plasma para inherited from Resonance.mat')
    disp(['  Ns = ',num2str(para.Ns')])
    disp(['  Ts = ',num2str(para.Ts')])
    disp(['  B0 = ',num2str(para.B0)])

    % scattering map
    sa = scatters(1); % 1: analytic, 2: Ey eigen, 3: Ez eigen  
    map = sa.map; % scatter from Resonances.mat

    % determine branch index b3 by matching w2
    dw2 = abs(map.w2 - waveS.w);
    [~,ind] = min(dw2);
    b3 = map.b3(ind);
    disp(['Plasma wave branch b3=',num2str(b3)])


    %%%%%%%%%%%%%%%%%%%%
    % load pump para
    load(char(apath.paras(1))) % wave1, wave2, para
    % determin pump branch
    load(char(apath.consts(1))) % eigenAK
    b = eigenAK.branches(1);
    if b==1
        waveP = wave1;
    else
        waveP = wave2;
    end

    % compute Raman growth rate
    % convert constant pump E to normalized a
    a1 = eigenAK.A*waveP.E2a;
    % use analytical pump frequency
    w1 = sa.w1;
    gammaR = sqrt(w1*para.wp)*abs(a1)/2; % in Trad/s


    %%%%%%%%%%%%%%%%%%%%
    % normalized coupling 
    M = abs(map.Mc(ind));
    % undamped growth rate in SI unit
    r0 = 1e12*M*gammaR; % gammaR from in Trad/s

    % plasma wave vector, backscattering
    ck3 = map.ck2r(ind)+sa.ck1r;
    % compute group velocity, theta and para from seed
    v3c = Vgroup1D(b3,ck3,theta,para,flag-1); % Three-Wave/src
    % convert to SI unit
    constants % load c8 from Three-Wave/src/auxiliary/constants.m
    v3 = 1e8*c8*v3c;

    % Laudau damping rate of unmagnetized Langmuir wave
    % identify which species is electron    
    Zs = para.Zs;
    [~,ind]=min(abs(Zs+1));
    disp(['electron is species ',num2str(ind)])
    % k*lambda_De = (ck/omega_pe)*(v_Te/c)
    kL = ck3*sqrt(para.us2(ind)/para.wps2(ind));
    disp(['k3*lambda_De is ',num2str(kL)])
    % wi/wr 
    wiwr = 1/kL^3*sqrt(pi/8)*exp(-3/2-1/2/kL^2);
    % wr in Trad/s
    wr = para.wp*(1+3*kL^2/2);
    % mu3 in SI unit
    mu3 = wr*wiwr*1e12;

    % load output
    lpara.v2 = v2;
    lpara.v3 = v3;
    lpara.mu2 = 0; % assume seed undamped
    lpara.mu3 = mu3;
    lpara.r0 = r0;
