% This function extract left and right moving waves from
% 1D simulation data. 
%
% For wave propagation in 1D, Faraday equation gives
%    right-moving wave: Bz = Ey/v,  By=-Ez/v
%    left-moving wave : Bz =-Ey/v,  By= Ez/v
% where v>0 is the phase velocity of a given plane wave.
%
% Suppose the signal is comprized of a single left and a single right wave
% Then the electric/magnetic fields can be written as
%    Ey =  Ery     +  Ely,     Ez = Erz     + Elz
%    By = -Erz/vr  + Elz/vl ,  Bz = Ery/vr  - Ely/vl
% Notice that the phase velocities for right/left wave may be different.
% Then, we have the following relations
%    Ey + vl*Bz = (1+vl/vr)*Ery,  Ez - vl*By = (1+vl/vr)*Erz
%    Ey - vr*Bz = (1+vr/vl)*Ely,  Ez + vr*Bz = (1+vr/vl)*Elz     
% Therefore, the electric field of right/left waves are 
%    right : Ery = (Ey+vl*Bz)/(1+vl/vr),  Erz = (Ez-vl*By)/(1+vl/vr)
%    left  : Ely = (Ey-vr*Bz)/(1+vr/vl),  Elz = (Ez+vr*By)/(1+vr/vl)
%
% The energy flux in the right/left directions are
%    Sr = +epsilon0*vgr*(Ery^2+Erz^2)
%    Sl = -epsilon0*vgl*(Ely^2+Elz^2)
% where vg are group velocities.
%
% Inputs:
%   data   : struct that contans Ey,Ez,By,Bz at one time slice
%            All components are AC only, with DC contribution subtracted
%   vps    : 1D array that contans phase velocities for right/left waves
%            vps = [vr, vl]
%            all phase velocities are positive
%   flag   : flag>0 plot figures
% Outputs:
%   RL     : struct that contains the following 
%              RL.comps = {'r','l'}
%              RL.Edata
%                  Edata(:,1)=Ery, Edata(:,2)=Erz
%                  Edata(:,3)=Ely, Edata(:,4)=Elz
%              RL.sr, RL.sl
%            where sr and sl are averaged normalized fluxes.
%
% Written by Yuan SHI on Oct 13, 2021

function RL = RightLeft(data, vps, flag)

    % default no plot
    if nargin<3
        flag = 0;
    end
    % initialize output by loading doc string
    RL.comps = {'r','l'};

    % unload components
    % Yee's E are edge centered
    Ey = data.Ey(2:end);
    Ez = data.Ez(2:end);
    % Yee's B are face centered
    By = (data.By(1:end-1)+data.By(2:end))/2;
    Bz = (data.Bz(1:end-1)+data.Bz(2:end))/2;
  
    % unload phase velocities
    % ensure positive
    vr = abs(vps(1));
    vl = abs(vps(2));

    % construct right waves
    Ery = (Ey + vl*Bz)/(1 + vl/vr);
    Erz = (Ez - vl*By)/(1 + vl/vr);
    % construct left waves
    Ely = (Ey - vr*Bz)/(1 + vr/vl);
    Elz = (Ez + vr*By)/(1 + vr/vl);

    % compute normalized fluxes
    sr = norm(Ery)^2+norm(Erz)^2;
    sl = norm(Ely)^2+norm(Elz)^2;

    % load outputs
    Edata(:,1) = Ery;
    Edata(:,2) = Erz;
    Edata(:,3) = Ely;
    Edata(:,4) = Elz;
    RL.Edata = Edata;
    RL.sr = sr;
    RL.sl = sl;
    % load auxilliary parameter
    RL.dx = data.dx;
    RL.time = data.t;
    RL.i0 = data.i0;
    RL.il = data.il + 1; % RL takes 2:end
    RL.ir = data.ir;

    % plot figures
    if flag>0
        figure
        subplot(2,1,1)
        plot(Ey,'k.-')
        hold on
        plot(Ery,'r')
        plot(Ely,'b')
        line(xlim,[0,0],'Color',[0.5,0.5,0.5]);
        ylabel('Ey')
        legend('Ey','Ery','Ely')
     
        subplot(2,1,2)
        plot(Ez,'k.-')
        hold on
        plot(Erz,'r')
        plot(Elz,'b')
        line(xlim,[0,0],'Color',[0.5,0.5,0.5]);
        ylabel('Ez')
        legend('Ez','Erz','Elz')

        set(gcf,'Color','w')
    end
