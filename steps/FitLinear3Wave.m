% This function use FitResidual.m as the objective function
% to fit for parameters in linearized three-wave equations
%     v3, mu3, r0
% v2 is from the wave front fit
% mu2 is always assumed to be zero
%
% The function carries out the following three steps:
%  (0) Calculate analytical parameters ->lpara0.mat
%  (1) Perform constrained fit ->lpara1.mat
%  (2) Perform unconstrained fit ->lpara2.mat
% Initial guesses are provided analytically.
% The fit uncertainty can be estimated from 
% the constrained and unconstraint fits. 
%
% When limit=0: use full solution, slow (double integral)
%     (1) Constrained fit:   v3 is analytic, only fit mu3 and r0
%     (2) Unstrained fit:    v3, mu3, r0 are all fitted
% When limit=1: use limit solution for v3=0, fast (single integral)
%     (1) Constrained fit:   v3 and mu3 set to zero, only fit r0
%     (2) Unconstrained fit: v3 set to zero, fit mu3 and r0
%
% Inputs:
%    apath : struct containing default paths
%            processed data are saved in <stimulated>WFdata.mat
%    its    : integer array, time slices to include
%             e.g. its=[12,13,14,17], corresponds to epoch outputs
%    limit : limit=0 use full analytical solution
%            limit=1 use limiting form of the analytical solution (Default)
%    flag  : flag=0, no plot 
%            flag=1, plot figures
%            flag=2, also report optimization progress (Default)
%
% Output: save to file
%         its : from input
%         lpara0 : analytical initial guess
%         lpara1 : constrained fit
%         lpara2 : unconstrained fit
%
% Last modified by Yuan SHI on June 18, 2022

function FitLinear3Wave(apath, its, limit, flag)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % initial preparation 
    if nargin<3
        limit = 1; % default use simplified solution
    end
    if nargin<4
        flag=2; % default show all plot
    end
    % report limit
    if limit==1
        disp('limit=1: Use limiting solution v3=0 for fitting')
        lstr = '_limit1';
    else
        limit=0;
        disp('limit=0: Use full solution for fitting')
        lstr = '_limit0';
    end

    % options for the search
    if flag==2
        % plot function value at each iteration
        options = optimset('PlotFcns',@optimplotfval,'Display','iter');
        %options = optimset('PlotFcns',@optimplotfval,'Display','notify');
    else % use default
        options = optimset();
    end

    % load v2 from WFdata.mat
    v2 = LoadWFv2(apath.SS);

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % create folder if not exist
    fpath = [apath.SS,'Fits/'];
    if ~exist(fpath,'dir')
        mkdir(fpath)
    end
    % determine which step to perform next
    % file names
    fname0 = [fpath,'lpara0.mat'];
    fname1 = [fpath,'lpara1',lstr,'.mat'];
    fname2 = [fpath,'lpara2',lstr,'.mat'];

    % step  : step=1 analytical para
    %         step=2 constrained fit, need 1
    %         step=3 unconstrained fit, need 1 & 2
    %         step=0 plot only
    step=0;
    % later steps need earlier steps
    try
        load(fname2)
        disp('Existing unconstrained lpara loaded from file')
        lpara2 % print
    catch
        % need to do step 3
        step = 3;
    end

    try
        load(fname1)
        disp('Existing constrained lpara loaded from file')
        lpara1 % print
    catch
        % need to do step 2
        step = 2;
    end

    try
        load(fname0)
        disp('Existing analytical lpara loaded from file')
        lpara0
    catch
        % need to do step 1
        step = 1;
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if step==1
        % analytical initial guess
        disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
        disp('Determine analytical initial guess...')
        lpara0 = AnaPara(apath,flag);
        % load limit
        lpara0.limit = limit % print lpada

        % save data to file
        save(fname0,'lpara0')
    end

    % plot figure
    if flag>0 && (step==1 || step==0)
        disp('Plot figure...')
        FitResidual(apath.SS,lpara0,its,1);
        set(gcf,'Name','Analytic')
    end
    % advance step
    if step==1
        step=2;
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % define objective functions for fitting
    % nested function knows about parent variables
    % cannot be placed inside log loops

    % unpack analytic parameters
    v3a = lpara0.v3;
    mu3a = lpara0.mu3;
    r0a = lpara0.r0;

    % constrained fit limit=0: v3 from analytic, fit mu3 and r0
    function r = fc0Residual(p)
        % load lpara
        lpara.v2 = v2;
        lpara.v3 = v3a;
        lpara.mu2 = 0; % assume a2 undampped
        lpara.limit = 0; % do not use limit form
        % ensure parameters are positive
        lpara.mu3 = abs(p(1));
        lpara.r0 = abs(p(2));
        % compute residual
        r = FitResidual(apath.SS,lpara,its); % no plot
    end

    % constrained fit limit=1: v3=mu3=0, fit r0
    function r = fc1Residual(p)
        % load lpara
        lpara.v2 = v2;
        lpara.v3 = 0;
        lpara.mu2 = 0;
        lpara.mu3 = 0;
        lpara.limit = 1; % use limiting form
        % ensure parameters are positive
        lpara.r0 = abs(p);
        % compute residual
        r = FitResidual(apath.SS,lpara,its); % no plot
    end

    % unconstrained fit limit=0: fit v3, mu3, r0
    function r = fu0Residual(p)
        % load lpara
        % ensure parameters are positive
        lpara.v2  = v2;
        lpara.mu2 = 0;
        lpara.v3  = abs(p(1));
        lpara.mu3 = abs(p(2));
        lpara.r0  = abs(p(3));
        lpara.limit = 0; % do not use limiting form
        % compute residual
        r = FitResidual(apath.SS,lpara,its); % no plot
    end

    % unconstrained fit limit=1: v3=0, fit mu3, r0
    function r = fu1Residual(p)
        % load lpara
        % ensure parameters are positive
        lpara.v2  = v2;
        lpara.mu2 = 0;
        lpara.v3  = 0;
        lpara.mu3 = abs(p(1));
        lpara.r0  = abs(p(2));
        lpara.limit = 1; % use limiting form
        % compute residual
        r = FitResidual(apath.SS,lpara,its); % no plot
    end


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % constrained fit
    if step==2
        disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
        disp('Constrained fit ...')

        % initialize lpara1 by loading known parameters
        lpara1.v2 = v2; % from wave front fit
        lpara1.mu2 = 0; % inherit analytic assumption mu2=0
        
        if limit==0 % full solution
            % construct function handle for the search
            func = @(q)fc0Residual(q);
            % initial guess
            q0 = [mu3a, r0a];
            % search for optimized value
            qf = fminsearch(func, q0, options);
     
            % load best fit parameters
            lpara1.mu3 = abs(qf(1));
            lpara1.r0 = abs(qf(2));
            lpara1.v3= v3a; % inherit from analytic
            lpara1.limit = 0 % print lpara1

        else % limiting solution
            % construct function handle for the search
            func = @(q)fc1Residual(q);
            % initial guess
            q0 = r0a;
            % search for optimized value
            qf = fminsearch(func, q0, options);
     
            % load best fit parameters
            lpara1.r0 = abs(qf);
            lpara1.v3= 0;
            lpara1.mu3 = 0;
            lpara1.limit = 1 % print lpara1
        end

        % save data to file
        save(fname1,'its','lpara1')
    end

    % plot figure
    if flag>0 && (step==2 || step==0)
        disp('Plot figure...')
        FitResidual(apath.SS,lpara1,its,1);
        set(gcf,'Name','Constrained')
    end
    % advance step
    if step==2 
        step=3;
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % unconstrained fit
    % fit v3, mu3, r0
    if step==3 
        disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
        disp('Unconstrained fit...')

        if limit==0 % full solution
            % unconstruct function handle for the search
            funu = @(q)fu0Residual(q);
            % initial guess
            q0 = [v3a, mu3a, r0a];
            % search for optimized value
            qf = fminsearch(funu, q0, options);
         
            % load best fit parameters
            lpara2.v2 = v2; % wave front fit
            lpara2.mu2 = 0; % assume mu2=0
            lpara2.v3  = abs(qf(1));
            lpara2.mu3 = abs(qf(2));
            lpara2.r0  = abs(qf(3));
            lpara2.limit = 0 % print lpara2

        else % limiting solution
            % unconstruct function handle for the search
            funu = @(q)fu1Residual(q);
            % initial guess
            q0 = [mu3a, r0a];
            % search for optimized value
            qf = fminsearch(funu, q0, options);
         
            % load best fit parameters
            lpara2.v2 = v2; % wave front fit
            lpara2.mu2 = 0; % assume mu2=0
            lpara2.v3 = 0;
            lpara2.mu3 = abs(qf(1));
            lpara2.r0  = abs(qf(2));
            lpara2.limit = 1 % print lpara2
        end
 
        % save data to file
        save(fname2,'its','lpara2')
    end

    % plot figure
    if flag>0 && (step==3 || step==0) 
        disp('Plot figure...')
        FitResidual(apath.SS,lpara2,its,1);
        set(gcf,'Name','Unconstrained')
    end

end
