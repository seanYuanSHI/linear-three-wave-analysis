% This function utilize NearDegenerate.m as an objective function 
% to search for values of (k1,k2) that minimizes the residual
% for data of the form y(x)=A1*sin(k1*x+b1) + A2*sin(k2*x+b2)
%
% Inputs:
%    y       : y values on uniform grid, vectors of size n-by-1
%    dx      : uniform grid spacing, scalar
%    k10,k20 : initial guess of wavevectors, real scalars
%    flag    : if flag=0, do not plot diagnostic figures
%              otherwise, plot figures with 'flag' as figure name 
% Outputs:
%    spara  : a struct contains the optimized values
%            spara.k1     : k1
%            spara.A1     : A1 ... Positive
%            spara.b1     : b1 ... Between -pi and +pi
%            spara.ac1    : fit parameters
%            spara.r      : norm of residual vector normalized by # points
%
% Written by Yuan SHI, Oct 10, 2021 

function spara = NDsearch(y,dx,k10,k20,flag)

    % default no plot
    if nargin<5
        flag = '0';
    else % convert to string
        flag = num2str(flag);
    end

    % check y dimension
    s = size(y);
    assert(min(s)==1,'y should be a vector')
    if s(2)>s(1)
        y = y';
    end

    % define objective function 
    function r = fNDsearch(y,dx,k)
         % assume k1=k(1), k2=k(2)
         ndpara = NearDegenerate(y,dx,k(1),k(2));
         % extract residual
         r = ndpara.r;
    end

    % options for the search
    if strcmp(flag,'0')~=1 && strcmp(flag,'1')~=1
        % plot function value at each iteration
        %options = optimset('PlotFcns',@optimplotfval,'Display','iter');
        options = optimset('PlotFcns',@optimplotfval,'Display','notify');
    else % use default
        options = optimset();
    end

    % construct function handle for the search
    fun = @(k)fNDsearch(y,dx,k);
    % initial guess
    ki = [k10,k20];
    % search for optimized value
    kf = fminsearch(fun, ki, options);

    % compute optimized parameters
    spara = NearDegenerate(y,dx,kf(1),kf(2),flag);
    % load output
    spara.k1 = kf(1);
    spara.k2 = kf(2);

end
