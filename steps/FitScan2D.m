% This function scans (r0, mu3) parameter space
% to get a sense of the fit concertainty.
% Scan in a rectangle specified by r0array and mu3array.
% Users should have a sense of where the minimum is,
% which can be predicted from lpara2.mat (unconstrained fit)
% or from automatic elliptical scan using FitScan2D_ellipse.m
%
% Inputs:
%    r0array  : array of r0 values to scan, length nr
%    mu3array : array of mu3 values to scan, length nm
%    spath    : path to seed stimulated folder
%               processed data are saved in <spath>WFdata.mat
%    limit    : limit=0 use full analytical solution
%               limit=1 use limiting form v3=0 (Default)
%
% Output: save to file <spath>Fits/lscan2D.txt 
%         header: # its, inherit from <spath>Fits/lpara.mat
%         body  : each raw is of the pattern 
%                 (r0, mu3, residual per point)
%         The .txt file can be plotted by other programs.
%
% Written by Yuan SHI on June 8, 2022

function FitScan2D(r0array, mu3array, spath, limit)
    if nargin<4
        limit=1;
    end
    % report limit
    if limit==1
        disp('limit=1: Use limiting solution v3=0 for fitting')
        lstr = '_limit1';
    else
        limit=0;
        disp('limit=0: Use full solution for fitting')
        lstr = '_limit0';
    end

    % lengths of arrays
    nr = length(r0array);
    nm = length(mu3array);

    % read parameters lpara
    load([spath,'Fits/lpara2',lstr,'.mat'])
    % copy lpara to be analytical 
    lpara = lpara2;

    % open file for saving data
    fname = [spath,'Fits/lscan2D.txt'];
    fid = fopen(fname,'a'); % append
    % write file header
    fprintf(fid,'####################');
    % write box parameters
    fprintf(fid,'\n# min(r0)=%e, max(r0)=%e, nr=%d',...
           min(r0array),max(r0array),nr);
    fprintf(fid,'\n# min(mu3)=%e, max(mu3)=%e, nm=%d',...
           min(mu3array),max(mu3array),nm);
    % finish file header
    fprintf(fid,'\n####################\n');
    fprintf(fid,'## r0, mu3, residual per point \n');

    % load value
    disp('Scanning (mu3,r0)...')
    for ir = 1:nr
        fprintf(['ir/nr = ',num2str(ir),'/',num2str(nr),'\n'])
        r0 = r0array(ir);
        for im = 1:nm
            fprintf(['im/nm = ',num2str(im),'/',num2str(nm),'\n'])
            mu3 = mu3array(im);

            % update struct
            lpara.mu3 = mu3;
            lpara.r0 = r0;

            % compute residual
            res = FitResidual(spath,lpara,its);
            % save data
            fprintf(fid,'%8.4e, %8.4e, %8.4e\n',r0, mu3, res);
        end
    end

    % close file
    fclose(fid);
