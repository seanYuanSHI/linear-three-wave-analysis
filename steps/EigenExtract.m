% This function performs step 3 of PostProcessing.m
% by extracting constant amplitudes and wave vectors
% from pump/seed eigen runs
%
% Inputs:
%    data  : struct that contains Ey,Ez, dx
%    k1,k2 : estimated wave vectors, in SI unit
%    flag  : flag=0, no display
%            flag=1, plot basic figures
%            flag=2, plot all figures
% Outputs:
%    eigenAK: struct that contains the following
%      comps   : doc string for data components used
%      branches: wave branch index the data component, integers
%      kSIs    : optimal wave vectors for the data component, in SI unit
%      Amps    : averaged amplitude of the data component
%      AEx     : averaged amplitude of Ex at average wave vector
%      phi     : transverse angle 
%                phi=pi/2 for transverse wave
%                phi=0 for longitudinal wave  

function eigenAK = EigenExtract(data, k1, k2, flag)

        % doc string of data components
        comps = {'Ey','Ez'}; 
        % initialize data
        branches = zeros(1,2); % The eigenmode is on branch 1 or 2
        kSIs = zeros(1,2); % abs of k in SI unit, refined
        Amps = zeros(1,2); % abs of mean amplitudes of the mode 
 
        % name for diagnostic figures
        if flag>1 % add Ex
            fcomps = comps(:,[1:2,2]);
            fcomps(3)={'Ex'};
        else
            fcomps = '000';
        end

        % process all components    
        dx = data.dx; % uniform grid spacing
        for i=1:2
            fprintf(['Extracting envelope of ',char(comps(i)),'...\n'])
            wd = data.(char(comps(i)));
            wpara = NDsearch(wd,dx,k1,k2,char(fcomps(i))) % print
     
            % mode with larger amplitude is the intended eigenmode
            if wpara.A1>wpara.A2
                k = wpara.k1;
            else
                k = wpara.k2;
            end
            kSIs(i) = k;
     
            % extract envelope
            [xc, Ac] = Envelope(wd,dx,k,char(fcomps(i)));
            % mean envelope
            Amps(i) = abs(mean(Ac));
     
            % branch index is the one closer to analytical 
            if abs(k-k1)<abs(k-k2)
                branches(i) = 1;
            else
                branches(i) = 2;
            end
        end % end of component for loop

        % Extract amplitude of Ex, use averaged wave vector
        [xc, Ac] = Envelope(data.Ex,dx,mean(kSIs),char(fcomps(3)));
        % mean envelope
        AEx = abs(mean(Ac));
        % total amplitude
        A = sqrt(Amps(1)^2+Amps(2)^2+AEx^2);
        % transverse angle
        phi = atan(norm(Amps)/AEx);
     
        % load output
        eigenAK.comps = comps;
        eigenAK.branches = branches;
        eigenAK.kSIs = kSIs;
        eigenAK.Amps = Amps;
        eigenAK.AEx = AEx;
        eigenAK.A = A;
        eigenAK.phi = phi;

