% This function compute analytical solution of linearized 
% three-wave equations for given lpara, and compute its
% residual from simulation data.
%
% Inputs:
%    spath : path to seed folder
%            processed data are saved in <spath>WFdata.mat
%    lpara : struct used by ../analytic/Delta.m or DaltaLimit.m
%            contains v2, v3, mu2, mu3, r0, limit 
%            When v3=0, the analytical solution is simplified.
%            When mu3=0, the solution is further simplified.
%                limit=1: enforce v3=0 (default)
%                limit=2: enforce v3=mu3=0
%    its    : integer array, time slices to include
%             e.g. its=[12,13,14,17], corresponds to epoch outputs
%    flag  : flag=0, default no plot, show message
%            flag>=1, plot analytical v.s. numerical and residuals at time slices
%            flag>=2, also plot 2D analytical solutions
% Output:
%   res    : residual per point re=sqrt(r2)/(nx*nt)
%            total residual is r2=\sum_{ix,it} (Ana(ix,it)-Num(ix,it))^2
%            where Ana is analytical solution, Num is simulation data
%   sol    : struct containing solutions
%            sol.x   : x grid, length nx
%            sol.t   : t grid, length nt
%            sol.D1ana : analytical solution for Delta1, nx-by-nt
%            sol.D1num : numerical data for Delta1, nx-by-nt
%
% Written by Yuan SHI on Oct 29, 2021
% Last modified by Yuan SHI on June 17, 2022

function [res, sol] = FitResidual(spath,lpara,its,flag)
    %%%%%%%%%%%%%%%%%%%%%%%%%
    % normalization factor for plot overlay
    % At different time slice, plot y/nfp/max(y)+it
    nfp = 3;
    %%%%%%%%%%%%%%%%%%%%%%%%%
    if nargin<4
        flag = 0; % default no plot
    end

    % load simulation WF
    % which contains: xa, ta, inds, nfs, alpha2
    load([spath,'WFdata.mat'])
    xa = WF.xa;
    ta = WF.ta;
    inds = WF.inds; % index of wave front
    nfs = WF.nfs;   % index thickness of wave front
    alpha2 = WF.alpha2;

    % convert its to slices in WF array
    slices = SelectSlices(WF.its, its);    

    % select time slices to process
    t0 = ta(slices); % monotonous increasing
    nt = length(slices);
    % process all spatial points
    x = xa;
    nx = length(xa);

    % select slices with t>0
    i0 = 1;
    while t0(i0)<=0 && i0<nt
        i0 = i0+1;
    end
    assert(t0(i0)>0,'No data selected since wave front enters!') 
    if flag>0
        disp('  Valid time slices in WF since wave front enters are: ')
        disp(slices(i0:nt))
    end
    t = t0(i0:nt);
    % update nt
    nt = nt-i0+1;

    % initialize sol
    sol.x = x;
    sol.t = t;
    D1ana = zeros(nx,nt);
    D1num = zeros(nx,nt);
 
    % convert alpha2 to d2 require damping factor
    mu2 = lpara.mu2;
    v2 = lpara.v2;
    df = exp(-mu2*x/v2);

    % check how analytic solution should be computed
    if isfield(lpara,'limit')
        limit = lpara.limit; % when limit is specified in lpara
    else
        limit = 1; % defaul use in limiting form v3=0
    end

    % copy lpara and enforce limit conditions
    rpara = lpara;
    rpara.limit = limit;
    if limit>0
        rpara.v3=0;
        if limit>1
            rpara.mu3=0;
            if flag>1
                disp('  Use analytical solution for v3=mu3=0') 
            end
        else
            if flag>1
                disp('  Use analytical solution for v3=0') 
            end
        end
    end

    % compute analytical solution 1: a2 <-> Delta1
    % use replicated parameters rpara
    d1a = Delta(x, t, rpara, 1, flag-1); % plot only when flag>1

    % initialize figure if plot
    if flag>0
        figure
        % normalization factor for figure overlay
        dmax = nfp*max(max(d1a));
        % axis bounds
        xmin = min(x);
        xmax = max(x);
    end
    % compute residual behind wave front
    if flag>0
        disp('  Computing residual...')
    end
    r2 = 0;
    % sum residual along x only behind wave front
    for ii=1:nt
        % index of time slice in data
        it = slices(i0+ii-1);
        % index of wave front, subtract double thickness
        mx = min(inds(it)-2*nfs(it),nx);
        % ensure at least 1
        mx = max(1,mx);

        % convert alpha2 to d1
        d1n = abs(alpha2(:,it))./df-1;
        % difference, mx-by-1 
        d = d1a(1:mx,ii) - d1n(1:mx);
        % residual
        r2 = r2 +d'*d;
        % load sol.D1
        D1ana(1:mx,ii) = d1a(1:mx,ii);
        D1num(1:mx,ii) = d1n(1:mx);

        % plot figure
        if flag>0
            % plot data
            subplot(1,2,1)
            plot(x(1:mx),d1a(1:mx,ii)/dmax+it,'r.-')
            hold on
            plot(x(1:mx),d1n(1:mx)/dmax+it,'ko')
            line([xmin,xmax],[it,it],'LineStyle','--','Color',[0.5,0.5,0.5]);
           
            % plot difference
            subplot(1,2,2)
            plot(x(1:mx),d/dmax+it,'k.-')
            hold on
            line([xmin,xmax],[it,it],'LineStyle','--','Color',[0.5,0.5,0.5]);
         end             
    end
    % residual per point
    res = sqrt(r2)/(nx*nt);
    % load sol
    sol.D1ana = D1ana;
    sol.D1num = D1num;

    % label figure
    if flag>0
        subplot(1,2,1)
        xlabel('x (meter)')
        ylabel(['d1/(',num2str(dmax),')+it'])
        ylim([0,max(slices)+1])
        title(['it=',num2str(min(its)),':',num2str(max(its))])
        legend('analytic','numeric','Location','southeast')

        subplot(1,2,2)
        xlabel('x (meter)')
        ylabel(['(ana-num)/(',num2str(dmax),')+it'])
        ylim([0,max(slices)+1])
        title(['...',spath(end-25:end)]) % last 25 characters

        % add suptitle, for MATLAB 2013, suptitle not available
        %ts = ['v3=',num2str(lpara.v3),' m/s'];
        %ts = [ts,', mu3=',num2str(lpara.mu3),' rad/s'];
        %ts = [ts,', r0=',num2str(lpara.r0),' rad/s'];
        %annotation('textbox',[0.5,1.0,0,0],'string',ts)
        set(gcf,'Color','w')
    end 
