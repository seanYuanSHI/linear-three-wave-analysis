% This function fit for wave front of a2 from simulation data
% Assuming PostProcess.m has completed step 6
%
% The wave front is a staight line corresponds to xa=v2*ta in 
% analytical frame (xa,ta). 
% The siulation setup uses a different frame (x,t), and 
% the data readout uses yet another frame (xd,td=t), which  
% are related to the analytical frame by transformations
%    xd = x  - x0
%    xa = xd + x0 = x
%    ta = t  - t0    
% Due to these x and t offsets, this function fit for
%   t = t0 + (xd + x0)/v2
%
% Inputs:
%    apath  : struct that contains default file paths
%    its    : integer array, time slices to include
%             e.g. its=[12,13,14,17];
%             The corresponding simulation time in SI unit is stored in EA
%    flag   : flag>0 plot diagnostic figure
%
% Outputs:
%    save struct WF to file in same path, the struct contains inputs and the following
%    t0     : time origin of analytical frame in data frame, scalar
%    v2,iv2 : wave group velocity, scalar>0
%    alpha2 : simulation data for right moving seed, nx-by-nt
%    xa     : spatial grid in analytical frame, nx-by-1
%    ta     : time grid in analytical frame, 1-by-nt
%    inds   : x index of wave fronts, 1-by-nt
%    nfs    : index thickness of wave front, 1-by-nt
%
% Written by Yuan SHI, Oct 28, 2021

function WaveFront(apath, its, flag)
    if nargin<3
        flag=0; % default no plot
    end

    % check length
    nt = length(its);
    assert(nt>1,'Need more than 1 time slice!')
    % ensure its is in ascending order
    its = sort(its);

    % initialize storage
    inds = zeros(1,nt); % index location of wave front
    nfs = zeros(1,nt); % index thickness of wave front
    t = zeros(1,nt);

    % normalize to seed only amplitude
    load(char(apath.consts(2))) % eigenAK.Amps=[Ey,Ez]
    Aperp = norm(eigenAK.Amps);

    %%%%%%%%%%%%%%%%%%%%
    % process each time slice 
    disp('Processing simulation data...')
    froot = [apath.SS,'Envelope/'];
    for i=1:nt
        % load data
        fname = [froot,num2str(its(i),'%04.f'),'.mat'];
        load(fname)
        % load similation time
        t(i) = EA.time;
        % locate wave front
        ar = EA.ar/Aperp; % normalized
        alpha2(:,i) = ar;
        [ix,nf]= StepFinder(ar, flag-1);
        inds(i) = ix;
        nfs(i) = nf;
    end

    %%%%%%%%%%%%%%%%%%%%
    % fit for frame origin
    disp('Fitting for analytical frame...')
    % x grids
    x = EA.xr; % already includes x0 offset between data/simulation origins
    nx = length(x); % nx-by-1

    % x location of wavefronts in simulation frame
    % select out valid wave fronts
    xv = zeros(nt,1);
    tv = zeros(nt,1);
    nv = 0; % number of valid points
    for i=1:nt
        ix = inds(i);
        if ix<=nx
            nv = nv+1;
            tv(nv) = t(i);
            xv(nv) = x(ix);
        end
    end

    % fit valid data tv=xv/v2+t0 by linear regression
    % linear regressions y=P*q+e, where P is predictor
    % q is coefficients, and e is error
    % minimizes e'*e=(y-P*q)'*(y-P*q).
    % Take variation, q is solved from (P'*P)*q=P'*y
    disp('Linear regression...')
    % construct predictor matrix
    P = [xv(1:nv),ones(nv,1)];
    PP = P'*P;
    Py = P'*tv(1:nv);
    % solve linear system for q
    q = PP\Py;
    % unload coefficients
    iv2 = q(1);
    v2 = 1/iv2;
    t0 = q(2);
    ta = t-t0;

    %%%%%%%%%%%%%%%%%%%%
    % load struct
    WF.its = its;

    WF.t0 = t0;
    WF.v2 = v2;
    WF.iv2 = iv2;
    WF.alpha2 = alpha2;
    WF.xa = x; % same as simulation frame
    WF.ta = ta; 
    WF.inds = inds;
    WF.nfs = nfs;

    % save data
    save([apath.SS,'WFdata.mat'],'WF')

    %%%%%%%%%%%%%%%%%%%%
    % plot diagnostic figure
    if flag>0
        WF
        disp('Plotting figure...')
        figure
        [T,X]=meshgrid(ta,x);
        contourf(X,T,alpha2)
        c=colorbar;
        % mark wave front
        hold on
        plot(x,x*iv2,'Color',[0,0,0]+0.5,'LineWidth',3)
        % labels
        xlabel('xa (meter)')
        ylabel('ta (second)')
        ylabel(c,['alpha2/h0']);
        title(apath.fshead(end-15:end)) % last 15 characters

        %xlim([0,max(x)])
        %ylim([0,max(ta)])
        set(gcf,'Color','w')
    end
