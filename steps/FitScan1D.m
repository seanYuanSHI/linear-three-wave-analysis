% This function scans r0 or mu3 to quantify fit concertainty.
% Assume v3=0, so only need analytical solution in limiting form.
% The program first search for the range where the fit residual
% doubles its minimum, and then do a uniform scan in that range.
% From the scan, statistics are extracted and saved.
%
% Inputs:
%    ns    : number of uniform search points
%    spath : path to seed stimulated folder
%            processed data are saved in <spath>WFdata.mat
%   origin : origin=0: scan near analytic "a"
%            origin=1: scan near constrained best fit "c".
%            origin=2: scan near unconstrained best fit "u".
%    axis  : axis='r0' : scan r0 for fixed mu3, v=r0
%            axis='mu3': scan mu3 for fixed r0, v=mu3
%    srm   : scan range multiplier
%            when srm >=1, search in an interval within 
%                          (r0-srm*delta, r0+srm*delta)
%                          where delta= |a-o| for origin "o"
%            when 0<srm<1, search an ellipse within
%                          (1-srm)*o to (1+srm)*o
%            when srm<0, use srm as radius in unit of Trad/s
%    flag  : flag>0 also plot results
%
% Outputs: 
%    save to file <spath>Fits/lscan1D_<axis>.txt 
%         header: # its, inherit from <spath>lpara.mat
%         body  : each raw is of the pattern 
%                 (scanned value, residual per point)
%         The .txt file can be plotted by other programs.
%    save data to <spath>Fits/1Dstat_<axis>.mat', which contains
%         width of double the minimum for the residual
%           resmin : minimum residual per point 
%           vr     : right(larger) v at double min
%           vl     : left (smaller) v at double min
%           va     : analytical v 
%           v0     : depends on origin, save analytical, 
%                    constrained, or unconstrainedt best fit for v
%          vname   : name for v0
%                     when axis='r0' , vname is "r0a","r0c",or "r0u"
%                     when axis='mu3', vname is "mu3a","mu3c",or "mu3u"
%
% Last modified by Yuan SHI on June 30, 2022

function FitScan1D(ns, spath, origin, axis, srm, flag)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % maximum number of iteration 
    % when searching for range
    maxiter = 100;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % process inputs
    if nargin<3
        origin=1; % default scan around constrained
    end
    if nargin<4
        axis='r0'; % default scan r0
    end
    if nargin<5
        srm=1; % default scan full a-o separation
    end
    if nargin<6
        flag=0; % default no plot
    end

    assert(ns>2,'Need more than 1 scan points!')
    % check input
    if srm==0
        disp('srm should not be 0! Set srm to 1') 
        srm = 1
    end

    % always load analytic lpara0
    load([spath,'Fits/lpara0.mat'])
    % depending on origin, read parameters in lpara 
    if origin==1
        disp('Scan near constrained best fit')
        load([spath,'Fits/lpara1_limit1.mat']) % limiting form
        lpara = lpara1;
        vname = 'c';
    elseif origin==2
        disp('Scan near unconstrained best fit')
        load([spath,'Fits/lpara2_limit1.mat']) % limiting form
        lpara = lpara2;
        vname = 'u';
    else
        origin=0;
        disp('Scan near analytic')
        lpara = lpara0;
        vname = 'a';
    end
    lpara.limit = 1; % ensure using limiting form 

    % results from analytic and origin
    if strcmp(axis,'r0') 
        disp('Scan r0 for fixed mu3')
        va = lpara0.r0;
        v0 = lpara.r0;
    else
        disp('Scan mu3 for fixed r0')
        va = lpara0.mu3;
        v0 = lpara.mu3;
        axis = 'mu3'; % enfore 
    end

    % determin the scan range
    if srm>=1 % range determined by a-o separation
        if origin==0 % "a" is "o"
            disp(['Range determined as |a|/2'])
            % multiplier*difference
            dv = va/2;
        else % "a" differ from "o" 
            disp(['Range determined as ',num2str(srm),'*|a-o|'])
            % multiplier*difference
            dv = srm*abs(v0-va);
        end
    elseif srm>0 && srm<1 
        disp('Local scan near the origin')
        dv = srm*v0;
    else
        disp(['Scan in specified range in Trad/s'])
        dv = 1e12*abs(srm);
    end
    % report center and initial radius
    fprintf('center : v0 = %e\n',v0)
    fprintf('initial radius : dv = %e\n',dv)

    % open file for saving data
    fname = [spath,'Fits/lscan1D_',axis,'.txt'];
    fid = fopen(fname,'a'); % append
    % write file header, its from lpara
    fprintf(fid,'####################\n# its = %d',its(1));
    % write all elements of its 
    nl = length(its);
    for i=2:nl
        fprintf(fid,', %d',its(i));
    end
    % finish file header
    fprintf(fid,'\n# scan origin: %s', vname);
    fprintf(fid,'\n#\n# %s, residual per point',axis); 
    fprintf(fid,'\n####################\n');


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % search for scan range
    disp('Initial search of doubling range...')
    % initial guess of minimum residual
    resmin = FitResidual(spath,lpara,its);
    % initialize storage
    vscan = zeros(1,ns+2*maxiter);
    residual = zeros(1,ns+2*maxiter);
    % initialize counter
    ii = 0;

    % increase v until residual double
    res = resmin;
    vr = v0;
    ir = 1;
    while ir<maxiter && res<2*resmin
        % increase vr
        vr = vr + dv;
        ir = ir + 1;
        % update struct
        if strcmp(axis,'r0') % scan r0
            lpara.r0 = vr;
        else % scan mu3
            lpara.mu3 = vr;
        end
        % compute residual
        res = FitResidual(spath,lpara,its);
        % save data
        fprintf(fid,'%8.4e, %8.4e\n',vr,res);
        % internal storage
        ii = ii + 1;
        vscan(ii) = vr;
        residual(ii) = res;
    end
    % report anomolous termination causes
    if ir == maxiter
        disp('Maximum iterations reached before finding right bound!')
    end        

    % decrease v until residual double
    res = resmin;
    vl = v0;
    il = 1;
    while il<maxiter && vl>0 && res<2*resmin 
        % increase v
        vl = vl - dv;
        il = il + 1;
        % update struct
        if strcmp(axis,'r0') % scan r0
            lpara.r0 = vl;
        else % scan mu3
            lpara.mu3 = vl;
        end
        % compute residual
        res = FitResidual(spath,lpara,its);
        % save data
        fprintf(fid,'%8.4e, %8.4e\n',vl,res);
        % internal storage
        ii = ii + 1;
        vscan(ii) = vl;
        residual(ii) = res;
    end
    % report anomolous termination causes
    if il == maxiter
        disp('Maximum iterations reached before finding left bound!')
    end        
    if vl<0
        disp('Left bound is set to zero')
        vl = 0;
    end


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % uniform scan
    fprintf('Uniform scan within vl=%e and vr=%e\n',vl,vr)
    % search array, avoid end points already scanned
    vs = linspace(vl+dv/ns,vr-dv/ns,ns);

    % scan v
    for i = 1:ns
        % update struct
        if strcmp(axis,'r0') % scan r0
            lpara.r0 = vs(i);
        else % scan mu3
            lpara.mu3 = vs(i);
        end
        % compute residual
        res = FitResidual(spath,lpara,its);
        % save data
        fprintf(fid,'%8.4e, %8.4e\n',vs(i),res);
        % internal storage
        ii = ii + 1;
        vscan(ii) = vs(i);
        residual(ii) = res;
    end
    % close file
    fclose(fid);


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % compute width at bouble the minimum
    disp('Computing statistics...')
    % sort according to vscan
    [vsort,ind] = sort(vscan(1:ii));
    resort = residual(ind);

    % update minimum residual
    [rm, ind] = min(resort);
    resmin = min(resmin,rm);
    % double the minimum residual
    m2 = 2*resmin;
    % search for left and right index
    ir = ind;
    while ir<ii && resort(ir)<m2
        ir = ir+1;
    end
    il = ind;    
    while il>1 && resort(il)<m2
        il = il-1;
    end

    % linear interpolation using r0 at ir, il, and min
    xa = vsort([ir-1,il]);
    xb = vsort([ir,il+1]);
    ya = resort([ir-1,il]);
    yb = resort([ir,il+1]);
    x = xa + (xb-xa)./(yb-ya).*(m2-ya);
    vr = x(1);
    if il==1 && resort(1)<m2 % outside range
        vl = 0;
    else % within range
        vl = x(2);
    end

    % save data to .mat file
    fname = [spath,'Fits/1Dstat_',axis,'.mat'];
    vname = [axis,vname];
    save(fname,'vr','vl','va','v0','resmin','vname')


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % plot figure
    if flag>0
       figure
       plot(vsort, resort, 'r.-')
       % mark analytical 
       line([va,va],ylim,'Color','k','LineStyle','-')
       line([v0,v0],ylim,'LineStyle','--','Color',[1,1,1]/2)
       line([vl,vr],[m2,m2],'LineStyle','--','Color',[1,1,1]/2)
       % label axis
       xlabel([axis, ' (rad/s)'])
       ylabel('Fit residual per point')
       title(['...',spath(end-35:end)]) % last 35 character
       set(gcf,'Color','w')
    end
