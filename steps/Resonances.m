% This function performs step 4 of PostProcess
% by solving for three-wave resonances.
%
% Solve for resonances at three pump k's from analytic + eigen (Ey, Ez) 
% Since the sign of khat does not mattter for resonance search,
% it's sufficient to use theta instead of theta_w
% Need to complete step 3 for pump before performing this step.
%
% Inputs: 
%    theta  : wave propagation angle, in degree
%    apath  : struct contains default file paths
% Outputs: save in file
%    para    : plasma parameters, inherit from PumpPara.mat
%    sources : origins how pump k are determined
%    scatters: scattering map table for 3-wave resonances
%    gammaR  : backward Raman growth rate in unmagnetized 
%              plasma of the same density. In magnetized
%              case, the undamped temporal growth rate is 
%              gamma=M*gammaR, in Trad/s

function Resonances(theta, apath)

    % load common physical constants   
    constants % load c8 from Three-Wave/src/auxiliary/constants.m
    % load para, wave1, wave2 for pump
    disp('Reading pump wave parameters...')
    load(char(apath.paras(1)))
    disp('Plasma para inherited from PumpPara.mat')
    disp(['  Ns = ',num2str(para.Ns')])
    disp(['  Ts = ',num2str(para.Ts')])
    disp(['  B0 = ',num2str(para.B0)])

    % load Amps, kSIs, branches for pump
    disp('Reading eigen pump fits...')
    load(char(apath.consts(1))) % eigenAK

    % branch index from Ey
    b = eigenAK.branches(1);
    % initialize storage for ck1 values
    cks = [0,0,0];
    % read analytical k for the intended branch
    if b==1
        wave = wave1;
    else
        wave = wave2;
    end
    % first k from analytical
    cks(1) = wave.ck;
    % read additional k from Ey and Ez data
    cks(2:3) = c8*1e-4*eigenAK.kSIs; % in Trad/s 
 
    % loop three 3 k values and store data in array of struct
    disp('Computing scattering map...')
    % source where k is estimated
    sources = {'Analytic','Ey eigen', 'Ez eigen'};
    % unit k vector in wave frame of seed
    theta_rad = theta/180*pi;
    khat = [sin(theta_rad);0;cos(theta_rad)];
    for i=1:3
        disp(sources(i))
        % norm of ck
        ck = cks(i); 
        % compute pump frequency for given k
        omega = ck_2_w_warm(ck,theta,para,0);
        w1 = omega(b);
     
        % search for resonances for backscattering
        map = main(b,w1,-ck*khat,b,khat,para);
        scatters(i).map = map;
        scatters(i).ck1r = ck;
        scatters(i).w1 = w1;
        scatters(i).b = b;
     
        % print results in nice format
        map_print(map,w1)
    end

    % fname for resonances
    fname = apath.PR;
    % save data
    fprintf(['Saving resonance data in \n',fname,'\n'])
    save(fname,'para','sources','scatters')
