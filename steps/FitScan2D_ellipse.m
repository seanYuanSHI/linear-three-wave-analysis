% This function scans (r0, mu3) parameter space
% to get a sense of the fit concertainty.
% Scan near constrained best fit with ranges determined from
% "a": analytic, "c": constrained, "u": unconstrained
% Since mu3 is nonzero, need to use full analytical solution.
%
% Inputs:
%    srm   : scan range multiplier, 
%            when srm >=1, search in an ellipse within 
%                          (x-srm*delta, x+srm*delta)
%                          where x is r0 or mu3, and delta= a-c or c-u
%            when srm < 1, search an ellipse within
%                           (1-srm)*c to (1+srm)*c
%    ns    : total number of search point in 2D plane is ~ns
%    spath : path to seed stimulated folder
%            processed data are saved in <spath>WFdata.mat
%
% Output: save to file <spath>Fits/lscan2D.txt 
%         header: # its, inherit from <spath>Fits/lpara.mat
%         body  : each raw is of the pattern 
%                 (r0, mu3, residual per point)
%         The .txt file can be plotted by other programs.
%
% Last modified by Yuan SHI on June 1, 2022

function FitScan2D_ellipse(srm, ns, spath)
    % check input
    srm = abs(srm); % ensure positive
    assert(srm~=0,'range multiplier should be positive!')
    assert(ns>2,'Need more than 2 points in each direction!')

    % read parameters from fit, its, lpara0,lpara1,lpara2
    load([spath,'Fits/lpara0.mat'])
    % try to read limit=0 data
    try
        load([spath,'Fits/lpara1_limit0.mat'])
        disp('lpara1 is for limit=0: full solution')
    catch
        load([spath,'Fits/lpara1_limit1.mat'])
        disp('lpara1 is for limit=1: limiting form')
        % use analytical v3
        lpara1.v3 = lpara0.v3;
    end    
    % results from constrained fit
    mu3c = lpara1.mu3;
    r0c = lpara1.r0;

    if srm>=1 % range determined by a-c or c-u
        try % unconstrained fit available
            load([spath,'Fits/lpara2_limit0.mat'])
            disp('lpara2 for limit=0 is available')
            disp(['Range determined as ',num2str(srm),'*|u-c|'])
        catch % no unconstrained fit available, use analytic instead
            disp(['Range determined as ',num2str(srm),'*|a-c|'])
            lpara2 = lpara0;
        end
        % results from unconstrained fit
        mu3u = lpara2.mu3;
        r0u = lpara2.r0;
        % multiplier*difference
        dmu = srm*abs(mu3c-mu3u);
        dr0 = srm*abs(r0c-r0u);

    else 
        disp('Local scan near constrained best fit')
        lpara2 = lpara1;
        dmu = srm*mu3c;
        dr0 = srm*r0c;
    end
    % report center and radius
    fprintf('center : r0c = %e, mu3c = %e\n',r0c, mu3c)
    fprintf('radius: dr0 = %e, dmu = %e\n',dr0, dmu)

    % search along concentric ellipses with fixed arc length
    % number of ellipse
    nr = ceil(sqrt(ns)); 
    % circumference of ellipse c~pi*(a+b)
    c0 = pi*(dmu+dr0);
    % total circumference of all ellipse
    % s = \sum_k=1^n c0/k ~ (log n + gamma)*c0
    % where gamma~0.5772 is Euler gamma constant
    cs = c0*(log(nr)+0.5772);
    % arc length for scan
    ds = cs/ns;

    % open file for saving data
    fname = [spath,'Fits/lscan2D.txt'];
    fid = fopen(fname,'a'); % append
    % write file header
    fprintf(fid,'####################\n#its = %d',its(1));
    % write all elements of its 
    nl = length(its);
    for i=2:nl
        fprintf(fid,', %d',its(i));
    end
    % finish file header
    fprintf(fid,'\n####################\n');
    fprintf(fid,'## r0, mu3, residual per point \n');


    % load value
    disp('Scanning (mu3,r0)...')
    % copy lpara to be constrained best fit
    lpara = lpara1;
    lpara.limit=0; % ensure using full solution for mu3~=0
    % scan from outer ellipse to inner ellipse
    for ir = 1:nr
        fprintf(['ir/nr = ',num2str(ir),'/',num2str(nr),'\n'])
        % circumference of the ellipse
        ci = c0/ir;
        % angle for given arc length
        dt = 2*pi*ds/ci; % in rad
        % number of angles to scan
        %nt = ceil(2*pi/dt);
        nt = ceil(ci/ds);
        % azimuthal scan
        for it = 1:nt
            fprintf(['it/nt = ',num2str(it),'/',num2str(nt),'\n'])
            % update lpara for current parameters
            theta = dt*(it-1);
            % ensure r0 and mu3 are positive
            r0 = abs(r0c + dr0*cos(theta)/ir);
            mu3 = abs(mu3c + dmu*sin(theta)/ir);
            %fprintf('theta (def)=%f, r0=%e, mu3=%e\n',theta/pi*180,r0,mu3)

            % update struct
            lpara.mu3 = mu3;
            lpara.r0 = r0;
            % compute residual
            res = FitResidual(spath,lpara,its);
            % save data
            fprintf(fid,'%8.4e, %8.4e, %8.4e\n',r0, mu3, res);
        end
    end
    % close file
    fclose(fid);
