% This function estimate elliptical polarization angle psi
% from simulation data Ey and Ez. 
%
% For wave propagation along x direction and B0 field in the
% x-z plane, eigenmodes are elliptically polarized with the 
% major/minor axes along the y and z axis. This is because of
% parity, time-reversal, and charge-conjugation symmetry, which
% demands that the polarization ellipse is invariant under
% reflection about the x-z plane.
%
% The perpendicular components of an eigenmode is of the form
%     e_perp = sin(psi)*sin(k*x-w*t) * (y_hat)
%            + cos(psi)*cos(k*x-w*t) * (k_hat \times y_hat)
% Notice that for k_hat = x_hat, (k_hat \times y_hat) = z_hat
% while for k_hat = -x_hat, (k_hat \times y_hat) = -z_hat.
% Nevertheless, in both cases, upto an overall sign
%     Ey = sin(psi)*sin(|k|x') 
%     Ez = cos(psi)*cos(|k|x') 
% where x'=x-w*t/k   
%
% Suppose "1" and "2" are two near degenerate eigenmodes with the
% the same frequency but different k1 and k2, then the psi angles
% are such that
%    Ey = E1*sin(psi1)sin(k1*x-w*t) 
%       + E2*sin(psi2)sin(k2*x-w*t) 
% 
%    Ec = E1*cos(psi1)cos(k1*x-w*t) 
%       + E2*cos(psi2)cos(k2*x-w*t) 
% where Ec is the k_hat\times y_hat component
% E1 and E2 are constants.
%
% Inputs:
%    data contains the following
%        Ey, Ez  : simulation data, vectors of length nx
%        dx      : uniform grid spacing, scalar
%    k1, k2  : initial guess of wave vectors, real scalars
%    flag    : flag>0 plot diagnostic figures
%              and print diagnostic messages
% Outputs:
%    psi1, psi2 : polarization angles in radians, scalars
%
% Written by Yuan SHI, Oct 9, 2021

function [psi1, psi2] = EigenPsi(data, k1, k2, flag)

    % check inputs
    if nargin<2
        flag = 0
    end
    % unpack
    Ez = data.Ez;
    Ey = data.Ey;
    dx = data.dx;

    % use abs of k
    k1a = abs(k1);
    k2a = abs(k2);

    % plot flags
    if flag>1
        yflag='Ey';
        zflag='Ez';
    else
        yflag=0;
        zflag=0;
    end

    % separate out contributions of the two
    % eigenmodes from data using NearDegenerate
    % f(x)=A1*sin(k1*x+b1) + A2*sin(k2*x+b2)
    % search for k to optimize the fit
    ypara = NDsearch(Ey, dx, k1a, k2a, yflag); 
    zpara = NDsearch(Ez, dx, k1a, k2a, zflag); 

    % sign convention s.t. Ey is always positive
    E1s1 = ypara.A1;
    E2s2 = ypara.A2;
    % sign for Ez depends on the propagation direction
    % but when ka is used, the sign is always positive
    E1c1 = zpara.A1;
    E2c2 = zpara.A2;

    % Correct signs due to phase jump when needed
    % The phase of Ez should lead the phase of Ey by pi/2.
    s1 = PhaseShift(ypara.b1, zpara.b1);
    if s1==1 % phase shift needed
        E1c1 = -E1c1;
        zpara.b1p = zpara.b1 + pi;
    end
    s2 = PhaseShift(ypara.b2, zpara.b2);
    if s2==1 % phase shift needed
        E2c2 = -E2c2;
        zpara.b2p = zpara.b2 + pi;
    end

    % compute psi angles
    % p = atan2(y,x)
    psi1 = atan2(E1s1, E1c1);
    psi2 = atan2(E2s2, E2c2);

