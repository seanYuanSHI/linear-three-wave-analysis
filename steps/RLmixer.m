% This function performs step=4 (lambda<0) for PostProcess.m
% The function add pump-eigen-only data and seed-eigen-only data
% and optimizes phase velocities vr and vl 
% that will be used to extract right/left moving waves.
%
% In RightLeft.m, the electric field of right/left waves are 
%    right : Ery = (Ey+vl*Bz)/(1+vl/vr),  Erz = (Ez-vl*By)/(1+vl/vr)
%    left  : Ely = (Ey-vr*Bz)/(1+vr/vl),  Elz = (Ez+vr*By)/(1+vr/vl)
%
% Inputs:
%    pdata: struct for pump-eigen data
%    sdata: struct for seed-eigen data
%      Both pdata and sdata contain Ey, Ez, By, Bz, w
%    Amps : expected amplitudes [Ary,Arz,Aly,Alz]
%    kSIs : expected wave vectors [kry,krz,kly,klz]
%    flag : flag>0 plot diagnostic figure
% Output:
%    vopt : optimal phase velocities
%           vopt=[vr,vl]
%    aux  : struct that contains data for IO when flag>0
%            x: spatial grid
%            Esy, Epy: Ey for seed and pump separately
%            Ery, Ely: Ey for right and left moving waves
%
% Written by Yuan SHI on June 6, 2022 

function [vopt, aux] = RLmixer(pdata, sdata, Amps, kSIs, flag) 
    % default no plot
    if nargin<3
        flag = 0;
    end

    % index range of pump data
    ilp = pdata.il;   
    irp = pdata.ir;  
    np = irp - ilp + 1; 
    % index range of seed data
    ils = sdata.il;   
    irs = sdata.ir;  
    ns = irs - ils + 1; 

    % difference of il and ir
    dil = ilp - ils; % expect>0
    dir = irp - irs; % expect>0

    % initial and final index for pump data
    pi = max(1,1-dil);
    pf = min(np, np-dir);
    % initial and final index for seed data
    si = max(1,1+dil);
    sf = min(ns, ns+dir);

    % data in common range 
    data.Ey = pdata.Ey(pi:pf) + sdata.Ey(si:sf);
    data.Ez = pdata.Ez(pi:pf) + sdata.Ez(si:sf);
    data.By = pdata.By(pi:pf) + sdata.By(si:sf);
    data.Bz = pdata.Bz(pi:pf) + sdata.Bz(si:sf);
    % load aux data, +1 to match with RL grid
    aux.Epy = pdata.Ey(pi+1:pf);
    aux.Esy = sdata.Ey(si+1:sf);

    % common dx
    dx = pdata.dx;
    sdx = sdata.dx;
    assert(sdx==dx,['dx mismatch! pump dx=',...
           num2str(dx),' seed dx=',num2str(sdx)])
    % load auxilliary parameters
    data.dx = dx;
    data.t = pdata.t;
    data.i0 = pdata.i0;
    data.il = pdata.il;
    data.ir = pdata.ir;

    % objective function
    % vps=[vry,vrz,vly,vlz]
    % nested function knows about variables in parent 
    function f = fRLmixer(vps)
        % extract r/l waves
        RL = RightLeft(data,vps);

        % extract amplitudes
        ARLs = zeros(1,4);
        % RL.Edata
        %   Edata(:,1)=Ery, Edata(:,2)=Erz
        %   Edata(:,3)=Ely, Edata(:,4)=Elz
        for i=1:4
            % extract envelope 
            [~,Ac] = Envelope(RL.Edata(:,i),dx,kSIs(i));
            % mean envelope
            ARLs(i) = abs(mean(Ac));
        end

        % difference from expected
        Adif = ARLs - Amps;
        % normalized difference
        f = norm(Adif);
    end

    % construct function handle for the search
    fun = @(vs)fRLmixer(vs);

    % options for the search
    if flag>1 % plot function value at each iteration
        %options = optimset('PlotFcns',@optimplotfval,'Display','iter');
        options = optimset('PlotFcns',@optimplotfval,'Display','notify');
    else % use default
        options = optimset();
    end

    % initial guess 
    % vps=[vr,vl]
    % vp=w/k, w in Trad/s, k in SI
    % kSIs = [kry,krz,kly,klz]
    v0(1) = mean(1e12*sdata.w./kSIs(1:2)); % seed vp
    v0(2) = mean(1e12*pdata.w./kSIs(3:4)); % pump vp

    % search for optimized value
    disp('Optimizing for phase velocities...')
    vopt = fminsearch(fun, v0, options);

    % plot figure
    if flag>0
        % extract right/left
        RL = RightLeft(data,vopt,flag);
        % mark amplitudes
        % Amps = [Ary,Arz,Aly,Alz]
        subplot(2,1,1)
        line(xlim,[Amps(1),Amps(1)],'Color','m');
        line(xlim,-[Amps(1),Amps(1)],'Color','m');
        line(xlim,[Amps(3),Amps(3)],'Color','c');
        line(xlim,-[Amps(3),Amps(3)],'Color','c');
        xlabel('ix')

        subplot(2,1,2)
        line(xlim,[Amps(2),Amps(2)],'Color','m');
        line(xlim,-[Amps(2),Amps(2)],'Color','m');
        line(xlim,[Amps(4),Amps(4)],'Color','c');
        line(xlim,-[Amps(4),Amps(4)],'Color','c');
        xlabel('ix')

        % x axis
        x = dx*((max(ilp,ils)+1:min(irp,irs))-pdata.i0);
        % Edata is from E(2:end)
        %   Edata(:,1)=Ery, Edata(:,2)=Erz
        %   Edata(:,3)=Ely, Edata(:,4)=Elz
        % load aux data
        aux.x = x;
        aux.Ery = RL.Edata(:,1);
        aux.Ely = RL.Edata(:,3);
        aux.Ary = Amps(1);
        aux.Aly = Amps(3);

        % plot residual
        figure
        subplot(2,1,1)
        plot(x, RL.Edata(:,1)-sdata.Ey(si+1:sf),'b')
        hold on
        plot(x, RL.Edata(:,2)-sdata.Ez(si+1:sf),'r')
        legend('Ery','Erz')
        xlabel('x (um)')
        ylabel('Er residual (V/m)')
        title(['Ar=',num2str(norm(Amps(1:2)),'%e'),' (V/m)'])

        subplot(2,1,2)
        plot(x, RL.Edata(:,3)-pdata.Ey(pi+1:pf),'c')
        hold on
        plot(x, RL.Edata(:,4)-pdata.Ez(pi+1:pf),'m')
        legend('Ely','Elz')
        xlabel('x (um)')
        ylabel('El Residual (V/m)')
        title(['Al=',num2str(norm(Amps(3:4)),'%e'),' (V/m)'])
        set(gcf,'Color','w')
    end
end
