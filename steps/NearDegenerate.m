% This function extract amplitudes of two near degenerate waves
% of the form y(x)=A1*sin(k1*x+b1) + A2*sin(k2*x+b2)
% where all parameters are constants and |k1-k2|<<k1,k2.
%
% If there is sufficient box size and resolution, then the
% two peaks can be distinguished from FFT. However, this function
% is dealing with the situation where the two peaks are not well
% separated or even merged into one. The method assumes that 
% k1 and k2 are known exactly, and then uses linear regression
% to determine other parameters.
%
% For simplicity, write cos(k1*x)=c1, sin(k2*x)=s2 and so on.
% Then y = A1*(s1*cos(b1)+c1*sin(b1))
%        + A2*(s2*cos(b2)+c2*sin(b2))
%        = (ac1*s1+as1*c1) + (ac2*s2+as2*c2)
% where ac1=A1*cos(b1) and so on. The linear regression fits
% for these four parameters.
%
% Inputs:
%    y     : y values on uniform grid, vectors of size n-by-1
%    dx    : uniform grid spacing, scalar
%    k1,k2 : exact values of the near degenerate waves, real scalars
%    flag  : if flag=0, do not plot diagnostic figures
%            otherwise, plot figures with 'flag' as figure name 
% Outputs:
%    ndpara  : a struct contains the following
%            ndpara.A1     : A1 ...Positive
%            ndpara.b1     : b1 ... Between -pi and +pi
%            ndpara.ac1    : fit parameters
%            ndpara.r      : norm of residual vector normalized by # points
%
% Written by Yuan SHI, Sep 17, 2021 

function ndpara = NearDegenerate(y,dx,k1,k2,flag)

    % default no plot
    if nargin<5
        flag = '0';
    else % convert to str
        flag = num2str(flag);
    end

    % check y dimension
    s = size(y);
    assert(min(s)==1,'y should be a vector')
    if s(2)>s(1)
        y = y';
    end
    ny = length(y);

    % initialize output by adding doc string
    ndpara.doc = 'y=A*sin(k*x+b), ac=A*cos(b), as=A*sin(b)';

    % generate reference signals
    x = dx*(0:ny-1)'; % column vector
    s1 = sin(k1*x);
    s2 = sin(k2*x);
    c1 = cos(k1*x);
    c2 = cos(k2*x);

    % linear regressions y=P*q+e, where P is predictor
    % q is coefficients, and e is error
    % minimizes e'*e=(y-P*q)'*(y-P*q).
    % Take variation, q is solved from (P'*P)*q=P'*y
    % construct predictor matrix
    P = [s1,c1,s2,c2];
    PP = P'*P;
    Py = P'*y;
    % solve linear system for q
    q = PP\Py;
    % store values
    % y= (ac1*s1+as1*c1) + (ac2*s2+as2*c2)
    ndpara.ac1 = q(1); 
    ndpara.as1 = q(2);
    ndpara.ac2 = q(3); 
    ndpara.as2 = q(4); 
   
    % compute amplitude and phase
    % [theta,rho]=cart2pol(x,y)
    % theta in range [-pi, pi]
    [b1, A1] = cart2pol(q(1),q(2)); 
    [b2, A2] = cart2pol(q(3),q(4));
    % store values
    ndpara.A1 = A1; 
    ndpara.b1 = b1; 
    ndpara.A2 = A2; 
    ndpara.b2 = b2;

    % reconstruction    
    z = A1*sin(k1*x+b1)+A2*sin(k2*x+b2); 
    % residual
    r = y-z;
    % record norm of the residual vector
    ndpara.r = norm(r)/ny;

    % plot diagnostic figures
    if strcmp(flag,'0')~=1
        % omit default name
        if strcmp(flag,'1')==1
            flag = '';
        end

        % plot Fourier
        figure('Name',flag)
        [k,f]=Fourier(y,dx,1);
        % mark expected k
        ak1=abs(k1);
        ak2=abs(k2);
        line([ak1,ak1],ylim,'Color',[0.5,0.5,0.5]);
        line([ak2,ak2],ylim,'Color',[0.5,0.5,0.5]);

        % plot reconstructed data and residual
        figure('Name',flag)
        subplot(2,1,1)
        plot(x,y,'.-')
        hold on
        plot(x,z,'r')
        legend('data','reconstruction')
        xlabel('x')

        subplot(2,1,2)
        plot(x,r)
        legend('data-recon')
        xlabel('x')
        ylabel('residual')        

        set(gcf,'Color','w')
    end
