% This function computes envelopes of right/left waves
% from the Ey and Ez components, and compute total 
% E envelope as well as the y/z ratios.
%
% Inputs:
%    RL   : struct that is output by RightLeft.m
%            Edata(:,1)=Ery, Edata(:,2)=Erz
%            Edata(:,3)=Ely, Edata(:,4)=Elz
%            dx   : uniform grid spacing, scalar
%    kSIs : norm of wave vectors in SI unit
%           kSIs = [kry, krz, kly, klz]
%           where kry is the k for right moving Ey, and so on
%    flag : if flag>0 plot basic diagnostic figures: envelope
%           if flag>1 plot additional diagnostic figures: optimization, ratio
% Output:
%    EA   : struct that contains the following fields
%           comps   : {'ry','rz','ly','lz'}, doc string recording components
%           AX      : 1x4 struct array contains envelopes and x grids
%                     AX(1) for Ery, and so on
%                     AX.Ac is the envelope
%                     AX.xc is the x grid
%                     Notice that right/left have different wavelengths
%                     so their x grids are different 
%           ar,al   : total amplitudes of right/left envelope =sqrt(y^2+z^2)
%           xr,xl   : x grid for right/left envelope
%           ryz,lyz : right/left amplitude ratios, ryz=Ey/Ez from linear regression
%
%    aux  : struct for IO purpose
%           xr ,xl  : same as the above
%           ary,aly : envelope of y component
%           Ery,Ely : Ey for right/left waves
%           x       : x grid for Ey
%
% Written by Yuan SHI, Oct 26, 2021
% Last modified by Yuan SHI on Jun 28, 2022

function [EA, aux] = RLenvelope(RL, kSIs, flag)
        if nargin<3
            flag=0; % default no plot
        end

        % load data
        Edata=RL.Edata;
        dx = RL.dx;
        % data x offset from simulation origin
        x0 = dx*(RL.il-RL.i0);

        % Initialize output by adding doc string
        comps = {'ry','rz','ly','lz'};
        EA.comps = comps;

        % deside if to plot diagnostic figures
        if flag>1
            fcomps = comps;
        else
            fcomps = '0000';
        end

        % process envelope for each component
        for i=1:4
            % extract envelope
            % right/left waves have different wavelength & grid
            [xc, Ac] = Envelope(Edata(:,i),dx,kSIs(i),char(fcomps(i)));
            % store data
            AX(i).Ac = Ac;
            AX(i).xc = xc;
        end

        % check right/left components have same length
        nry = length(AX(1).xc);
        nrz = length(AX(2).xc);
        nr = min(nry,nrz);
        if nry~=nrz
            disp(['!!! Unmatched: nry=',num2str(nry),' nrz=',num2str(nrz)]);
        end
        nly = length(AX(3).xc);
        nlz = length(AX(4).xc);
        nl = min(nly,nlz);
        if nly~=nlz
            disp(['!!! Unmatched: nly=',num2str(nly),' nlz=',num2str(nlz)]);
        end

        % unload components for further processing
        ary = AX(1).Ac(1:nr);
        arz = AX(2).Ac(1:nr);
        aly = AX(3).Ac(1:nl);
        alz = AX(4).Ac(1:nl);

        % compute total amplitude of right/left waves
        ar = sqrt(ary.^2+arz.^2);
        al = sqrt(aly.^2+alz.^2);

        % compute amplitude ratios Ey/Ez
        ryz = (ary'*arz)/(arz'*arz);
        lyz = (aly'*alz)/(alz'*alz);
        
        % store data in struct
        EA.AX = AX;
        EA.ar = ar;
        EA.al = al;
        EA.xr = AX(1).xc(1:nr) + x0;
        EA.xl = AX(3).xc(1:nl) + x0;
        EA.ryz = ryz;
        EA.lyz = lyz;
        EA.time = RL.time;

        % load auxilliary data for IO
        aux.xr = EA.xr;
        aux.xl = EA.xl;
        aux.ary = ary;
        aux.aly = aly;
        aux.Ery = Edata(:,1);
        aux.Ely = Edata(:,3);
        aux.x = dx*((RL.il:RL.ir)'-RL.i0);

        % plot figures
        if flag>0 % plot total envelopes
            figure('Name','total envelope')
            plot(EA.xr,ar,'r.-')
            hold on
            plot(EA.xl,al,'b.-')
            xlabel('x')
            ylabel('a')
            legend('right','left')
            set(gcf,'Color','w')
        end

        if flag>1 % plot ratio reconstruction
           figure('Name','envelope ratio')
           plot(EA.xr,ary,'r.-')
           hold on
           plot(EA.xr,arz*ryz,'m.-')
           plot(EA.xl,aly,'b.-')
           plot(EA.xl,alz*lyz,'c.-')
           xlabel('x')
           ylabel('ay')
           legend('ry','rz*(ry/rz)','ly','lz*(ly/lz)')
           set(gcf,'Color','w')
        end
   
