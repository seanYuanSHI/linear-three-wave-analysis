% This function extract the envelope of 
%   y(x)=A(x)*sin(k*x+b)
% where A(x) is the slowly varying envelope
% k*x is the fast varying phase, and b is
% some initial phase. 
%
% This is numerical phase-lock amplifier.
% The program first generates a reference signal 
%    z(x)=sin(k*x+p)
% and mix the reference with the signal
%    m(x)=y(x)*z(x)
%        =(A(x)/2)*[cos(b-p)-cos(2*k*x+b+p)]
% By averaging m(x) over known wavelength
%    <m>=A(x)/2*cos(b-p)
% Finally, to determine A, the phase p is 
% selected to maximize <m>.
%
% To reduce systematic error due to sampling phase mismatch
% the data is interpolated onto a uniform grid such that
% each period of the wave is sampled by integer number of points
%
% Inputs:
%    y    : data possibly containing many waves, ny-by-1
%           assuming the data is on a uniform x grid
%    dx   : the uniform grid spacing
%    k    : wave vector, assumed to be known exactly
%           the wavelength is l=2*pi/k
%    flag : if flag=1, plot envelope figure
%           if flag=2, also plot diagnostic figures
%           if flag=str, only plot envelope with str as figure name
% Outputs:
%    xc : the down-sampled cell centers, nc-by-1, start from origin
%    Ac : the envelope on the cell centers, nc-by-1
%
% The outputs are of length
%    nc~ny*(box length)/(wavelength)
%
% Written by Yuan SHI on Oct 25, 2021

function [xc, Ac] = Envelope(y, dx, k, flag)
    % default no plot
    if nargin<4
        flag = '0';
    else % ensure flag is str
        flag = num2str(flag);
    end
    % ensure k is positive
    k = abs(k);

    % check y is a column vector
    s = size(y);
    assert(min(s)==1,'y should be a column vector');
    if s(2)~=1 % row vector
        y=y';
    end
    % length of y
    ny = length(y);
    % construct x column vector
    x = dx*(0:ny-1)';

    % expected wavelength
    l = 2*pi/k;
    % grids per wavelength
    nw = ceil(l/dx);
    % number of down sampled grids
    % equals to (box size)/(wave length)
    nc = floor(ny/nw);

    % new grid where each period is divided 
    % into integer number of points
    xv = linspace(0,l*nc,nw*nc+1);
    % interpolate data onto new grid
    yv = interp1(x,y,xv); 

    % mixer function
    % return phase-locked average of m
    % nested function knows about variables in parent function
    function ma = Mixer(p)
        % reference signal
        zv = sin(k*xv+p);
        % initialize outputs
        ma = zeros(nc,1);
        
        % pointwise multiplication
        yz = yv.*zv;
        % Average over wavelength
        for i=1:nc
            % initial index
            i0 = (i-1)*nw+1;
            % arithmatic average
            ma(i) = 2*mean(yz(i0:i0+nw-1));
        end
    end

    % objective function for phase locking
    % to be used with fminbnd
    function f = Pscanner(q)
        % mixer value
        ma = Mixer(q);
        % negative norm of ma
        f = -norm(ma);
    end

    % options for monitoring the min finder
    if strcmp(flag,'0')~=1 && strcmp(flag,'1')~=1 
        % plot at each iteration
        %options = optimset('PlotFcns',@optimplotfval,'Display','iter');
        options = optimset('PlotFcns',@optimplotfval,'Display','notify');
    else % use default
        options = optimset();
    end

    % function handle
    fun = @(q)Pscanner(q);
    % search for optimal phase within [-pi,pi]
    b = fminbnd(fun,-pi,pi,options);

    % extract envelope for optimal phase
    Ac = Mixer(b);
    % generate x basis
    % cell boundaries
    xb = linspace(0,l*nc,nc+1)';
    % cell centers
    xc = xb(1:nc)+l/2;

    % plot figures
    if strcmp(flag,'0')~=1
        figure('Name',flag)
        plot(x,y,'k.-')
        hold on
        plot(xv,yv,'o')
        plot(xc,Ac,'r.-')
        plot(xc,-Ac,'r.-')
        xlabel('x')
        ylabel('y')
        legend('data','interp','envelope')
        set(gcf,'Color','w')
    end
end
    
