% This function uses simulation data from calibration runs
% to determine the optimal phase velocities to be used by
% RightLeft.m. The optimization minimizes the flux ratio sn/sp
% where "p" is the intended wave propagation, and "n" is the
% opposite propagation direction.
%
% In RightLeft.m, the electric field of right/left waves are 
%    right : Ery = (Ey+vl*Bz)/(1+vl/vr),  Erz = (Ez-vl*By)/(1+vl/vr)
%    left  : Ely = (Ey-vr*Bz)/(1+vr/vl),  Elz = (Ez+vr*By)/(1+vr/vl)
%
% Inputs:
%   data   : struct that contans Ey,Ez,By,Bz at one time slice
%            All components are AC only, with DC contribution subtracted.
%            Assume the data is for unidirectional wave propagation.
%   v0     : Initial guess of phase velocity, scalar
%            v0>0 for right propagating wave
%            v0<0 for left propagating wave
%   flag   : flag=1 plot basic figures
%            flag=2 also plot diagnostic figures
% Outputs:
%    vopt  : optimized phase velocities [vr,vl]
%            The phase velocities depend on the wave mode.
%
%            When v0>0, vl is used to extract Ery and Erz
%              When the data is for right-moving seed, minimize left/right.
%              In this case, left-moving data are close to noise.
%              The minimization tries to eliminate left-moving flux,
%              so vr is better constrained and more sensitive.
%              This vr is specific for seed wavelength.
%
%            When v0<0, vr is used to extract Ely and Elz
%              When the data is for left-moving pump, minimize right/left.
%              In this case, right-moving data are close to noise.
%              The minimization tries to eliminate right-moving flux,
%              so vl is better constrained and more sensitive.
%              This vl is specific for pump wavelength.
%
% Written by Yuan SHI, Oct 13, 2021
% Revised by Yuan SHI, June 6, 2022

function vopt = RLsearch(data, v0, flag)

    % default no plot
    if nargin<3
        flag = 0;
    end
    
    % construct objective function
    % vps=[vry,vrz,vly,vlz]
    % nested function knows about variables in parent 
    function f = fRLsearch(vps, iflag)
        % extract r/l waves
        RL = RightLeft(data,vps,iflag);
 
        if v0>0 % right propagating
            % minimize left flux
            f = RL.sl/RL.sr;
        else % left propagating
            f = RL.sr/RL.sl;
        end
    end
    % construct function handle for the search
    fun = @(vs)fRLsearch(vs,0);

    % options for the search
    if flag>1 % plot function value at each iteration
        %options = optimset('PlotFcns',@optimplotfval,'Display','iter');
        options = optimset('PlotFcns',@optimplotfval,'Display','notify');
    else % use default
        options = optimset();
    end

    % initial guess 
    va = abs(v0);
    % search for optimized value
    vopt = fminsearch(fun, [va,va], options);

    % plot figure
    if flag>0
        fRLsearch(vopt,1);
    end
end
