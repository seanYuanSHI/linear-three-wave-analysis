% This function load v2 from WFdata.mat
% to avoid loading variable into static work space
% Processed data are saved in <spath>WFdata.mat

function v2 = LoadWFv2(spath)
    load([spath,'WFdata.mat'])
    v2 = WF.v2;
