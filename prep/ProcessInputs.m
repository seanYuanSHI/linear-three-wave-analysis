% This script process inputs for PostProcess.m
disp('%%%%%%%%%%%%%%%%%%%%%%')
% report how lambda is used
slambda = sign(lambda);
if step<5
    disp('The sign of lambda is significant')
else
    disp('lambda is in-significant')
end
if step==1 
    disp('The value of lambda is significant')
    alambda = abs(lambda);
    disp(['lambda = ',num2str(alambda),' um'])
else
    disp('The value of lambda is in-significant')
end

% report if its is used
if step~=1 && (step~=4 || slambda>0) && step~=9
   disp('Time slices are: ')
   its = sort(ceil(abs(its))) % print
   nt = length(its);
else
   disp('Time slices in-significant')
end

% report flag status
if flag==0
    disp('flag=0: no plot will be shown')
elseif flag==1
    disp('flag=1: only major figures will be shown.')
elseif flag==2
    disp('flag=2: all diagnostic figures will be shown')
else
    disp(['flag=',num2str(flag)])
end

% report other inputs
disp(['Pump path is ',fhead])
disp(['Seed path is ',fhead,shead])
disp(['Performing step ',num2str(step)])


% overwrite parameters
if length(overwrite)>0
    disp('Parameter overwrite specified.')
    if strcmp(overwrite(1),'B')
        disp('Overwrite B0')
        B0 = sscanf(overwrite,'B%f')
    elseif strcmp(overwrite(1),'N')
        disp('Overwrite n0')
        n0 = sscanf(overwrite,'N%f')
    elseif strcmp(overwrite(1),'t')
        disp('Overwrite theta')
        theta = sscanf(overwrite,'theta%f')
    elseif strcmp(overwrite(1),'T')
        disp('Overwrite T0')
        % convert eV to keV
        T0 = 1e-3*sscanf(overwrite,'T%f')
    else
        disp('Unrecognized: no overwrite')  
    end 
end

% pump use name 1 and seed use name 2
% wave propagation angle in degree 
if slambda>0 % seed in x direction
    iname = 2;
    theta_w = theta;
    if step<=3
        disp('Processing seed...')
    end
else % pump in -x direction
    iname = 1;
    theta_w = 180-theta;
    if step<=3
        disp('Processing pump...')
    end
end

if step>3
    disp('Processing stimulated...')
end
