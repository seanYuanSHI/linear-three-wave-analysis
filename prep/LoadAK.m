% This function load amplitudes and wave vectors 
% for pump and seed eigen runs after step=3
%
% Input:
%    apath : struct that contains default file paths
%    name  : choose from 'Amps' and 'kSIs'
% Outout:
%    values: Amps or kSIs, length 1-by-4
%            Amps = [Ary,Arz,Aly,Alz]
%            kSIs = [kry,krz,kly,klz]
%
% Written by Yuan SHI on June 6, 2022


function values = LoadAK(apath,name)

    % load seed constants, stored in eigenAK
    load(char(apath.consts(2)))
    values(1:2)=eigenAK.(name);
    
    % load pump constants update eigenAK
    load(char(apath.consts(1)))
    values(3:4)=eigenAK.(name);
