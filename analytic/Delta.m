% The Delta0 and Delta1 functions at multiple time slices
% This is a wrapper function that calls tDelta.m,
% tDeltaLimit.m, or DeltaLimit.m depending on parameters.
%
% Inputs:
%    x     : spatial grid, vector, x>0
%    t     : time, vector, t>0
%    lpara : struct that contains the following in SI unit
%            v2  : group velocity of a2, >0
%            v3  : group velocity of a3, >=0
%            mu2 : damping coefficient for a2, >=0
%            mu3 : damping coefficient for a3, >=0
%            r0  : temporal growth rate
%    n     : order of the function, either 0 or 1
%    flag  : flag>0, report progress and plot as function of (x,t)
%
% Output:
%    dn    : value of the Delta_n function on x grid, nx-by-nt
%
% Written by Yuan SHI on Oct 28, 2021
% Last modified by Yuan SHI on June 17, 2022

function dn = Delta(x, t, lpara, n, flag)
    if nargin<4
        n=1; % default solve for a2
    end
    if nargin<5
        flag=0; % default no plot
    end

    % check length
    nx = length(x);
    nt = length(t);
    assert(length(n)==1,'n should be scalar')

    % check bounds
    assert(min(x)>0,'x should be positive!')
    assert(min(t)>0,'t should be positive!')
    assert(n==0 || n==1,'n is either 0 or 1!')

    % call DeltaLimit.m if mu3=v3=0
    % use tDeltaLimit.m if v3=0
    v3  = lpara.v3;
    mu3 = lpara.mu3; 
    if flag>0
       if v3==0
           disp('v3=0')
           if mu3==0
               disp('mu3=0')
           end
       else
           disp('Use full solution')
       end
    end

    % load output
    if v3==0 && mu3==0
        dn = DeltaLimit(x, t, lpara, n);
    else
        % initialize output
        dn = zeros(nx,nt);
        z0 = zeros(nx,1); % initial integral
        ti = 0; % initial time
        tp = 0.0; % progress report
        dtp = max(0.1,1/nt);

        % load time slices
        for i=1:nt
            tf = t(i);
            if flag>0 && i/nt>=tp
                disp([num2str(100*tp),'% finished'])
                tp = tp+dtp;
            end

            % compute next time slice
            if v3==0
                % call tDeltaLimit.m if v3=0
                z = tDeltaLimit(x,ti,tf,lpara,n); % no plot
            else
                % use full solution
                z = tDelta(x,ti,tf,lpara,n); % no plot
            end

            % accumulate integral
            z0 = z0 + z;
            % load value
            dn(:,i) = z0;
            % unpdate initial time
            ti = tf;
        end     
    end

    % plot figure
    if flag>0
        disp('Plotting figure...')
        figure
        [T,X]=meshgrid(t,x);
        contourf(X,T,dn)
        c=colorbar;
        % mark wave front
        hold on
        plot(x,x/lpara.v2,'Color',[0,0,0]+0.5,'LineWidth',3)
        % labels
        xlabel('x')
        ylabel('t')
        ylabel(c,['Delta',num2str(n),'(x,t)']);
        s1 = sprintf('v2=%.2e, v3=%.2e',lpara.v2,v3); 
        s2 = sprintf('mu3=%.2e,r0=%.2e',mu3,lpara.r0); 
        title({s1,s2})

        xlim([0,max(x)])
        ylim([0,max(t)])
        set(gcf,'Color','w')
    end

