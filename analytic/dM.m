% The integrande of the M0 and M1 functions
%     M(n,x,y) = int_{t=0}^{t=1} dM(n,t,x,y)
% Inputs:
%    n     : order of the function, either 0 or 1
%    t     : variable of the integrand, vector
%    x,y   : parameters of the integrand, scalars
%    flag  : flag>0 plot as function of t
%
% Written by Yuan SHI, Oct 27, 2021

function z = dM(n, t, x, y, flag)
    if nargin<5
        flag=0; % default no plot
    end

    % check length
    nt = length(t);
    assert(length(n)==1,'n should be scalar')
    assert(length(x)==1,'x should be scalar!')
    assert(length(y)==1,'y should be scalar!')

    % check bounds
    assert(min(t)>=0 && max(t)<=1,'t should be in [0,1]!')
    assert(n==0 || n==1,'n is either 0 or 1!')
    assert(x>=0,'x should be nonnegative!')
    assert(y>0,'y should be positive!')

    % nbar is binary negation of n
    if n==1
        nbar=0;
    else
        nbar=1;
    end

    % commonly used factors
    xs = x^2;
    xi2 = x/2;
    xy2 = 2*x*y;
    y2 = 2*y;    

    % initialize output
    z = zeros(size(t));
    % load elements
    for it = 1:nt
        ti = t(it);
        xti = x*ti;

        % prefactor
        p = sqrt((xti+n*y2)/(xti+y2));
     
        % first bessel factor
        % argument
        a1 = sqrt(xti^2+ti*xy2);
        % bessel function
        b1 = besseli(nbar,a1);
     
        % second bessel factor
        if ti==1
            b2 = xi2;
        else
            b2 = besseli(1,x*(1-ti))/(1-ti);
        end
     
        % total integrade
        z(it) = p*b1*b2;
    end

    % plot figure
    if flag>0
        plot(t,z,'.-')
        xlabel('t')
        ylabel(['dM',num2str(n),'(t,x,y)'])
        title(['x=',num2str(x),', y=',num2str(y)])
        set(gcf,'Color','w')
    end
