% The D0 and D1 kernal functions
%
% Inputs:
%    n     : order of the function, either 0 or 1
%    x     : variable of the function, vector
%    y     : parameters of the function, scalar
%    flag  : flag>0 plot as function of x
% Output:
%    z    : value of the D_n function, same length as x
%
% Written by Yuan SHI, Oct 27, 2021

function z = Dkernal(n, x, y, flag)
    if nargin<4
        flag=0; % default no plot
    end

    % check length
    assert(length(n)==1,'n should be scalar')
    assert(length(y)==1,'y should be scalar!')

    % check bounds
    assert(min(x)>=0,'x should be nonnegative!')
    assert(n==0 || n==1,'n is either 0 or 1!')
    assert(y>0,'y should be positive!')

    % prefactor
    p = sqrt(1+2*n*y./x);
    % argument for bessel functions
    w = sqrt(x.^2+2*y*x);
    % D function
    z = p.*besseli(n,w) - Mkernal(n,x,y);

    % plot figure
    if flag>0
        clf
        plot(x,z,'.-')
        % plot limiting case
        if y<=0.05;
            hold on
            if n==0
                plot(x,2*besseli(1,x)./x,'r.-')
            else
                plot(x,zeros(size(x)),'r.-')
                ylim([0,0.1])
            end
            legend('D Nintegral','D(y->0)')
        end
        xlabel('x')
        ylabel(['D',num2str(n),'(x,y)'])
        title(['y=',num2str(y)])
        set(gcf,'Color','w')
    end

