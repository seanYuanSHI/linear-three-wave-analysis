% The Delta0 and Delta1 functions at one time slice
% in the limit v3=0 but mu3~=0. In this limit
%    a2/(h0*exp(-mu2*x/v2)) - 1  = Delta_1
%    a3/(h0*exp(-mu2*x/v2))      = Delta_0
% where h0 is the constant amplitude of the step-function pulse that excites a2
%
% Inputs:
%    x     : spatial grid, vector, x>0
%    ti    : initial time, scalar, ti>0
%    tf    : final time, scalar, tf>ti
%    lpara : struct that contains the following in SI unit
%            v2  : group velocity of a2, >0
%            mu3 : damping coefficient for a3, >=0
%            r0  : temporal growth rate
%    n     : order of the function, either 0 or 1
%    flag  : flag>0, plot as function of x
% Output:
%    dn    : value of the Delta_n function on x grid, nx-by-1
%
% Written by Yuan SHI on June 17, 2022

function dn = tDeltaLimit(x, ti, tf, lpara, n, flag)
    if nargin<5
        n=1; % default solve for a2
    end
    if nargin<6
        flag=0; % default no plot
    end

    % check length
    nx = length(x);
    assert(length(ti)==1,'ti should be scalar')
    assert(length(tf)==1,'tf should be scalar')
    assert(length(n)==1,'n should be scalar')
    % check bounds
    assert(min(x)>0,'x should be positive!')
    assert(tf>ti && ti>=0,'tf>ti>=0 should be positive!')
    assert(n==0 || n==1,'n is either 0 or 1!')

    % unload lpara, ensure positive
    v2 = abs(lpara.v2);
    mu3 = abs(lpara.mu3);
    r0 = abs(lpara.r0);

    % initialize output
    dn = zeros(nx,1);
    % load value
    for i=1:nx
        % retarded time since wave front pass
        xv2 = x(i)/v2;
        tri = ti - xv2;
        trf = tf - xv2;
        % nonzero only when tr>0
        if trf>0
            % normalized time
            rt = 2*r0*xv2;
            % normalized damping
            g2 = mu3/(2*rt*r0);
            % integral bounds
            gti = 2*r0*real(sqrt(xv2*tri));
            gtf = 2*r0*sqrt(xv2*trf);

            % function handel
            if n==1 % a2
                fun = @(z)exp(-g2*z.^2).*besseli(1,z);
            else % a3
                fun = @(z)z.*exp(-g2*z.^2).*besseli(0,z)/rt;
            end

            % numerical integral
            dn(i) = integral(fun,gti,gtf);
        end
    end

    % plot figure
    if flag>0
        figure
        plot(x,dn,'.-')
        % mark wave front
        hold on
        v2ti = v2*ti;
        v2tf = v2*tf;
        line([v2ti,v2ti],ylim,'Color',[0.5,0.5,0.5]);
        line([v2tf,v2tf],ylim,'Color',[0.1,0.1,0.1]);
        % labels
        xlabel('x')
        ylabel(['tDelta',num2str(n),'(x,ti,tf)'])
        % title strings
        s1 = sprintf('ti=%.2e, tf=%.2e',ti,tf);
        s2 = sprintf('v2=%.2e, mu3=%.2e, r0=%.2e',v2,mu3,r0); 
        title({s1,s2})

        xlim([0,max(x)])
        set(gcf,'Color','w')
    end

