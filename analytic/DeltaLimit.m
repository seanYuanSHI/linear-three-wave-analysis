% The Delta0 and Delta1 functions in the limit 
% v3=0 and mu3=0. In this case, we still have 
%    a2/(h0*exp(-mu2*x/v2)) - 1  = Delta_1
%    a3/(h0*exp(-mu2*x/v2))      = Delta_0
% where h0 is the constant amplitude of the step-function
% This function is to be called by Delta.m
%
% Inputs:
%    x     : spatial grid, vector, x>0
%    t     : time, vector, t>0
%    lpara : struct that contains the following in SI unit
%            v2  : group velocity of a2, >0
%            r0  : temporal growth rate
%    n     : order of the function, either 0 or 1
%
% Output:
%    dn    : value of the Delta_n function on x grid, nx-by-nt
%
% Written by Yuan SHI on Nov 12, 2021

function dn = DeltaLimit(x, t, lpara, n)
    if nargin<4
        n=1; % default solve for a2
    end
    if n==1
        nbar=0;
    else
        n=0;
        nbar=1;
    end

    % unload struct
    v2 = lpara.v2;
    r0 = lpara.r0;

    % initialize output
    nx = length(x);
    nt = length(t);
    dn = zeros(nx,nt);

    % load values
    % MATLAB is column major
    for it=1:nt
        for ix=1:nx
            % transit time
            tt = x(ix)/v2;
            % retarded time
            tr = t(it)-tt;
            % nonzero only within light cone
            if tt>0 && tr>0
                % gain exponent
                G = 2*r0*sqrt(tr*tt);
                % prefactor
                p = nbar*sqrt(tr/tt)+n;
                % bessel function
                b = besseli(nbar,G);
                % load value
                dn(ix,it) = p*b;
            end
        end
    end     
  
    % shift to Delta1 = alpha2/h0 - 1
    if n==1
        dn = dn-1;
    end   
