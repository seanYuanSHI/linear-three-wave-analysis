% The M0 and M1 kernal functions
%     M(n,x,y) = int_{t=0}^{t=1} dM(n,t,x,y)
% Inputs:
%    n     : order of the function, either 0 or 1
%    x     : variable of the function, vector
%    y     : parameters of the function, scalar
%    flag  : flag>0 plot as function of x
%
% Written by Yuan SHI, Oct 27, 2021

function z = Mkernal(n, x, y, flag)
    if nargin<4
        flag=0; % default no plot
    end

    % check length
    nx = length(x);
    assert(length(n)==1,'n should be scalar')
    assert(length(y)==1,'y should be scalar!')

    % check bounds
    assert(min(x)>=0,'x should be positive!')
    assert(n==0 || n==1,'n is either 0 or 1!')
    assert(y>0,'y should be positive!')

    % initialize output
    z = zeros(size(x));
    % load values
    for i=1:nx
        % function handel
        fun = @(t)dM(n,t,x(i),y);
        % numerical integral
        z(i) = integral(fun,0,1);
    end

    % plot figure
    if flag>0
        clf
        plot(x,z,'.-')
        % plot limiting case
        if y<0.3;
            hold on
            if n==0
                plot(x,besseli(0,x)-2*besseli(1,x)./x,'r.-')
            else
                plot(x,besseli(1,x),'r.-')
            end
            legend('M Nintegral','M(y->0)')
        end
        xlabel('x')
        ylabel(['M',num2str(n),'(x,y)'])
        title(['y=',num2str(y)])
        set(gcf,'Color','w')
    end

