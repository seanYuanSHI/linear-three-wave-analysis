% This function is used by EigenPsi.m to correct for phase jump. 
% The phase of the z signal should lead the phase of y signal by pi/2.
% Namely, bz = by + pi/2. However, due to the 2*pi periodicity,
% the range -pi<b<pi, and numerical errors, the condition 
% is slightly more complicated, which is accounted for by this function.
%
%          bz
%          |
%   ------ pi%%%%%%
%   |     %|%%%%% |
%   |   %%pi/2%   |
%   |%%%%%%|%     |
% -pi-pi/2-0+pi/2+pi+by
%   %%%%%%%|      %
%   %%%% -pi/2   %%
%   %%%    |   %%%%
%   %%-- -pi %%%%%%
% 
% Phases in the %% region are OK. Otherwise, a phase shift on z is needed.
%
% Inputs:
%    by, bz : phases for y and z signals, in rad and in range [-pi,pi]
% Output:
%    s      : s=0 if no phase shift is needed 
%             s=1 if a pi-phase shifted is needed for bz
%
% Written by Yuan SHI on Oct 9, 2021
function s = PhaseShift(by,bz)
    % check input range
    msg = 'Phases should be within -pi and +pi !!!';
    assert(by>=-pi && by<=pi, msg)
    assert(bz>=-pi && bz<=pi, msg)

    % defaul no phase shift
    s = 0;
    % pi/2
    pi2 = pi/2;

    % check by<0
    if by<0 && abs(bz-by-pi2)>pi2
        s = 1;
    end

    % check by>=0
    if by>=0 && abs(bz-by+pi2)<=pi2
        s = 1;
    end

