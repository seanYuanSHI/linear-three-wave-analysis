% This function calculate the running average 
% of an array for given window size.
%
% Written by Yuan SHI on June 4, 2022

function avg = RunAverage(array, window, flag)
    if nargin<3
        flag=0; % default no plot
    end

    % length of array
    n = length(array);
    % half index size of window
    wid = ceil(min(window,n)/2);
 
    % initialize output
    avg = zeros(size(array));
    % load average
    for i=1:n
        % left/right index
        il = max(1,i-wid);
        ir = min(n,i+wid);
        avg(i) = mean(array(il:ir));
    end

    % plot figure
    if flag>0
        figure
        plot(array,'b.-')
        hold on
        plot(avg,'r')
        legend('array','average')
        set(gcf,'Color','w')
    end
    

