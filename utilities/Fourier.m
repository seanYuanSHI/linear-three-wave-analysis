% This function take fft on a real-valued 1D data 
% assuming the x grid is uniformly spaced with 
% spacing dx. Only output the first half of the 
% replicated spectrum.
%
% Inputs:
%    y      : spatial-domain data, vector
%    dx     : uniform spatial grid length
%    flag   : flag=1 plot data and power spectrum
% Outputs:
%    k  : momentum grid, vector of the same size as y
%    f  : Fourier space data, complex vector
%
% Written by Yuan SHI, Sep 14, 2021

function [k, f] = Fourier(y, dx, flag)

    % defaul no plot
    if nargin<3
        flag = 0;
    end

    % length of data 
    nd = length(y);
    % take fft
    fd = fft(y);
    % keep only first half, the second half is 
    % merely a reflection for real-valued data
    ih = ceil(nd/2);
    f = fd(1:ih);

    % construct k vector
    dk = 2*pi/(dx*(nd-1));
    k = zeros(size(f));
    nf = length(f);
    for i=1:nf
        k(i) = (i-1)*dk;
    end

    % plot figure
    if flag==1
        % plot spatial data
        x = zeros(size(y));
        for i=1:nd
            x(i) = (i-1)*dx;
        end
        subplot(2,1,1)
        plot(x,y)
        xlabel('x')
        ylabel('y')

        % plot power spectrum
        subplot(2,1,2)
        plot(k,2*abs(f)/nd,'.-')
        xlabel('k')
        ylabel('2*|f|/N')

        set(gcf,'Color','w')
    end

    


