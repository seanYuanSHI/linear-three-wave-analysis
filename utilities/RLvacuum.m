% This function read simulation data and
% calculate right/left waves in vacuum gaps
% to check reflection/transmission.
%
% Inputs: 
%    it     : time slice to analyze
%    fpath  : folder containing simulation data
%    region : region= 1: right vacuum gap
%             region=-1: left vacuum gap
%    flag   : flag>0 plot figures
% Outputs:
%   RL     : struct that contains the following 
%              RL.comps = {'ry','rz','ly','lz'}
%              RL.t : time at this slice
%              RL.x : spatial grid in the vacuum gap
%              RL.Edata
%                  Edata(:,1)=Ery, Edata(:,2)=Erz
%                  Edata(:,3)=Ely, Edata(:,4)=Elz
%              RL.sr, RL.sl
%            where sr and sl are averaged normalized fluxes.
%
% Written by Yuan SHI on June 27, 2022

function RL = RLvacuum(it, fpath, region, flag)
    % default inputs
    if nargin<4
        flag=1; % default plot
    end
    if nargin<3
        region = 1; % default right gap
    end
    % load setup parameters
    ParaSetup

    % select grid points
    % half safety lambda
    sl2 = ceil(safety_lambda/2);
    if region>0 % right vacuum gap
        ir = (box_lambda-sl2)*cell_lambda;
        il = (box_lambda-right_lambda+sl2)*cell_lambda;
    else % left vacuum gap
        il = sl2*cell_lambda;
        ir = (left_lambda-sl2)*cell_lambda;
    end

    % read simulation data
    [xm, Ey, t] = ReadGrid(it,'Ey',fpath);
    [~, Ez, ~] = ReadGrid(it,'Ez',fpath);
    [~, By,~] = ReadGrid(it,'By',fpath);
    [~, Bz,~] = ReadGrid(it,'Bz',fpath);
    % store data
    x = xm(il:ir);
    data.Ey = Ey(il:ir);
    data.Ez = Ez(il:ir);
    data.By = By(il:ir);
    % subtract DC field, B0 and theta from ParaSetup
    data.Bz = Bz(il:ir)-1e2*B0*sin(theta/180*pi);
    disp(['theta=',num2str(theta)])
  
    % compute right/left, use vacuum speed of light c8
    RL = RightLeft(data,c8*1e8*ones(1,2),flag);
    RL.x = x;
    RL.t = t;

    % plot data selection
    if flag>0
        figure
        plot(xm,Ey,'b')
        hold on
        %plot(xm,Ez,'r')
        plot(x,data.Ey,'c','LineWidth',2)
        %plot(x,data.Ez,'.m')
        % mark plasma boundaries
        ii = left_lambda*cell_lambda+1;
        line([xm(ii),xm(ii)],ylim,'LineStyle','--','Color','g','LineWidth',3)
        ii = (box_lambda-right_lambda)*cell_lambda;
        line([xm(ii),xm(ii)],ylim,'LineStyle','--','Color','g','LineWidth',3)
        % mark data boundaries
        line([xm(il),xm(il)],ylim,'LineStyle','-','Color','r','LineWidth',2)
        line([xm(ir),xm(ir)],ylim,'LineStyle','-','Color','r','LineWidth',2)
        % mark figure
        xlabel('x (m)')
        ylabel('Ey (V/m)')
        %legend('Ey','Ez')
        set(gcf,'Color','w')
    end
