% This function locate the wave front of a2.
% Recall that a2 is excited by a step-function
% pulse on the left boundary. So at x=0, a2=h0 
% is some constant. As the wave propagates and 
% grows, the wave front will located at some xr.
% Analytically, a2(x<xr)>h0 and a2(x>xr)=0.
% However, due to numerical errors, and
% finite-width ramp up of seed, the wave front 
% will be more fuzzy. In this program.
% The wave front will be identified as h0/2.
%
% Inputs:
%    a2   : envelope of a2 on uniform spatial grid, length nx
%    flag : flag>0 plot diagnostic figure
% Output:
%    ix   : index, a2(ix-1)>h0/2 and a2(ix)<h0/2
%           ix = nx+1 if no wave front is identified
%    nf   : integer thickness of the wave front
%           a2(ix-nf)>0.9*h0, as(ix+nf)<0.1*h0
%
%    when fail to identify wave front, ix = nx+1 and nf=0
%
% Last modified by Yuan SHI on Oct 29, 2021

function [ix, nf] = StepFinder(a2, flag)
    if nargin<2
        flag=0; % default no plot
    end
    % length of a2
    nx = length(a2);
    % check length
    assert(nx>2,'a2 should be an array!')
    % ensure positive envelope
    aa = abs(a2);

    % initial estimate 
    h0 = aa(1);
    i0 = 2;
    while aa(i0)>0.9*h0 && i0<nx
        i0 = i0+1;
    end

    if i0==nx % no wave front identified
        ix = nx+1;
        ir = ix;
        il = ix;
        nf = 0;   
    else
        % more balanced estimate of h0
        h0 = (aa(1)+aa(i0-1))/2;
        ix = 1;
        while aa(ix)>h0/2 && ix<nx
            ix = ix+1;
        end

        if ix==nx % no wave front identified
            ix = nx+1;
            ir = ix;
            il = ix;
            nf = 0;
        else % estimate wave front thickness
           il = ix;
           while aa(il)<=0.9*h0 && il>1
               il = il-1;
           end

           ir = ix;
           while aa(ir)>0.1*h0 && ir<nx
               ir = ir + 1;
           end

           nf = ir-il+1;
        end
    end

    % check data is below threshold beyond wave front
    if ix<=nx
        m = mean(aa(ix:nx));
        % mark invalid identification
        if m>h0/2
            ix=nx+1;
            il=ix;
            ir=ix;
            nf=0;
        end
    end
    
    % plot diagnostic figure
    if flag>0
        disp('Plotting diagnostic figure')
        if ix>nx
            disp('No wave front identified')
        end
        %clf
        figure
        plot(a2,'.-')
        hold on
        line([ix,ix],ylim,'Color','k');
        line([ir,ir],ylim,'Color',[0.5,0.5,0.5],'LineStyle','--');
        line([il,il],ylim,'Color',[0.5,0.5,0.5],'LineStyle','--');
        xlabel('x index')
        ylabel('a2')
        set(gcf,'Color','w')
    end
        
