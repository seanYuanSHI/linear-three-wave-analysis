% This function returns default file paths
% When lambda < 0, process pump wave 
% 1 file name: <fhead>/PumpPara.mat
% 2 data path: <fhead>/PumpLinear/Psi.mat
% 3 data path: <fhead>/PumpEigen/Const.mat            
%
% 4 data path: <fhead>/Resonaces.mat
%   
% When lambda >0, post process seed wave
% 1 file name: <fhead><shead>/SeedPara.mat
% 2 data path: <fhead><shead>/SeedLinear/Psi.mat
% 3 data path: <fhead><shead>/SeedEigen/Const.mat
%
% 4 data path: <fhead><shead>/Stimulated/Vphase.mat            
%
% 5 data path: <fhead><shead>/Stimulated/RightLeft
% 6 data path: <fhead><shead>/Stimulated/Envelope
% 7 data path: <fhead><shead>/Stimulated/WFdata.mat
% 8 data path: <fhead><shead>/Stimulated/Fits/
% 9 data path: <fhead><shead>/Stimulated/Fits/

function apath = PathSetup(fhead, shead)

% enter directory for Three-Wave
cd ~/MATLAB/three-wave-matlab/src/

% default file name and path
% for pump
PL = [fhead,'/PumpLinear/']; 
PE = [fhead,'/PumpEigen/']; 

Ppsi = [PL,'Psi.mat']; 
Pconst = [PE,'Const.mat']; 
Ppara = [fhead,'/PumpPara.mat'];
PR = [fhead,'/Resonances.mat'];

% for seed
fshead = [fhead,shead];
SL = [fshead,'/SeedLinear/']; 
SE = [fshead,'/SeedEigen/']; 
SS = [fshead,'/Stimulated/']; 

Spsi = [SL,'Psi.mat']; 
Sconst = [SE,'Const.mat']; 
Spara = [fshead,'/SeedPara.mat'];

% array of name for later convenience
% store all path
apath.fhead = fhead;
apath.shead = shead;
apath.fshead = fshead;

apath.PLSL = {PL,SL};
apath.PESE = {PE,SE};
apath.PR = PR;
apath.SS = SS; 

apath.SSV = [SS,'VPhases.mat'];
apath.paras = {Ppara,Spara};
apath.psis = {Ppsi,Spsi};
apath.consts = {Pconst,Sconst};
